
$("#log-filter-alerts .log-filter-option").click(function(){
	handleOptionsClick(this, "filter-alerts");
});

$("#log-filter-levels .log-filter-option").click(function(){
	handleOptionsClick(this, "filter-levels");
});

function handleOptionsClick(item, fieldName)
{
	var isSelected = $(item).hasClass("log-filter-option-selected");
	if(isSelected){
		$(item).removeClass("log-filter-option-selected");
	} else {
		$(item).addClass("log-filter-option-selected");
	}
	
	var hasFilter = false;
	var value = "";
	$($(item).parent()).children().each(function(){
		if($(this).hasClass("log-filter-option-selected")){
			if(value != ""){
				value += ",";
			}
			value += $(this).attr("value");
		} else {
			hasFilter = true;
		}
	});
	
	if(hasFilter){
		$("#" + fieldName).val(value);
	} else {
		$("#" + fieldName).val("");
	}
	
}