package com.loghobbit.service.clients;

import org.apache.cxf.helpers.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.loghobbit.entities.v1.User;

public class ServiceClientBase {
	protected static final ObjectMapper mapper = new ObjectMapper();
	
	protected String endpoint;
	protected HttpClient client;
	
	protected ServiceClientBase(String endpoint, HttpClient client)
	{
		this.endpoint = endpoint;
		this.client = client;
	}
	
	protected void handleErrorResponse(HttpResponse response) throws Exception
	{
		if(response.getStatusLine().getStatusCode() >= 300 || response.getStatusLine().getStatusCode() < 200)
		{
			String message = IOUtils.toString(response.getEntity().getContent());
			response.getEntity().getContent().close();
			throw new Exception(message);
		}
	}
	
	protected void addCommonHeaders(HttpUriRequest request, User user)
	{
		request.setHeader("token", user.getSecurityToken());
		request.setHeader("Content-Type", "application/json");
		request.setHeader("Accept", "application/json");
	}
}
