package com.loghobbit.service.v1;

import static org.mockito.Mockito.*;

import javax.ws.rs.core.Response;

import org.junit.Assert;
import org.junit.Test;

import com.loghobbit.entities.v1.TeamMember;
import com.loghobbit.entities.v1.TeamMemberType;

public class GroupSvc_PutGroupTest extends GroupSvc_TestBase {

	@Test
	public void newGroup_Valid() throws Exception
	{
		this.group.setId(null);
		Response response = this.groupSvc.putGroup(this.group, USER_ID);
		
		Assert.assertNotNull(response);
		Assert.assertEquals("Status Code", 200, response.getStatus());
		
		verify(this.groupAccessLayer).addGroup(this.group);
	}
	
	@Test
	public void newGroup_NullGroup() throws Exception
	{
		this.group.setId(null);
		Response response = this.groupSvc.putGroup(null, USER_ID);
		
		this.validateErrorResponse(response, 400);
		
		verify(this.groupAccessLayer, times(0)).addGroup(this.group);
	}
	
	@Test
	public void newGroup_notAValidUserId() throws Exception
	{
		this.group.setId(null);
		
		reset(this.userAccessLayer);
		Response response = this.groupSvc.putGroup(this.group, USER_ID);
		
		Assert.assertNotNull(response);
		Assert.assertEquals("Status Code", 200, response.getStatus());
		
		verify(this.groupAccessLayer).addGroup(this.group);
	}
	
	@Test
	public void newGroup_NoMembers() throws Exception
	{
		this.group.setId(null);
		this.group.getTeamMembers().clear();
		Response response = this.groupSvc.putGroup(this.group, USER_ID);
		
		Assert.assertNotNull(response);
		Assert.assertEquals("Status Code", 200, response.getStatus());
		
		verify(this.groupAccessLayer).addGroup(this.group);
		Assert.assertEquals("User count", 1, this.group.getTeamMembers().size());
		
		TeamMember member = this.group.getTeamMembers().get(0);
		Assert.assertEquals("User Id", USER_ID, member.getMemberId());
		Assert.assertEquals("Member Type", TeamMemberType.owner, member.getType());
		Assert.assertEquals("Accepted status", true, member.getAccepted());
	}
	
	@Test
	public void newGroup_AdminMember() throws Exception
	{
		this.group.setId(null);
		this.anotherMember.setType(TeamMemberType.admin);

		Response response = this.groupSvc.putGroup(this.group, USER_ID);

		Assert.assertNotNull(response);
		Assert.assertEquals("Status Code", 200, response.getStatus());
		
		verify(this.groupAccessLayer).addGroup(this.group);
		Assert.assertEquals("User count", 2, this.group.getTeamMembers().size());
	}
	
	@Test
	public void newGroup_AdminMemberNotAllowed() throws Exception
	{
		this.group.setId(null);
		this.user.getSystemOptions().setCanHaveAdmins(false);
		this.anotherMember.setType(TeamMemberType.admin);

		Response response = this.groupSvc.putGroup(this.group, USER_ID);

		this.validateErrorResponse(response, 400);
	}
	
	@Test
	public void updateGroup_Valid() throws Exception
	{
		Response response = this.groupSvc.putGroup(this.group, USER_ID);

		Assert.assertNotNull(response);
		Assert.assertEquals("Status Code", 200, response.getStatus());
		
		verify(this.groupAccessLayer).updateGroup(this.group);
	}
	
	@Test
	public void updateGroup_ExistingGroupNotFound() throws Exception
	{
		reset(this.groupAccessLayer);
		Response response = this.groupSvc.putGroup(this.group, USER_ID);

		this.validateErrorResponse(response, 400);
	}
	
	@Test
	public void updateGroup_Admin() throws Exception
	{
		this.anotherMember.setType(TeamMemberType.admin);
		Response response = this.groupSvc.putGroup(this.group, USER_ID);

		Assert.assertNotNull(response);
		Assert.assertEquals("Status Code", 200, response.getStatus());
		
		verify(this.groupAccessLayer).updateGroup(this.group);
	}
	
	@Test
	public void updateGroup_AdminsNotAllowed() throws Exception
	{
		this.user.getSystemOptions().setCanHaveAdmins(false);
		this.anotherMember.setType(TeamMemberType.admin);
		Response response = this.groupSvc.putGroup(this.group, USER_ID);

		this.validateErrorResponse(response, 400);
	}
	
	@Test
	public void updateGroup_NoTeamMembers() throws Exception
	{
		this.group.getTeamMembers().clear();
		Response response = this.groupSvc.putGroup(this.group, USER_ID);

		this.validateErrorResponse(response, 400);
	}
	
	@Test
	public void updateGroup_NullAccessKey() throws Exception
	{
		this.group.setAccessKey(null);
		Response response = this.groupSvc.putGroup(this.group, USER_ID);

		this.validateErrorResponse(response, 400);
	}
	
	@Test
	public void updateGroup_EmptyAccessKey() throws Exception
	{
		this.group.setAccessKey("");
		Response response = this.groupSvc.putGroup(this.group, USER_ID);

		this.validateErrorResponse(response, 400);
	}
	
	@Test
	public void updateGroup_SmallAccessKey() throws Exception
	{
		this.group.setAccessKey("123456789");
		Response response = this.groupSvc.putGroup(this.group, USER_ID);

		this.validateErrorResponse(response, 400);
	}
}
