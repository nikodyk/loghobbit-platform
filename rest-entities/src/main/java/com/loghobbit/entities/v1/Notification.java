package com.loghobbit.entities.v1;

public class Notification {
	private NotificationType type;
	public NotificationType getType(){ return this.type; }
	public void setType(NotificationType value){ this.type = value; }
	
	private Group group;
	public Group getGroup(){ return this.group; }
	public void setGroup(Group value){ this.group = value; }
	
	private User from;
	public User getFrom(){ return this.from; }
	public void setFrom(User value) { this.from = value; }
}
