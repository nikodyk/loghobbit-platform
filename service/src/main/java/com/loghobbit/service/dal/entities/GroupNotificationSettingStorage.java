package com.loghobbit.service.dal.entities;

import com.loghobbit.entities.v1.GroupNotificationSetting;

public class GroupNotificationSettingStorage {
	public static final String GROUP_ID = "setting.groupId";
	public static final String USER_ID = "userId";
	
	private String userId;
	public String getUserId(){ return this.userId; }
	public void setUserId(String value){ this.userId = value; }
	
	private GroupNotificationSetting setting;
	public GroupNotificationSetting getSetting(){ return this.setting; }
	public void setSetting(GroupNotificationSetting value){ this.setting = value; }
}
