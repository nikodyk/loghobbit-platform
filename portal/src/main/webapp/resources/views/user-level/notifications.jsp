<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>LogHobbit: Notifications</title>
	<jsp:include page="../../fragments/header.jsp"/>
	
</head>
<body>
	<jsp:include page="../../fragments/navbar.jsp" />
	
	<div class="container">
		<h1>Notifications</h1>
	</div>
	
	<table class="container">
		<thead>
			<tr>
				<th>From</th>
				<th>Message</th>
			</tr>
		</thead>
	<c:if test="${notifications != null }">
		<c:forEach var="item" items="${notifications }">
			<tr>
				<td>${item.from.fullName }</td>
				<td>Invites you to join '${item.group.name }'</td>
				<td>
					<a class="btn btn-primary" href="/notifications/group/${item.group.id }/accept">Accept</a>
					<a class="btn btn-primary" href="/notifications/group/${item.group.id }/decline">Decline</a>
				</td>
			</tr>
		</c:forEach>
	</c:if>
	</table>
</body>
</html>