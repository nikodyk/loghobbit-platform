<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${showHeader && (logs != null || sessionScope.filter != null)}">

<table class="table">
	<thead>
		<tr>
			<th class="col-md-2"><h4>Date</h4></th>
			<th class="col-md-1"><h4>Type</h4></th>
			<th class="col-md-8"><h4>Message</h4></th>
			<th class="col-md-1"><a onclick="toggleFilter()"><i class="fa fa-filter"></i></a></th>
		</tr>
	</thead>
	<tr id="log-filters" style="display:none;" class="log-filter-row">
		<td colspan="4">
			<form method="POST" action="?logViewAction=filter">
				<div class="form-group">
					<label for="filter-message-regex">Message Regex:</label>
					<input id="filter-message-regex" name="messageRegex" type="text" class="form-control" value="${sessionScope.filter.messageRegex }"/>
				</div>
				<div class="form-group">
					<label for="filter-server-regex">Server Regex:</label>
					<input id="filter-server-regex" name="serverRegex" type="text" class="form-control" value="${sessionScope.filter.serverRegex }"/>
				</div>
				<div class="form-group">
					<label for="filter-before">Before:</label> 
					<input id="filter-before" name="beforeDate" class="form-control" type="date" value="" />
				</div>
				<div class="form-group">
					<label>Level:</label>
					<div id="log-filter-levels">
						<a class="log-filter-option ${(filter_level_info)? 'log-filter-option-selected' : '' }" value="info">info</a>
						<a class="log-filter-option ${(filter_level_warn)? 'log-filter-option-selected' : '' }" value="warn" >warning</a>
						<a class="log-filter-option ${(filter_level_error)? 'log-filter-option-selected' : '' }" value="error" >error</a>
						<a class="log-filter-option ${(filter_level_fatal)? 'log-filter-option-selected' : '' }" value="fatal" >fatal</a>
					</div>
					<input type="hidden" id="filter-levels" name="levels" value="" />
				</div>
				<div class="form-group">
					<label>Exceptions:</label>
					<div id="log-filter-alerts">
						<a class="log-filter-option ${(filter_part_exception)? 'log-filter-option-selected' : '' }" value="exception">Exception</a>
						<a class="log-filter-option ${(filter_part_message)? 'log-filter-option-selected' : '' }" value="message">Message</a>
					</div>
					<input type="hidden" id="filter-alerts" name="messageParts" value="" />
				</div>
				<div class="form-group text-right" style="margin-top:10px;">
					<input type="submit" class="btn btn-primary" value="Apply"/>&nbsp;<a href="/?groupId=${group.id }&showFilter=true" class="btn btn-primary">Reset</a>&nbsp;<a class="btn btn-primary" onclick="toggleFilter()">Close</a>
				</div>
			</form>
		</td>
	</tr>
	<tr>
		<td id="newLogButton"  colspan="4" class="row warning" style="display:none;">
			<a class="btn btn-warning col-xs-12" onclick="location.reload();")>
				New log entries have been added (<span id="newLogEntryCount"></span>)
			</a>
		</td>
	</tr>
</c:if>
<c:forEach var="item" items="${logs}" varStatus="loop">
	<tr class="lvl-${item.logEntry.level }">
		<td class="col-md-2">${item.logEntry.simpleDate}</td>
		<td class="col-md-1">${item.logEntry.level}</td>
		<td class="col-md-8 message-summary">${item.logEntry.message}</td>
		<td class="col-md-1">
			<a class="more-button" onClick="moreLess(this)" target="log-details-index-${loop.index }">More
				<c:if test="${ item.logEntry.exception != null }">
					<i class="exception-present fa fa-exclamation-circle"></i>
				</c:if>
			</a>
		</td>
	</tr>
	<tr id="log-details-index-${loop.index }" class="log-details bs-callout bs-callout-info lvl-${item.logEntry.level }" style="display:none">
		<td colspan="4">
			<div><b>Log Name:</b> ${item.logEntry.logName}</div>
			<div><b>Server:</b> ${item.logEntry.server}</div>
			<div><b>Message:</b></div>
			<div><pre>${item.logEntry.message}</pre></div>
			<c:if test="${item.logEntry.exception != null }">
				<div><b>Exception:</b></div>
				<div>
					<pre style="white-space: pre-line">${item.logEntry.exception}</pre>
				</div>
			</c:if>
			<div><b>Alerts:</b></div>
			<div class="log-alerts-details">
				<c:forEach var="alert" items="${item.alertCriteria }">
					<div class="log-alert-details line-item">
						<c:choose>
							<c:when test="${alert.id == null }">
								<span><a class="alert-edit">Create Alert</a></span>
							</c:when>
							<c:otherwise>
								<span>${alert.name }</span>
								<span style="float:right;">
									<a class="alert-edit">Edit</a>
									<span>|</span>
									<c:choose>
										<c:when test="${ alert.users.contains(userId)}">
											<a>Ignore</a>
										</c:when>
										<c:otherwise>
											<a>Watch</a>
										</c:otherwise>
									</c:choose>
								</span>
							</c:otherwise>
						</c:choose>
						<div class="alert-details" style="display:none;">
							<form method="POST" action="?logViewAction=addAlert">
								<input type="hidden" name="id" value="${alert.id }" />
								<input type="hidden" name="groupId" value="${alert.groupId }"/>
								<div class="form-group">
									<label>Name:</label>
									<input id="name" name="name" type="text" class="form-control" placeholder="New alert name" value="${alert.name }"/>
								</div>
								<div class="form-group">
									<label>Message Regex:</label>
									<input id="messageRegex" name="messageRegex" type="text" class="form-control" value="${alert.messageRegex }"/>
								</div>
								<div class="form-group">
									<label>Exception Regex:</label>
									<input id="exceptionRegex" name="exceptionRegex" type="text" class="form-control" placeholder="Exception Regular Expression.  If blank none will be used." value="${alert.exceptionRegex }"/>
								</div>
								<div class="form-group">
									<label>Server Regex:</label>
									<input id="serverRegex" name="serverRegex" type="text" class="form-control" placeholder="Server regular expression.  If blank none will be used." value="${alert.serverRegex }"/>
								</div>
								<div>
									<c:choose>
										<c:when test="${alert.users.contains(userId) }">
											<a id="update" class="btn btn-primary">Update</a>
											<a id="cancel" class="btn btn-primary">Ignore</a>
										</c:when>
										<c:otherwise>
											<a  id="update" class="btn btn-primary">Add</a>
										</c:otherwise>
									</c:choose>
									<a id="cancel" class="btn btn-primary">Cancel</a>
								</div>
							</form>
						</div>
					</div>
				</c:forEach>
			</div>
		</td>
	</tr>
</c:forEach>

<tr><td colspan="4">
	<div class="col-xs-1 col-xs-offset-4  text-center">
	<c:if test="${page > 0 }">
		<a href="?groupId=${group.id}&page=${page - 1 }">&lt;&nbsp;Previous</a>
	</c:if>
	</div>
	<div class="col-xs-1 text-center">${page}</div>
	<div class="col-xs-1 text-center"><a href="?groupId=${group.id}&page=${page + 1 }">Next &nbsp; &gt;</a></span></div>
</td></tr>

<c:if test="${showHeader && (logs != null || sessionScope != null)}">
	</table>
	<script type="text/javascript" src="/resources/js/log-management/log-line.js"></script>
	<script type="text/javascript" src="/resources/js/log-management/log-filter.js"></script>
	<script type="text/javascript" src="/resources/js/log-management/log-alert.js"></script>
	
</c:if>