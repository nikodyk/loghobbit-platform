<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<link rel="icon" 
      type="image/png" 
      href="/resources/images/fav_icon.ico">

<link href="/resources/bootstrap/css/bootstrap.css" rel="stylesheet" />
<link href="/resources/bootstrap/css/docs.min.css" rel="stylesheet" />
<link href="/resources/style.css" rel="stylesheet" />
<link href="/resources/font-awesome-4.3.0/css/font-awesome.min.css" rel="stylesheet" />

<script type="text/javascript" src="/resources/js/jquery-2.1.4.min.js"></script>

<c:if test="${ sessionScope.analyticsId != null && sessionScope.analyticsId != '' }">
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	
	  ga('create', '${analyticsId}', 'auto');
	  ga('send', 'pageview');
	</script>
</c:if>