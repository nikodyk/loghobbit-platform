package com.loghobbit.entities.v1;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;

public class AlertCriteria {
	public static final String ID = "_id";
	public static final String NAME = "name";
	public static final String MESSAGE_REGEX = "messageRegex";
	public static final String EXCEPTION_REGEX = "exceptionRegex";
	public static final String SERVER_REGEX = "serverRegex";
	public static final String USERS = "users";
	public static final String GROUP_ID = "groupId";
	
	@Id
	private String id;
	public String getId(){ return this.id; }
	public void setId(String value){ this.id = value; }
	
	private String groupId;
	public String getGroupId(){ return this.groupId; }
	public void setGroupId(String value){ this.groupId = value; }
	
	private String name;
	public String getName(){ return this.name; }
	public void setName(String value){ this.name = value; }
	
	private String messageRegex;
	public String getMessageRegex(){ return this.messageRegex; }
	public void setMessageRegex(String value){ this.messageRegex = value; }
	
	private String exceptionRegex;
	public String getExceptionRegex(){ return this.exceptionRegex; }
	public void setExceptionRegex(String value){ this.exceptionRegex = value; }
	
	private String serverRegex;
	public String getServerRegex(){ return this.serverRegex; }
	public void setServerRegex(String value){ this.serverRegex = value; }
	
	private List<String> users;
	public List<String> getUsers()
	{
		if(this.users == null)
		{
			this.users = new ArrayList<String>();
		}
		return this.users;
	}
	public void setUsers(List<String> value){ this.users = value; }
}
