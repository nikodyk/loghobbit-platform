package com.loghobbit.entities.v1;

import java.util.ArrayList;
import java.util.List;

public class GroupNotificationSetting {
	private String groupId;
	public String getGroupId(){ return this.groupId; }
	public void setGroupId(String value){ this.groupId = value; }
	
	private boolean optOutNotifications;
	public boolean getOptOutNotifications(){ return this.optOutNotifications; }
	public void setOptOutNotifications(boolean value){ this.optOutNotifications = value; }
	
	private List<String> alwaysNotifyOn;
	public List<String> getAlwaysNotifyOn()
	{
		if(this.alwaysNotifyOn == null) this.alwaysNotifyOn = new ArrayList<String>();
		return this.alwaysNotifyOn;
	}
	public void setAlwaysNotifyOn(List<String> value){ this.alwaysNotifyOn = value; }
}
