package com.loghobbit.entities.v1;

import java.util.Date;

public class GroupStatistics implements Comparable<GroupStatistics> {
	public static final String DATE = "date";
	public static final String GROUP_ID = "groupId";
	
	private String groupId;
	public String getGroupId(){ return this.groupId; }
	public void setGroupId(String value){ this.groupId = value; }
	
	private long size;
	public long getSize(){ return this.size; }
	public void setSize(long value){ this.size = value; }
	
	private int logCount;
	public int getLogCount(){ return this.logCount; }
	public void setLogCount(int value){ this.logCount = value; }
	
	private Date date;
	public Date getDate(){ return this.date; }
	public void setDate(Date value){ this.date = value; }
	
	@Override
	public int compareTo(GroupStatistics other) {
		return this.date.compareTo(other.date);
	}
	
	@Override
	public String toString()
	{
		StringBuilder retval = new StringBuilder();
		retval.append(this.date.toString());
		retval.append(" ");
		retval.append(this.size);
		retval.append(" ");
		retval.append(this.logCount);
		return retval.toString();
	}
}
