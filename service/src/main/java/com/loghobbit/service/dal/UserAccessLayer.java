package com.loghobbit.service.dal;

import java.util.Calendar;
import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.IndexOperations;
import org.springframework.data.mongodb.core.index.Index;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.loghobbit.entities.v1.User;
import com.loghobbit.entities.v1.UserSystemOptions;
import com.loghobbit.entities.v1.requests.UpdateUserRequest;
import com.mongodb.WriteResult;

public class UserAccessLayer extends MongoDaoBase implements IUserAccessLayer {
	
	public void initialize()
	{
		IndexOperations user = this.getOperations().indexOps(User.class);
		user.ensureIndex(new Index().on(User.TEMPORARY_TOKEN, Direction.ASC));
	}
	
	public User login(String email, byte[] password)
	{
		Query query = new Query(Criteria.where(User.EMAIL).is(email.toLowerCase())
										.and(User.PASSWORD).is(password));
		
		String token = new ObjectId().toHexString();
		
		query.limit(1);
		User user = this.getOperations().findAndModify(query, new Update().set(User.SECURITY_TOKEN, token), User.class);
		if(user == null)
		{
			return null;
		}
		
		user.setPassword(null);
		user.setSecurityToken(token);
		return user;
	}

	@Override
	public boolean doesEmailExist(String email) {
		User user = this.getOperations().findById(email, User.class);
		return user != null;
	}

	@Override
	public void registerUser(User user) {
		user.setSecurityToken(new ObjectId().toHexString());
		this.getOperations().insert(user);
	}

	@Override
	public String getUserId(String token) {
		Query query = new Query(Criteria.where(User.SECURITY_TOKEN).is(token));
		query.fields()
				.exclude(User.PASSWORD)
				.exclude(User.FULL_NAME);
		
		User user = this.getOperations().findOne(query, User.class);

		return (user == null)? null : user.getEmail();
	}

	@Override
	public void updateSubscription(String userId, UserSystemOptions systemOptions) {
		Query query = new Query(Criteria.where(User.EMAIL).is(userId));
		
		this.getOperations().updateFirst(query, 
										new Update().set(User.SYSTEM_OPTIONS, systemOptions), 
										User.class);
	}

	@Override
	public User getUserForCleanup() {
		Date executeDate = new Date();
		Calendar lastCleanupThreashold = Calendar.getInstance();
		lastCleanupThreashold.add(Calendar.DAY_OF_YEAR, -1);
		return this.getOperations().findAndModify(new Query(Criteria.where(User.LAST_CLEANUP).lt(lastCleanupThreashold.getTime().getTime())), 
												new Update().set(User.LAST_CLEANUP, executeDate.getTime()), 
												User.class);
	}

	@Override
	public User getUserFromId(String userId) {
		return this.getOperations().findById(userId, User.class);
	}

	@Override
	public boolean updateUser(UpdateUserRequest updates, byte[] oldPassword, byte[] newPassword, String userId)
	{
		Update update = new Update();
		Query query = new Query(Criteria.where(User.EMAIL).is(userId));
		if(updates.getFullName() != null)
		{
			update.set(User.FULL_NAME, updates.getFullName());
		}
		if(newPassword != null)
		{
			query.addCriteria(Criteria.where(User.PASSWORD).is(oldPassword));
			update.set(User.PASSWORD,  newPassword);
		}
		
		WriteResult result = this.getOperations().updateFirst(query, update, User.class);
		
		return result.getN() > 0;
	}

	@Override
	public String getSSOToken(String userId)
	{
		ObjectId token = new ObjectId();
		this.getOperations().updateFirst(new Query(Criteria.where(User.EMAIL).is(userId)), new Update().set(User.TEMPORARY_TOKEN, token.toHexString()), User.class);
		return token.toHexString();
	}

	@Override
	public User validateToken(String token) {
		User user = this.getOperations().findAndModify(new Query(Criteria.where(User.TEMPORARY_TOKEN).is(token)), new Update().set(User.TEMPORARY_TOKEN, null), User.class);
		return user;
	}

	@Override
	public void userVerified(String userId) {
		Update update = new Update().set(User.IS_VALIDATED, true)
									.set(User.EMAIL_VERIFICATION_TOKEN, null);
		
		this.getOperations().updateFirst(new Query(Criteria.where(User.EMAIL).is(userId)), update, User.class);
	}
}
