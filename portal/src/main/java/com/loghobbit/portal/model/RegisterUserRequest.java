package com.loghobbit.portal.model;

public class RegisterUserRequest {
	private String fullName;
	public String getFullName(){ return this.fullName; }
	public void setFullName(String value){ this.fullName = value; }
	
	private String email;
	public String getEmail(){ return this.email; }
	public void setEmail(String value){ this.email = (value == null)? value : value.toLowerCase(); }
	
	private String password;
	public String getPassword(){ return this.password; }
	public void setPassword(String value){ this.password = value; }
}
