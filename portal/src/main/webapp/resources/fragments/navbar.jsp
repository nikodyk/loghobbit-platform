<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<script type="text/javascript" src="/resources/js/jquery-2.1.4.min.js">
</script>

<input type="hidden" id="service-endpoint" value="${serviceEndpoint}"/>
<nav class="navbar navbar-inverse navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<img src="/resources/images/Character_Small.png" class="menu-logo"/>
			<a class="navbar-brand"
				href="http://getbootstrap.com/examples/starter-template/#">LogHobbit</a>
		</div>
		<div id="navbar" class="collapse navbar-collapse">
			<ul class="nav navbar-nav">
				<c:choose>
					<c:when test="${loggedIn}">
						<li id="home"><a href="/home">Home</a></li>
						<li id="groups"><a href="/groups">Groups</a></li>
						<li id="notifications"><a href="/notifications">Notifications <span class="notificationCount"></span></a></li>
						<li id="walkthrough"><a href="http://blog.loghobbit.com/?page_id=31">Walk Through</a></li>
						<c:choose>
							<c:when test="${ sessionScope.accountUrl == '/' }">
							</c:when>
							<c:otherwise>
								<li id="account"><a href="${sessionScope.accountUrl }?token=${sessionScope.user.securityToken}">Account</a></li>
							</c:otherwise>
						</c:choose>
						<li>
							<div class="pull-right" style="margin-top:7px;margin-left:10px">
								<a class="btn btn-primary" href="/logout"><b>Logout</b></b></a>
							</div>
						</li>
					</c:when>
				</c:choose>
			</ul>
			<script type="text/javascript">
				var page = "${page}";
				$("#" + page).addClass("active");
			</script>
		</div>
		<!--/.nav-collapse -->
	</div>
</nav>
<c:if test="${sessionScope.user.isValidated == false }">
<div class="nav-warning bg-warning text-center">
	<b>Your account has not been verified. You will not be able to log to your own log groups until you've verified your email address.
	<a href="/verify/resend">Resend verification email</a></b>
	<br/>
	<b>
	<c:choose>
		<c:when test="${requestScope.emailSent == 'true' }">
			Email Sent	
		</c:when>
		<c:when test="${requestScope.emailSent == 'failed' }">
			Email could not be sent
		</c:when>
	</c:choose>
	</b>
</div>
</c:if>
