package com.loghobbit.service.clients;


import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.loghobbit.entities.v1.User;
import com.loghobbit.entities.v1.requests.GetUserDetailsRequest;
import com.loghobbit.entities.v1.requests.SubscriptionUpdateRequest;

public class SubscriptionClient extends ServiceClientBase {
	private static final ObjectMapper mapper = new ObjectMapper();
	private static final Logger logger = Logger.getLogger(SubscriptionClient.class);
	
	public SubscriptionClient(String endpoint, HttpClient client)
	{
		super(endpoint, client);
	}
	
	public boolean setUserThreasholds(SubscriptionUpdateRequest updateRequest)
	{
		try
		{
			HttpPost post = new HttpPost(this.endpoint + "/v1/subscription/update");
			post.setEntity(new StringEntity(mapper.writeValueAsString(updateRequest)));
			post.setHeader("Content-Type", "application/json");
			post.setHeader("Accept", "application/json");
			//post.setHeader("Accepts", "application/json");
			
			HttpResponse response = this.client.execute(post);
			
			if(response == null || response.getStatusLine().getStatusCode() != 204)
			{
				return false;
			}
		}
		catch(Exception e)
		{
			logger.error("An error occured while updating the user's subscription", e);
			return false;
		}

		return true;
	}

	public User getUserDetails(String userToken, String applicationId) throws Exception {
		GetUserDetailsRequest request = new GetUserDetailsRequest()
											.setApplicationId(applicationId)
											.setUserToken(userToken);
		HttpPost post = new HttpPost(this.endpoint + "/v1/subscription/details");
		post.setEntity(new StringEntity(mapper.writeValueAsString(request)));
		post.setHeader("Content-Type", "application/json");
		post.setHeader("Accept", "application/json");
		post.setHeader("token", userToken);
		
		HttpResponse response = this.client.execute(post);
			
		if(response == null || response.getStatusLine().getStatusCode() != 200)
		{
			return null;
		}
		
		User user = mapper.readValue(response.getEntity().getContent(), com.loghobbit.entities.v1.User.class);
		response.getEntity().getContent().close();
		
		return user;
	}
}
