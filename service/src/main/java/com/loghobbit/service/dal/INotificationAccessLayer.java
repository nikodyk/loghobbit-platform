package com.loghobbit.service.dal;

import java.util.List;

import com.loghobbit.entities.v1.Notification;

public interface INotificationAccessLayer {
	/**
	 * Gets all user notifications
	 * @param userId the user's id
	 * @return notifications for the user
	 */
	List<Notification> getUserNotifications(String userId);
	
	/**
	 * Gets the count of pending notifications
	 * @param userId the user's id
	 * @return a count of pending notifications
	 */
	int getPendingUserNotificationCount(String userId);
	
	/**
	 * Adds a notification for a user
	 * @param userId the user's id
	 * @param notification the notification to add
	 */
	void addUserNotification(String userId, Notification notification);

	/**
	 * Gets a notification for a group and user
	 * @param groupId the id of the group
	 * @param userId the id of the user
	 * @return
	 */
	Notification getUserNotificationForGroup(String groupId, String userId);

	/**
	 * Delete the user notification for the group and user
	 * @param groupId the id of the group
	 * @param userId the id of the user
	 */
	void deleteUserNotification(String groupId, String userId);
}
