package com.loghobbit.entities.v1.requests;

import com.loghobbit.entities.v1.UserSystemOptions;

public class SubscriptionUpdateRequest {
	public static final int NO_LIMIT = -1;
	
	/**
	 * The id of the application making the call.  This is generally
	 * the application which owns the system configuration
	 */
	private String applicationId;
	public String getApplicationId(){ return this.applicationId; }
	public void setApplicationId(String value){ this.applicationId = value; }
	
	/**
	 * The id of the user (typically the user's email address)
	 */
	private String userId;
	public String getUserId(){ return this.userId; }
	public void setUserId(String value){ this.userId = value; }
	
	/**
	 * The system options for the user
	 */
	private UserSystemOptions systemOptions;
	public UserSystemOptions getSystemOptions(){ return this.systemOptions; }
	public void setSystemOptions(UserSystemOptions value){ this.systemOptions = value; }
}
