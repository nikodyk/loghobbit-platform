package com.loghobbit.service.dal;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;

public class MongoConnection {
	private String host;
	public String getHost(){ return this.host; }
	public MongoConnection setHost(String value)
	{
		this.host = value;
		return this;
	}
	
	private int port;
	public int getPort(){ return this.port; }
	public MongoConnection setPort(int value)
	{
		this.port = value;
		return this;
	}
	
	private String failoverHost;
	public String getFailoverHost(){ return this.failoverHost; }
	public MongoConnection setFailoverHost(String value)
	{
		this.failoverHost = value;
		return this;
	}
	
	private int failoverPort;
	public int getFailoverPort(){ return this.failoverPort; }
	public MongoConnection setFailoverPort(int value)
	{
		this.failoverPort = value;
		return this;
	}
	
	private String database;
	public String getDatabase(){ return this.database; }
	public MongoConnection setDatabase(String value)
	{
		this.database = value;
		return this;
	}
	
	private String user;
	public String getUser(){ return this.user; }
	public MongoConnection setUser(String value)
	{
		this.user = value;
		return this;
	}
	
	private String password;
	public MongoConnection setPassword(String value)
	{
		this.password = value;
		return this;
	}
	
	private MongoClient mongoClient;
	protected MongoOperations connection;
	
	public void initialize() throws UnknownHostException
	{
		List<ServerAddress> servers = new ArrayList<ServerAddress>();
		servers.add(new ServerAddress(this.host, port));
		if(this.failoverHost != null && this.failoverHost.trim().length() > 0)
		{
			servers.add(new ServerAddress(this.failoverHost, this.failoverPort));
		}
		
		if(this.user != null && !this.user.isEmpty())
		{
			MongoCredential credential = MongoCredential.createMongoCRCredential(this.user, this.database, this.password.toCharArray());
			this.password = null;
			
			this.mongoClient = new MongoClient(servers, Arrays.asList(credential));
		}
		else
		{
			this.mongoClient = new MongoClient(servers);
		}
		
		this.connection = new MongoTemplate(this.mongoClient, this.database);
	}
	
	public MongoOperations getOperations()
	{
		return this.connection;
	}
}
