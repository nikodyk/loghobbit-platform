package com.loghobbit.service.workers;

import org.springframework.beans.factory.annotation.Autowired;

import com.loghobbit.entities.v1.Group;
import com.loghobbit.entities.v1.GroupNotificationSetting;
import com.loghobbit.entities.v1.TeamMember;
import com.loghobbit.service.dal.IGroupAccessLayer;
import com.loghobbit.service.dal.ILogAccessLayer;
import com.loghobbit.service.dal.entities.UniqueLogEntry;
import com.loghobbit.service.v1.NotificationSvc;

public class AnalyzeUniqueLogEntryWorker extends Thread {
	private static final GroupNotificationSetting DEFAULT_SETTINGS = new GroupNotificationSetting();
	
	@Autowired
	private ILogAccessLayer logAccessLayer;
	public AnalyzeUniqueLogEntryWorker setLogAccessLayer(ILogAccessLayer value)
	{
		this.logAccessLayer = value;
		return this;
	}
	
	@Autowired
	private IGroupAccessLayer groupAccessLayer;
	public AnalyzeUniqueLogEntryWorker setGroupAccessLayer(IGroupAccessLayer value)
	{
		this.groupAccessLayer = value;
		return this;
	}
	
	@Autowired
	private NotificationSvc notificationSvc;
	public AnalyzeUniqueLogEntryWorker setNotificationSvc(NotificationSvc value)
	{
		this.notificationSvc = value;
		return this;
	}
	
	private UniqueLogEntry entry;
	public UniqueLogEntry getEntry(){ return this.entry; }
	public AnalyzeUniqueLogEntryWorker setEntry(UniqueLogEntry value)
	{
		this.entry = value;
		return this;
	}
	
	public AnalyzeUniqueLogEntryWorker()
	{
		this.setName("AnalyseUniqueLogEntryWorker");
	}
	
	@Override
	public void run()
	{
		if(this.entry == null)
		{
			return;
		}
		
		UniqueLogEntry existingEntry = this.logAccessLayer.insertUniqueMessage(this.entry);
		if(existingEntry != this.entry)
		{
			return;
		}
		
		Group group = this.groupAccessLayer.getGroupById(existingEntry.getGroupId());
		if(group == null)
		{
			return;
		}
		
		for(TeamMember member : group.getTeamMembers())
		{
			if(!member.getAccepted())
			{
				continue;
			}
			
			GroupNotificationSetting memberSetting = this.groupAccessLayer.getGroupMemberNotificationSetting(group.getId(), member.getMemberId());
			if(memberSetting == null)
			{
				memberSetting = DEFAULT_SETTINGS;
			}
			
			if(memberSetting.getOptOutNotifications())
			{
				continue;
			}
			
			this.notificationSvc.sendLogAlert(this.entry.getLogEntry(), member.getMemberId(), group, memberSetting);
		}
	}
}
