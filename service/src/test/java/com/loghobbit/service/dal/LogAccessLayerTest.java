package com.loghobbit.service.dal;

import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.loghobbit.entities.Level;
import com.loghobbit.entities.LogEntry;
import com.loghobbit.entities.v1.GroupStatistics;
import com.loghobbit.service.utils.DateUtils;

public class LogAccessLayerTest {
	private LogAccessLayer logAccessLayer;
	
	@Before
	public void setup() throws UnknownHostException
	{
		MongoConnection connection = new MongoConnection();
		connection.setDatabase("loghobbit");
		connection.setHost("localhost");
		connection.setPort(27017);
		connection.initialize();
		
		this.logAccessLayer = new LogAccessLayer();
		this.logAccessLayer.setConnection(connection);
		this.logAccessLayer.initialize();
	}
	
	@Test
	public void groupSizeDbTest()
	{
		Date date = new Date();
		Calendar logDate = Calendar.getInstance();
		logDate.add(Calendar.SECOND, 1);
		LogEntry entry = new LogEntry();
		entry.setAccessKey("");
		entry.setDate(logDate.getTime());
		entry.setLevel(Level.error);
		entry.setLogName("test");
		entry.setMessage("Map reduce log");
		entry.setServer("database test");
		entry.setSimpleDate(logDate.toString());
		
		this.logAccessLayer.addLogEntry(entry, "unit test");
		
		logDate.add(Calendar.SECOND, 1);
		List<GroupStatistics> stats = this.logAccessLayer.getSizeOfLogsInGroup(Arrays.asList("unit test"), date, logDate.getTime());
		Assert.assertNotNull(stats);
		
		for(GroupStatistics stat : stats)
		{
			Assert.assertEquals("Stats Date", DateUtils.getTodayBOD(), stat.getDate());
			Assert.assertNotEquals("Size", 0, stat.getSize());
			Assert.assertNotEquals("Log Count", 0, stat.getLogCount());
		}
	}
}
