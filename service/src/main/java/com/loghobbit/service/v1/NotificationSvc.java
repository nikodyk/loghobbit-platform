package com.loghobbit.service.v1;

import java.io.IOException;
import java.security.InvalidParameterException;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.amazonaws.util.IOUtils;
import com.loghobbit.entities.LogEntry;
import com.loghobbit.entities.v1.Group;
import com.loghobbit.entities.v1.GroupNotificationSetting;
import com.loghobbit.entities.v1.Notification;
import com.loghobbit.entities.v1.NotificationResponse;
import com.loghobbit.entities.v1.NotificationType;
import com.loghobbit.entities.v1.TeamMember;
import com.loghobbit.entities.v1.TeamMemberType;
import com.loghobbit.entities.v1.User;
import com.loghobbit.service.AuthenticationFilter;
import com.loghobbit.service.MailService;
import com.loghobbit.service.dal.IGroupAccessLayer;
import com.loghobbit.service.dal.INotificationAccessLayer;
import com.loghobbit.service.dal.IUserAccessLayer;

@Path("/notifications")
public class NotificationSvc {
	private String groupInvite;
	private String emailVerification;
	private String newLogEntry;
	private String acceptedInvite;
	private String declinedInvite;
	
	@Value("${portal.url}")
	private String portalUrl;
	public NotificationSvc setPortalUrl(String value)
	{ 
		this.portalUrl = value;
		return this;
	}
	
	@Autowired
	private INotificationAccessLayer notificationAccessLayer;
	public NotificationSvc setNotificationAccessLayer(INotificationAccessLayer value)
	{
		this.notificationAccessLayer = value;
		return this;
	}
	
	@Autowired
	private IGroupAccessLayer groupAccessLayer;
	public NotificationSvc setGroupAccessLayer(IGroupAccessLayer value)
	{
		this.groupAccessLayer = value;
		return this;
	}
	
	@Autowired
	private IUserAccessLayer userAccessLayer;
	public NotificationSvc setUserAccessLayer(IUserAccessLayer value)
	{
		this.userAccessLayer = value;
		return this;
	}
	
	@Autowired
	private MailService mailService;
	public NotificationSvc setMailService(MailService value)
	{
		this.mailService = value;
		return this;
	}
	
	public void initialize() throws IOException
	{
		this.groupInvite = IOUtils.toString(this.getClass().getResourceAsStream("/messages/JoinGroup.html"));
		this.emailVerification = IOUtils.toString(this.getClass().getResourceAsStream("/messages/VerifyEmail.html"));
		this.newLogEntry = IOUtils.toString(this.getClass().getResourceAsStream("/messages/NewLogNotification.html"));
		this.acceptedInvite = IOUtils.toString(this.getClass().getResourceAsStream("/messages/AcceptedInviteNotification.html"));
		this.declinedInvite = IOUtils.toString(this.getClass().getResourceAsStream("/messages/DeclinedInviteNotification.html"));
	}
	
	@GET
	public List<Notification> getNotifications(@HeaderParam(AuthenticationFilter.USER_DATA) String userId)
	{
		List<Notification> retval = this.notificationAccessLayer.getUserNotifications(userId);
		return retval;
	}
	
	@PUT
	@Path("/group/{groupId}")
	public void groupNotificationResponse(String responseText, @PathParam("groupId")String groupId, @HeaderParam(AuthenticationFilter.USER_DATA) String userId)
	{
		if(responseText == null)
		{
			throw new InvalidParameterException("The response cannot be null");
		}
		if(groupId == null || groupId.trim().length() == 0)
		{
			throw new InvalidParameterException("The group id must be valid");
		}
		NotificationResponse response = null;
		try
		{
			response = Enum.valueOf(NotificationResponse.class, responseText);
		}
		catch(Throwable e)
		{
			throw new InvalidParameterException("The response value is invalid " + responseText);
		}
		
		Notification notification = this.notificationAccessLayer.getUserNotificationForGroup(groupId, userId);
		if(notification == null)
		{
			throw new IllegalStateException("There is no notification for the group specified");
		}
		
		Group group = this.groupAccessLayer.getGroupById(groupId);
		if(group == null)
		{
			this.notificationAccessLayer.deleteUserNotification(groupId, userId);
			throw new IllegalStateException("The group this invitation is for is no longer available");
		}
		
		User user = this.userAccessLayer.getUserFromId(userId);
		if(user == null)
		{
			throw new IllegalStateException("Unable to get the current user's profile");
		}
		
		for(TeamMember member : group.getTeamMembers())
		{
			if(member.getMemberId().equals(userId))
			{
				member.setAccepted(response == NotificationResponse.Accept);
				break;
			}
		}
		this.groupAccessLayer.updateGroupMembers(group);
		this.notificationAccessLayer.deleteUserNotification(groupId, userId);
		
		for(TeamMember member : group.getTeamMembers())
		{
			if((member.getType() == TeamMemberType.admin ||
					member.getType() == TeamMemberType.owner) &&
					member.getAccepted() &&
					!member.getMemberId().equals(userId))
			{
				String verb = (response == NotificationResponse.Accept)? "accepted" : "declined";
				String body = (response == NotificationResponse.Accept)? this.acceptedInvite : this.declinedInvite;
				body = body.replace("{accepteeName}", user.getFullName())
							.replace("{groupName}", group.getName())
							.replace("{portal.url}", this.portalUrl);
				
				this.mailService.sendMail(member.getMemberId(), String.format("%s %s the invite to join %s", user.getFullName(), verb, group.getName()), body);
			}
		}
	}
	
	public void sendJoinGroupNotification(User fromUser, String toEmail, Group group)
	{
		Notification notification = new Notification();
		notification.setGroup(group);
		notification.setFrom(fromUser);
		notification.setType(NotificationType.JoinGroup);
		this.notificationAccessLayer.addUserNotification(toEmail, notification);
		
		this.mailService.sendMail(toEmail, 
								  "You've been invited to " + group.getName(), 
								  this.groupInvite.replace("{inviter}", fromUser.getFullName())
									.replace("{portal.url}", this.portalUrl)
									.replace("{group name}", group.getName())
									.replace("{group id}", group.getId()));
	}

	@POST
	@Path("email/verify")
	public void sendEmailVerifications(String email, String emailVerificationToken) {
		User user = this.userAccessLayer.getUserFromId(email);
		if(user.getIsValidated())
		{
			return;
		}
		
		this.mailService.sendMail(email, "Verify your LogHobbit email address", 
									this.emailVerification
											.replace("{portal.url}", this.portalUrl)
											.replace("{fullName}", user.getFullName())
											.replace("{verificationtoken}", emailVerificationToken));
	}
	
	public void sendLogAlert(LogEntry entry, String memberId, Group group, GroupNotificationSetting memberSetting)
	{
		this.mailService.sendMail(memberId, 
								"New Error: " + entry.getMessage(), 
								this.newLogEntry.replace("{portal.url}", this.portalUrl)
										.replace("{group.name}", group.getName())
										.replace("{server.name}", entry.getServer())
										.replace("{message}", entry.getMessage())
										.replace("{exception}", (entry.getException() == null)? "" : entry.getException()));
	}
}
