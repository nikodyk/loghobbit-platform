package com.loghobbit.service.dal.entities;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.loghobbit.entities.LogEntry;

@Document(collection="LogEntry")
public class LogEntryStorage {
	public static final String ACCESS_KEY = "logEntry.accessKey";
	public static final String DATE_MS = "dateMS";
	public static final String GROUP_ID = "groupId";
	public static final String LEVEL = "logEntry.level";
	public static final String EXCEPTION = "logEntry.exception";
	public static final String MESSAGE = "logEntry.message";
	public static final String SERVER = "logEntry.server";
	public static final String SIZE = "size";
	public static final String ID = "_id";
	
	@Id
	private ObjectId id;
	public ObjectId getId(){ return this.id; }
	public void setId(ObjectId value){ this.id = value; }
	
	private String groupId;
	public String getGroupId(){ return this.groupId; }
	public void setGroupId(String value){ this.groupId = value; }
	
	private long dateMS;
	public long getDateMS(){ return this.dateMS; }
	public void setDateMS(long value){ this.dateMS = value; }
	
	private LogEntry logEntry;
	public LogEntry getLogEntry(){ return this.logEntry; }
	public void setLogEntry(LogEntry value){ this.logEntry = value; }
	
	private long size;
	public long getSize(){ return this.size; }
	public void setSize(long value){ this.size = value; }
}
