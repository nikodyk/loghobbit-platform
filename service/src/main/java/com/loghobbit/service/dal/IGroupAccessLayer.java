package com.loghobbit.service.dal;

import java.util.Date;
import java.util.List;

import com.loghobbit.entities.v1.Group;
import com.loghobbit.entities.v1.GroupNotificationSetting;
import com.loghobbit.entities.v1.GroupStatistics;

public interface IGroupAccessLayer {
	
	void addOrganization(Group organization);

	/**
	 * Gets a organizations based on the email
	 * @param email the user's email
	 * @return a list of organizations
	 */
	List<Group> getGroupsOwnedByUser(String email);

	/**
	 * 
	 * @param accessKey the access key for the group
	 * @return a group associated with the access key
	 */
	Group getGroupByAccessKey(String accessKey);

	/**
	 * Gets a list of groups the user is a member of
	 * @param email the email of the user
	 * @return groups the user is a member of
	 */
	List<Group> getGroupsForMember(String email);

	/**
	 * Updates a group
	 * @param group updates a group
	 */
	void updateGroup(Group group);

	/**
	 * Creates a group
	 * @param group the group to create
	 * @return the id for the group
	 */
	String addGroup(Group group);

	/**
	 * Gets a group by its id
	 * @param id the id of the group
	 * @return the group
	 */
	Group getGroupById(String id);

	/**
	 * Deletes a group
	 * @param groupId the id of the group to delete
	 */
	void deleteGroup(String groupId);


	/**
	 * Updates a group team members
	 * @param group the group to update
	 */
	void updateGroupMembers(Group group);

	/**
	 * Gets a group member's notification settings
	 * @param groupId the id of the group
	 * @param userId the id of the user
	 * @return the notification settings
	 */
	GroupNotificationSetting getGroupMemberNotificationSetting(String groupId, String userId);

	/**
	 * Gets a group that needs to be analyzed for previous usage
	 * @param tomorrowsDate the date for tomorrow
	 * @return a group that needs to be analyzed
	 */
	Group getGroupToAnalyze(Date tomorrowsDate);

	/**
	 * Gets the latest statistic stored for the group
	 * @param groupId the id of the group
	 * @return the latest stat
	 */
	GroupStatistics getLastStatisticForGroup(String groupId);

	/**
	 * Adds a group stat to the database
	 * @param stat the stat to add
	 */
	void addGroupStat(GroupStatistics stat);

	/**
	 * Gets the group stats for the specified date range
	 * @param groupIds the ids of the groups to retrieve
	 * @param start the beginning of the date window
	 * @param end the end of the date window
	 * @return
	 */
	List<GroupStatistics> getGroupsStats(List<String> groupIds, Date start, Date end);
}
