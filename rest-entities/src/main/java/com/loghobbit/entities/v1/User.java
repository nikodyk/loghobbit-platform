package com.loghobbit.entities.v1;

import org.springframework.data.annotation.Id;

public class User {
	public static final String EMAIL = "_id";
	public static final String PASSWORD = "password";
	public static final String SECURITY_TOKEN = "securityToken";
	public static final String FULL_NAME = "fullName";
	public static final String SUBSCRIPTION_LEVEL = "subscriptionLevel";
	public static final String LAST_CLEANUP = "lastCleanupDate";
	public static final String SYSTEM_OPTIONS = "systemOptions";
	public static final String TEMPORARY_TOKEN = "temporaryToken";
	public static final String IS_VALIDATED = "isValidated";
	public static final String EMAIL_VERIFICATION_TOKEN = "emailVerificationToken";
	
	private String fullName;
	public String getFullName(){ return this.fullName; }
	public void setFullName(String value){ this.fullName = value; }
	
	@Id
	private String email;
	public String getEmail(){ return this.email; }
	public void setEmail(String value){ this.email = (value == null)? value : value.toLowerCase(); }
	
	/**
	 * This is a md5 hash of the user's password
	 */
	private byte[] password;
	public byte[] getPassword(){ return this.password; }
	public void setPassword(byte[] value){ this.password = value; }
	
	/**
	 * This is the system's way of identifying if the user validated their email
	 * address
	 */
	private boolean isValidated;
	public boolean getIsValidated(){ return this.isValidated; }
	public void setIsValidated(boolean value){ this.isValidated = value; }
	
	/**
	 * This is the token used to authenticate the user
	 */
	private String securityToken;
	public String getSecurityToken(){ return this.securityToken; }
	public void setSecurityToken(String value){ this.securityToken = value; }
	
	/**
	 * The last date the logs were cleaned up
	 */
	private long lastCleanupDate;
	public long getLastCleanupDate(){ return this.lastCleanupDate; }
	public void setLastCleanupDate(long value){ this.lastCleanupDate = value; }
	
	/**
	 * The system subscription options
	 */
	private UserSystemOptions systemOptions;
	public UserSystemOptions getSystemOptions(){ return this.systemOptions; }
	public void setSystemOptions(UserSystemOptions value){ this.systemOptions = value; }
	
	/**
	 * The temporary token for sso
	 */
	private String temporaryToken;
	public String getTemporaryToken(){ return this.temporaryToken; }
	public void setTemporaryToken(String value){ this.temporaryToken = value; }
	
	/**
	 * This token is used to verify the user's email address.  This is null if the email address has
	 * been verified.
	 */
	private String emailVerificationToken;
	public String getEmailVerificationToken(){ return this.emailVerificationToken; }
	public void setEmailVerificationToken(String value){ this.emailVerificationToken = value; }
}
