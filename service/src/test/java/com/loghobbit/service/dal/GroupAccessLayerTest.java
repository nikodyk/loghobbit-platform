package com.loghobbit.service.dal;

import java.net.UnknownHostException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.loghobbit.entities.v1.GroupStatistics;

public class GroupAccessLayerTest {
	private GroupAccessLayer groupAccessLayer;
	
	@Before
	public void setup() throws UnknownHostException
	{
		MongoConnection connection = new MongoConnection();
		connection.setDatabase("loghobbit");
		connection.setHost("localhost");
		connection.setPort(27017);
		connection.initialize();
		
		this.groupAccessLayer = new GroupAccessLayer();
		this.groupAccessLayer.setConnection(connection);
		this.groupAccessLayer.initialize();
	}
	
	@Test
	public void get()
	{
		GroupStatistics stat = this.groupAccessLayer.getLastStatisticForGroup("561f155eafe3bb5139894d95");
		//Assert.assertNotNull(stat);
	}
}
