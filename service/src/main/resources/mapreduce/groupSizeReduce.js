function(key, logEntries)
{
	var size = 0;
	var itemCount = (logEntries.length == 0)? 1 : logEntries.length;
	var date = logEntries[0].date;
	var groupId = logEntries[0].groupId;
	
	for(var i = 0; i < itemCount; i++)
	{
		var item = logEntries[i];
		size += item.size;
	}
	
	var retval = {
			groupId: groupId,
			logCount: (itemCount == Math.NaN)? 1 : itemCount,
			size: size,
			date: date
	};
	return retval;
}