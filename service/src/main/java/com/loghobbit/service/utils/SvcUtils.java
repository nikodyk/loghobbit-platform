package com.loghobbit.service.utils;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

public class SvcUtils {
	public static Response constructError(Status status, String errorMessage)
	{
		return Response.status(status).entity(errorMessage).type(MediaType.TEXT_PLAIN).build();
	}
}
