# README #

### License ###

See file LICENSE

### What is this repository for? ###

This is the LogHobbit open source log management platform.  It contains all the major server side components needed for running the LogHobbit service.  These services allow logging from the Log4J and Log4Net components, and viewing the resulting logs.

### How do I get set up? ###

* Clone repository
* Outside of the repository create a "configurations" folder
** Create a "portal" folder
*** Copy configuration files from the portal project to this folder, and modify the configurations to suite your needs
** Create a "service" folder
*** Copy configuration files from the service project to this folder, and modify the configurations to suite your needs
* build with maven using command 'mvn clean install -Dbuild.type=prod'

If Running Locally (or running the tests)
* Create a user on your local platform
* Create a group and modify its access token to be 55b59bb28902680c7ad230ea
* Verify communication by ensuring there are no deserialization errors from the service and no errors in the console log

Starting a local instance
* Start mongod on your system
* navigate to the service and start it using command 'mvn tomcat7:run'
* navigate to the portal and start it using command 'mvn tomcat7:run'
* use a web browser to navigate to 'http://localhost:1200/" and sign up
* After creating a user use a mongodb browser to modify the group's access token to '55b59bb28902680c7ad230ea'

### Contribution guidelines ###

* When contributing, make a branch.  When your feature is complete make a pull request
* All system tests should use the token specified above
* Ensure that all tests pass before making a pull request.  Commenting out test cases will cause the pull request to become immediatly rejected

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact