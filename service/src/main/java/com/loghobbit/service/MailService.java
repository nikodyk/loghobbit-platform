package com.loghobbit.service;

import java.io.IOException;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.Message.RecipientType;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;

public class MailService {
	private Logger logger = Logger.getLogger(MailService.class);
	private static final String FROM_EMAIL = "notifications@loghobbit.com";
	private Session session;
	
	public void initialize() throws IOException
	{
		Properties properties = new Properties();
		properties.load(this.getClass().getResourceAsStream("/smtp.properties"));
		this.session = Session.getInstance(properties, new MailAuthenticator(properties.getProperty("mail.smtp.user"), properties.getProperty("mail.smtp.password")));
	}
	
	public void sendMail(String toEmail, String title, String body)
	{
		MimeMessage message = new MimeMessage(this.session);
		try {
			message.setFrom(new InternetAddress(FROM_EMAIL));
			message.setRecipient(RecipientType.TO, new InternetAddress(toEmail));
			message.setSubject(title);
			message.setContent(body, "text/html; charset=utf-8");
			
			Transport.send(message);
			
		} catch (MessagingException e) {
			logger.error("an error occured while sending mail", e);
		}
	}
}
