package com.loghobbit.service.exceptions;

import com.loghobbit.entities.v1.AlertCriteria;

public class AlertExistsException extends Exception {
	private static final long serialVersionUID = -6639635191512696433L;
	
	private AlertCriteria existingAlert;
	public AlertCriteria getExistingAlert(){ return this.existingAlert; }
	public AlertExistsException setExistingAlert(AlertCriteria value)
	{
		this.existingAlert = value;
		return this;
	}
}
