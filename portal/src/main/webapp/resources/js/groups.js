function groupDetails(element)
{
	this.element = $(element);
	this.contentArea = this.element.find("#group-content");
	this.groupId = this.element.find("#group-id").val();
	this.memberType = this.element.find("#member-type").val();
	this.inviteeEmail = this.element.find("#group-invite-text");
	this.membersElement = this.element.find("#group-members");
	this.members = new Array();
	
	this.toggle = function(){
		if(this.element.css('display') == 'none'){
			this.show();
		}
		else{
			this.hide();
		}
	}
	this.show = function(){
		this.element.slideDown();
		this.contentArea.slideDown();
	};
	
	this.hide = function(){
		this.contentArea.slideUp();
		this.element.slideUp();
	};
	
	this.teamRoleChanged = function(selectChanged){
		var results = $($($(selectChanged).parent()).parent()).find(".fa-envelope");
		if(selectChanged.value != 'member' && results.length > 0)
		{
			alert('The member must accept before they can be made an admin or owner')
			selectChanged.value = 'member';
			return;
		}
		if(selectChanged.value == 'owner')
		{
			this.element.find("#group-members #member-role").each(function(){
				if(this.value == "owner")
				{
					this.value = 'member';
				}
			});
			selectChanged.value = 'owner';
		}
	};
	
	this.invite = function(){
		if(this.memberType == 'member')
		{
			alert('Members cannot invite other users.  You must be at least an admin');
			return;
		}
		
		var content = $($("#group-dialog-member-template").html());
		var email = this.inviteeEmail.val();
		
		$(content.find("#memberId")).text(email);
		
		this.setupGroupMemberElement(content);
		content.show();
		
		this.membersElement.append(content);
	};
	
	this.setupGroupMemberElement = function(jMember)
	{
		var self = this;
		jMember.find("#member-role").on('change', function(){ 
			self.teamRoleChanged(this);
		});
	};
	
	this.updateSucceeded = function(data){
		var saveButton = this.element.find("#details-update");
		saveButton.empty();
		saveButton.text("Update");
		if(this.groupId == "" && data != null)
		{
			window.location = "/home?groupId=" + data;
		}
		else
		{
			location.reload();
		}
	};
	
	this.updateFailed = function(data){
		var saveButton = this.element.find("#details-update");
		saveButton.empty();
		saveButton.text("Update");
	};
	
	this.update = function(){
		var saveButton = this.element.find("#details-update"); 
		saveButton.empty();
		saveButton.append('<img src="/resources/images/small-spinner.gif"/>')
		var name = this.element.find("#group-name").val();
		var description = this.element.find("#group-description").val();
		var accessKey = this.element.find("#group-access-key").val();
		
		var members = new Array();
		var membersArea = this.element.find("#group-members");
		var children = membersArea.children();
		for(var ii = 0; ii < children.length; ii++){
			var member = new Object();
			member.memberId = $(children[ii]).find("#memberId").text();
			var memberType = $(children[ii]).find("#memberType option:selected");
			member.type = memberType.val();
			member.accepted = $(children[ii]).find(".success-icon").length > 0;
			members[ii] = member;
		}
		
		var postData = JSON.stringify({ 
			id:this.groupId, 
			name:name,
			accessKey:accessKey,
			description:description, 
			teamMembers:members });
		var self = this;
		
		$.ajax({
			type : "PUT",
			url : "/group",
			data : postData,
			contentType: "application/json",
			success:function(data){
				self.updateSucceeded(data);
			},
			failed:function(){
				self.updateFailed();
			}
		});
	};
	
	var self = this;
	
	$('#detailsLink_' + this.groupId).click(function(){ self.toggle(); });
	this.element.find("#details-close").click(function(){ self.hide(); });
	this.element.find("#group-invite-button").click(function(){ self.invite(); })
	this.element.find("#details-update").click(function(){ self.update(); });
	this.element.find("#group-members .row").each(function(){
		self.setupGroupMemberElement($(this));
	});
}

var groupDetailsArray = new Array();
$(document).ready(function(){
	$(".group-details").each(function(key, value){
		groupDetailsArray[groupDetailsArray.length] = new groupDetails(value);
	});
});