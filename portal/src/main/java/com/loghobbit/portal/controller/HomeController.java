package com.loghobbit.portal.controller;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.HtmlUtils;

import com.loghobbit.entities.Level;
import com.loghobbit.entities.v1.AlertCriteria;
import com.loghobbit.entities.v1.Group;
import com.loghobbit.entities.v1.LogFilter;
import com.loghobbit.entities.v1.MessagePart;
import com.loghobbit.entities.v1.Notification;
import com.loghobbit.entities.v1.NotificationResponse;
import com.loghobbit.entities.v1.User;
import com.loghobbit.entities.v1.UserLogEntry;
import com.loghobbit.portal.filters.LoggedIn;
import com.loghobbit.portal.model.LogFilterRequest;
import com.loghobbit.portal.model.LogViewAction;
import com.loghobbit.service.clients.ServiceClientFactory;

@Controller
public class HomeController {
	private static Logger logger = Logger.getLogger(HomeController.class);
	private static final DateFormat DATE_PARSER = new SimpleDateFormat("yyyy-MM-dd");
	
	@Autowired
	@Qualifier("ServiceClientFactory")
	public ServiceClientFactory clientFactory;
	public HomeController setClientFactory(ServiceClientFactory value)
	{
		this.clientFactory = value;
		return this;
	}
	
	@LoggedIn
	@RequestMapping(value="/home", method= RequestMethod.GET)
	public String getHome(HttpServletRequest request, @RequestParam(required = false) String groupId, @RequestParam(required = false) Integer page, Model model) throws IOException, NoSuchAlgorithmException
	{
		User user = (User) request.getSession().getAttribute(SessionItems.USER);
		
		Group[] groups;
		try {
			groups = this.clientFactory.getGroupClient().getUsersGroups(user.getSecurityToken());
		} catch (Exception e) {
			model.addAttribute("GetGroupsError", true);
			return "user-level/home";
		}
		
		int selectedOffset = 0;
		if(groupId != null && groupId.length() > 0)
		{
			for(int i = 0; i < groups.length; i++)
			{
				if(groups[i].getId().equals(groupId))
				{
					selectedOffset = i;
					break;
				}
			}
		}
		
		request.getSession().setAttribute(SessionItems.GROUP, groups[selectedOffset]);
		if(page == null || page == 0)
		{
			request.getSession().setAttribute(SessionItems.LOG_REQUEST_DATE, new Date());
			request.getSession().setAttribute(SessionItems.LOG_LATEST_DATE, new Date());
		}
		request.getSession().setAttribute(SessionItems.LOG_ON_PAGE, 0);
		
		model.addAttribute("userId", user.getEmail());
		model.addAttribute("canHaveAdmins", (user.getSystemOptions() == null )? false : user.getSystemOptions().getCanHaveAdmins());
		model.addAttribute("selectedOffset", selectedOffset);
		model.addAttribute("groups", groups);
		model.addAttribute("group", groups[selectedOffset]);
		
		model.addAttribute("showHeader", true);
		getLogs(request, page, model);
		
		LogFilter filter  = (LogFilter)request.getSession().getAttribute(SessionItems.FILTER);
		List<Level> levels = null;
		List<MessagePart> messageParts = null;
		if(filter != null)
		{
			levels = filter.getLevels();
			messageParts = filter.getMessageParts();
		}
		
		//
		// Levels
		//
		if(levels == null || levels.size() == 0)
		{
			levels = Arrays.asList(Level.values());
		}
		for(Level level : levels)
		{
			model.addAttribute("filter_level_" + level.name(), true);
		}
		
		//
		// Message parts
		//
		if(messageParts == null || messageParts.size() == 0)
		{
			messageParts = Arrays.asList(MessagePart.values());
		}
		for(MessagePart part : messageParts)
		{
			model.addAttribute("filter_part_" + part.name(), true);
		}
		
		return "user-level/home";
	}
	
	@LoggedIn
	@RequestMapping(value="/home", method= RequestMethod.POST)
	public String postHome(LogFilterRequest filterRequest, AlertCriteria alertCriteria, HttpServletRequest request, @RequestParam(required = false) String groupId, @RequestParam(required = false) Integer page, @RequestParam(required = false) LogViewAction logViewAction, Model model) throws IOException, NoSuchAlgorithmException
	{
		User user = (User)request.getSession().getAttribute(SessionItems.USER);
		
		switch(logViewAction)
		{
		case addAlert:
			try {
				this.clientFactory.getLogClient().addAlert(alertCriteria, user);
			} catch (Exception e) {
				logger.warn("An error occured while adding an alert", e);
				model.addAttribute("error", "Could not add alert due to " + e.getMessage());
			}
			break;
		case removeAlert:
			break;
		case filter:
			handleFilterSetup(filterRequest, request, groupId, page, model);
			break;
		}
		
		return this.getHome(request, groupId, page, model);
	}
	
	private void handleFilterSetup(LogFilterRequest filterRequest, HttpServletRequest request, String groupId, Integer page, Model model)
	{
		LogFilter filter = new LogFilter();
		if(filterRequest.getMessageRegex() != null && filterRequest.getMessageRegex().length() > 0)
		{
			filter.setMessageRegex(filterRequest.getMessageRegex());
		}
		if(filterRequest.getServerRegex() != null && filterRequest.getServerRegex().length() > 0)
		{
			filter.setServerRegex(filterRequest.getServerRegex());
		}
		if(filterRequest.getBeforeDate() != null && filterRequest.getBeforeDate().length() > 0)
		{
			try {
				filter.setBeforeDate(DATE_PARSER.parse(filterRequest.getBeforeDate()));
			} catch (ParseException e) {
				logger.warn("Could not parse date", e);
			}
		}
		if(filterRequest.getLevels() != null && filterRequest.getLevels().length() > 0)
		{
			String[] parts = filterRequest.getLevels().split(",");
			if(parts.length < 4)
			{
				for(String part : parts)
				{
					Level level = Enum.valueOf(Level.class, part);
					filter.getLevels().add(level);
				}
			}
		}
		if(filterRequest.getMessageParts() != null && filterRequest.getMessageParts().length() > 0)
		{
			String[] parts = filterRequest.getMessageParts().split(",");
			if(parts.length < 2)
			{
				for(String item : parts)
				{
					MessagePart part = Enum.valueOf(MessagePart.class, item);
					filter.getMessageParts().add(part);
				}
			}
		}
		
		if(filter.getMessageRegex() != null || 
				filter.getBeforeDate() != null ||
				filter.getLevels().size() > 0 || 
				filter.getMessageParts().size() > 0 ||
				filter.getServerRegex() != null)
		{
			request.getSession().setAttribute(SessionItems.FILTER, filter);
		}
		else
		{
			request.getSession().setAttribute(SessionItems.FILTER, null);
		}
	}
	
	@LoggedIn
	@RequestMapping(value="/logs", method= RequestMethod.GET)
	public String getLogs(HttpServletRequest request, @RequestParam(required = false) Integer onPage, Model model) throws IOException, NoSuchAlgorithmException
	{
		HttpSession session = request.getSession();
		Group group = (Group)session.getAttribute(SessionItems.GROUP);
		Date requestDate = (Date)session.getAttribute(SessionItems.LOG_REQUEST_DATE);
		onPage = (onPage == null)? 0 : onPage;
		User user = (User)session.getAttribute(SessionItems.USER);
		LogFilter filter = (LogFilter)session.getAttribute(SessionItems.FILTER);
		
		UserLogEntry[] items;
		try {
			items = this.clientFactory.getLogClient().getLogsForGroup(group.getAccessKey(), requestDate, onPage, user, filter);
		} catch (Exception e) {
			model.addAttribute("SystemError", e.getMessage());
			return "user-level/log-partial";
		}
		
		model.addAttribute("page", onPage);
		
		if(items.length == 0)
		{
			// Exit early as there are no logs
			model.addAttribute("logs", null);
			request.getSession().setAttribute(SessionItems.LOG_ON_PAGE, onPage + 1);
			return "user-level/log-partial";
		}
		
		if((onPage == null || onPage == 0)
				&& items[0].getLogEntry().getDate().after(requestDate))
		{
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(items[0].getLogEntry().getDate());
			calendar.add(Calendar.SECOND, 1);
			
			session.setAttribute(SessionItems.LOG_REQUEST_DATE, calendar.getTime());
			session.setAttribute(SessionItems.LOG_LATEST_DATE, calendar.getTime());
		}
		
		// Encode content requiring it
		for(UserLogEntry item : items)
		{
			if(item.getLogEntry().getException() != null)
			{
				item.getLogEntry().setException(HtmlUtils.htmlEscape(item.getLogEntry().getException()));
				item.getLogEntry().setException(item.getLogEntry().getException().replace("\n", "<br/>"));
			}
			
			AlertCriteria newAlert = new AlertCriteria();
			newAlert.setMessageRegex(item.getLogEntry().getMessage());
			newAlert.setGroupId(group.getId());
			item.getAlertCriteria().add(newAlert);
		}

		if(items != null && items.length > 0)
		{
			model.addAttribute("logs", items);
			request.getSession().setAttribute(SessionItems.LOG_ON_PAGE, onPage + 1);
		}
		
		return "user-level/log-partial";
	}
	
	@LoggedIn
	@RequestMapping(value="/logs/count", method=RequestMethod.GET)
	public ModelAndView getNewLogCount(HttpServletRequest request, ModelAndView model) throws Exception
	{
		HttpSession session = request.getSession();
		Group group = (Group)session.getAttribute(SessionItems.GROUP);
		Date requestDate = (Date)session.getAttribute(SessionItems.LOG_REQUEST_DATE);
		User user = (User)session.getAttribute(SessionItems.USER);
		
		this.clientFactory.getLogClient().getNewLogCount(group.getAccessKey(), requestDate, user);
		return model;
	}
	
	
	@LoggedIn
	@RequestMapping(value="/notifications", method=RequestMethod.GET)
	public String getNotifications(HttpServletRequest request, Model model)
	{
		User user = (User)request.getSession().getAttribute(SessionItems.USER);
		try {
			Notification[] notifications = this.clientFactory.getNotificationClient().getNotifications(user);
			model.addAttribute("notifications", notifications);
		} catch (Exception e) {
			logger.error("An error occured while retrieving notifications", e);
		}
		return "user-level/notifications";
	}
	
	@LoggedIn
	@RequestMapping(value="/notifications/group/{groupId}/{action}", method=RequestMethod.GET)
	public String acceptGroup(@PathVariable("groupId") String groupId, @PathVariable("action") String action, HttpServletRequest request)
	{
		User user = (User)request.getSession().getAttribute(SessionItems.USER);
		try {
			NotificationResponse response = null;
			for(NotificationResponse item : NotificationResponse.values())
			{
				if(action.equalsIgnoreCase(item.toString()))
				{
					response = item;
					break;
				}
			}
			if(response == null)
			{
				return "redirect:/notifications";
			}
			this.clientFactory.getNotificationClient().groupJoinResponse(groupId, response, user);
		} catch(Exception e)
		{
			logger.error("An error occured while accepting the group " + groupId);
		}
		return "redirect:/notifications";
	}
	
	@LoggedIn
	@RequestMapping(value="/logout", method=RequestMethod.GET)
	public String logout(HttpServletRequest request)
	{
		request.getSession().invalidate();
		return "redirect:/";
	}
	
	@LoggedIn
	@RequestMapping(value="/walkthrough", method=RequestMethod.GET)
	public String getWalkThrough()
	{
		return "user-level/walkthrough";
	}
}
