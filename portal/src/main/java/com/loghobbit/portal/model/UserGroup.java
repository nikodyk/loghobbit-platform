package com.loghobbit.portal.model;

import com.loghobbit.entities.v1.Group;

public class UserGroup {
	private Group group;
	public Group getGroup(){ return this.group; }
	public UserGroup setGroup(Group value)
	{
		this.group = value;
		return this;
	}
	
	private String userMemberType;
	public String getUserMemberType(){ return this.userMemberType; }
	public UserGroup setUserMemberType(String value)
	{
		this.userMemberType = value;
		return this;
	}
	
	private String owner;
	public String getOwner(){ return this.owner; }
	public UserGroup setOwner(String value)
	{ 
		this.owner = value; 
		return this;
	}
}
