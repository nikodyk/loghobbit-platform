package com.loghobbit.portal.model;

public class Credentials {
	private String email;
	public String getEmail(){ return this.email; }
	public Credentials setEmail(String value)
	{
		this.email = value;
		return this;
	}
	
	private String password;
	public String getPassword(){ return this.password; }
	public Credentials setPassword(String value)
	{
		this.password = value;
		return this;
	}
}
