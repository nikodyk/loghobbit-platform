package com.loghobbit.service.dal.entities;

import com.loghobbit.entities.v1.Notification;

public class NotificationStorage {
	public static final String USER_ID = "userId";
	public static final String GROUP_ID = "notification.group._id";
	public static final String DATE = "dateMS";
	public static final String VIEWED = "viewed";
	public static final String NOTIFICATION = "notification";
	
	private String userId;
	public String getUserId(){ return this.userId; }
	public void setUserId(String value){ this.userId = value; }
	
	private long dateMS;
	public long getDateMS(){ return this.dateMS; }
	public void setDateMS(long value){ this.dateMS = value; }
	
	private boolean viewed;
	public boolean getViewed(){ return this.viewed; }
	public void setViewed(boolean value){ this.viewed = value; }
	
	private Notification notification;
	public Notification getNotification(){ return this.notification; }
	public void setNotification(Notification value){ this.notification = value; }
}
