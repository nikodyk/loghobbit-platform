package com.loghobbit.service.v1;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.http.entity.StringEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.loghobbit.entities.v1.Group;
import com.loghobbit.entities.v1.GroupStatistics;
import com.loghobbit.entities.v1.TeamMember;
import com.loghobbit.entities.v1.TeamMemberType;
import com.loghobbit.entities.v1.User;
import com.loghobbit.service.AuthenticationFilter;
import com.loghobbit.service.dal.IGroupAccessLayer;
import com.loghobbit.service.dal.ILogAccessLayer;
import com.loghobbit.service.dal.IUserAccessLayer;
import com.loghobbit.service.utils.DateUtils;

@Path("/v1/groups")
public class GroupSvc {
	@Autowired
	@Qualifier("GroupAccessLayer")
	private IGroupAccessLayer groupAccessLayer;
	public GroupSvc setGroupAccessLayer(IGroupAccessLayer value)
	{
		this.groupAccessLayer = value;
		return this;
	}
	
	@Autowired
	@Qualifier("LogAccessLayer")
	private ILogAccessLayer logAccessLayer;
	public GroupSvc setLogAccessLayer(ILogAccessLayer value)
	{
		this.logAccessLayer = value;
		return this;
	}
	
	@Autowired
	@Qualifier("NotificationService")
	private NotificationSvc notificationService;
	public GroupSvc setNotificationService(NotificationSvc value)
	{
		this.notificationService = value;
		return this;
	}
	
	@Autowired
	private IUserAccessLayer userAccesslayer;
	public GroupSvc setUserAccessLayer(IUserAccessLayer value)
	{
		this.userAccesslayer = value;
		return this;
	}
	
	@GET
	@Path("/")
	public List<Group> getGroupsForUser(@HeaderParam(AuthenticationFilter.USER_DATA) String userId)
	{
		if(userId == null)
		{
			throw new IllegalStateException("The current user's id was not provided");
		}
		
		List<Group> groups = this.groupAccessLayer.getGroupsForMember(userId);
		
		return groups;
	}
	
	@PUT
	@Path("/")
	public Response putGroup(Group group, @HeaderParam(AuthenticationFilter.USER_DATA) String userId) throws UnsupportedEncodingException
	{
		User user = this.userAccesslayer.getUserFromId(userId);
		
		if(group == null)
		{
			return this.getErrorResponse(Status.BAD_REQUEST, "The group data supplied was invalid");
		}
		
		
		if(group.getId() != null && group.getId().length() > 0)
		{
			return this.updateGroup(group, user);
		}
		
		boolean userFound = false;
		for(TeamMember member : group.getTeamMembers())
		{
			member.setMemberId(member.getMemberId().trim());
			if(member.getMemberId().equals(userId))
			{
				if(member.getType() != TeamMemberType.owner)
				{
					return this.getErrorResponse(Status.BAD_REQUEST, "The current user must be the owner when first creating the group");
				}
				userFound = true;
			}
			else
			{
				if(member.getType() == TeamMemberType.admin && !user.getSystemOptions().getCanHaveAdmins())
				{
					return this.getErrorResponse(Status.BAD_REQUEST, "The current user doesn't have the ability to use admins");
				}
				if(member.getType() == TeamMemberType.owner)
				{
					return this.getErrorResponse(Status.BAD_REQUEST, "The current user must be the owner when first creating the group");
				}
			}
		}
		
		if(!userFound)
		{
			group.getTeamMembers().add(TeamMember.createTeamMember(userId, TeamMemberType.owner, true));
		}
		String id = this.groupAccessLayer.addGroup(group);
		
		for(TeamMember member : group.getTeamMembers())
		{
			if(member.getType() != TeamMemberType.owner)
			{
				this.notificationService.sendJoinGroupNotification(user, member.getMemberId(), group);
			}
		}
		
		return Response.ok(id).build();
	}
	
	private Response updateGroup(Group group, User user) throws UnsupportedEncodingException
	{
		if(group.getTeamMembers() == null || group.getTeamMembers().size() == 0)
		{
			return this.getErrorResponse(Status.BAD_REQUEST, "The group must have at least one user");
		}
		
		Group existing = this.groupAccessLayer.getGroupById(group.getId());
		TeamMemberType accessType = this.getAccessForUser(existing, user.getEmail());
		if(accessType == TeamMemberType.none)
		{
			return this.getErrorResponse(Status.BAD_REQUEST, "Could not find the specified group id");
		}
		if(accessType == TeamMemberType.member)
		{
			return this.getErrorResponse(Status.FORBIDDEN, "The current user does not have permissions to perform this operation");
		}
		if(group.getAccessKey() == null || group.getAccessKey().trim().length() < 10)
		{
			return this.getErrorResponse(Status.BAD_REQUEST, "A valid access key must be supplied.  An access key must have at least 10 characters and must be unique");
		}
		
		List<String> membersToNotify = new ArrayList<String>();
		for(TeamMember member : group.getTeamMembers())
		{
			if(!user.getSystemOptions().getCanHaveAdmins() && member.getType() == TeamMemberType.admin)
			{
				return this.getErrorResponse(Status.BAD_REQUEST, "The user's subscription does not support admins");
			}
			
			member.setMemberId(member.getMemberId().trim());
			boolean memberFound = false;
			for(TeamMember current : existing.getTeamMembers())
			{
				if(member.getMemberId().equals(current.getMemberId()))
				{
					memberFound = true;
					member.setAccepted(current.getAccepted());
					break;
				}
			}
			
			if(!memberFound)
			{
				member.setAccepted(false);
				membersToNotify.add(member.getMemberId());
			}
		}
		
		this.groupAccessLayer.updateGroup(group);
		
		for(String memberId : membersToNotify)
		{
			this.notificationService.sendJoinGroupNotification(user, memberId, group);
		}
		
		return Response.ok(group.getId()).build();
	}
	
	@DELETE
	@Path("/{groupId}")
	public Response deleteGroup(@PathParam("groupId") String groupId, @HeaderParam(AuthenticationFilter.USER_DATA) String userId) throws UnsupportedEncodingException
	{
		if(groupId == null)
		{
			return this.getErrorResponse(Status.BAD_REQUEST, "The group id specified was invalid");
		}
		
		Group group = this.groupAccessLayer.getGroupById(groupId);
		if(group == null)
		{
			return this.getErrorResponse(Status.BAD_REQUEST, "The group id specified could not be found");
		}
		
		TeamMember userItem = null;
		for(TeamMember member : group.getTeamMembers())
		{
			if(member.getMemberId().equals(userId))
			{
				userItem = member;
				break;
			}
		}
		if(userItem == null)
		{
			return this.getErrorResponse(Status.BAD_REQUEST, "The current user cannot be found in the team member list for this group");
		}
		
		if(userItem.getType() != TeamMemberType.owner)
		{
			return this.getErrorResponse(Status.FORBIDDEN, "The current team member does not have permission to perform this action.");
		}
		
		this.groupAccessLayer.deleteGroup(groupId);
		return Response.ok().build();
	}
	
	@GET
	@Path("/{groupId}/leave")
	public Response leaveGroup(@PathParam("groupId") String groupId, @HeaderParam(AuthenticationFilter.USER_DATA) String userId) throws UnsupportedEncodingException
	{
		if(groupId == null)
		{
			return this.getErrorResponse(Status.BAD_REQUEST, "The group id specified was invalid");
		}
		
		Group group = this.groupAccessLayer.getGroupById(groupId);
		if(group == null)
		{
			return this.getErrorResponse(Status.BAD_REQUEST, "The group id specified could not be found");
		}
		
		Iterator<TeamMember> members = group.getTeamMembers().iterator();
		boolean found = false;
		while(members.hasNext())
		{
			TeamMember member = members.next();
			if(member == null)
			{
				continue;
			}
			
			
			if(member.getMemberId().equals(userId))
			{
				if(member.getType() == TeamMemberType.owner)
				{
					return this.getErrorResponse(Status.BAD_REQUEST, "The group owner cannot leave the group. Change ownership of the group and try again.");
				}
				
				members.remove();
				found = true;
				break;
			}
		}
		
		if(!found)
		{
			return this.getErrorResponse(Status.BAD_REQUEST, "The user specified does not exist in the group");
		}
		
		this.groupAccessLayer.updateGroupMembers(group);
		
		return Response.ok().build();
	}
	
	@GET
	@Path("/stats")
	public Response getGroupStats(@HeaderParam("startDate") Date start, @HeaderParam("endDate") Date end, @HeaderParam(AuthenticationFilter.USER_DATA) String userId)
	{
		if(start == null)
		{
			start = new Date(0);
		}
		if(end == null)
		{
			end = new Date();
		}
		
		List<GroupStatistics> groupStats = null;
		
		List<Group> groups = this.groupAccessLayer.getGroupsOwnedByUser(userId);
		if(groups == null || groups.size() == 0)
		{
			return Response.ok(new ArrayList<GroupStatistics>()).build();
		}
		
		List<String> groupIds = new ArrayList<String>(groups.size());
		for(Group group : groups)
		{
			groupIds.add(group.getId());
		}
		
		groupStats = this.groupAccessLayer.getGroupsStats(groupIds, start, end);
		
		//
		// Get todays information if applicable
		//
		Date today = DateUtils.getTodayBOD();
		if(today.after(end))
		{
			return Response.ok(groupStats).build();
		}
		
		Date tomorrow = DateUtils.getTomorrowBOD();
		
		List<GroupStatistics> todayStats = this.logAccessLayer.getSizeOfLogsInGroup(groupIds, today, tomorrow);
		
		if(todayStats == null)
		{
			return Response.ok(groupStats).build();
		}
		
		groupStats = new ArrayList<GroupStatistics>(groupStats);
		for(GroupStatistics stat : todayStats)
		{
			groupStats.add(stat);
		}
		
		return Response.ok(groupStats).build();
	}
	
	private Response getErrorResponse(Status status, String message) throws UnsupportedEncodingException {
		return Response.status(status)
						.type(MediaType.TEXT_PLAIN_TYPE)
						.entity(new StringEntity(message)).build();
	}

	private TeamMemberType getAccessForUser(Group group, String userId)
	{
		if(group == null || userId == null)
		{
			return TeamMemberType.none;
		}
		for(TeamMember member : group.getTeamMembers())
		{
			if(member.getMemberId().equals(userId))
			{
				return member.getType();
			}
		}
		
		return TeamMemberType.none;
	}
}
