<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script type="text/javascript" src="/resources/js/group-edit-form.js"></script>
<div id="group-form-backdrop" class="fade-screen" style="display:none;">
</div>

<div id="group-form-dialog" class="dialog" style="display:none;">
	<div class="dialog-content">
		<div class="dialog-form">
			<h1 id="group-form-dialog-title">Create Group</h1>
			<input id="group-form-id" type="hidden">
			<div class="row form-group">
				<div class="col-xs-12">
					<label>Name</label> 
					<input id="group-form-name" type="text" class="form-control" placeholder="Group Name" />
				</div>
			</div>
			<div class="row form-group">
				<div class="col-xs-12">
					<label>Description</label> 
					<textarea id="group-form-description" class="form-control" placeholder="Description"></textarea>
				</div>
			</div>
			<div class="row form-group">
				<div class="col-xs-12">
					<div>
						<label>Members</label>
					</div>
					<div id="group-dialog-invite" class="row">
						<div class="col-xs-10"><input id="group-form-invite-text" type="text" class="form-control" placeholder="Email address" /></div> 
						<div class="col-xs-2"><a href="#" class="btn btn-primary" onClick="inviteToGroup()">Invite</a></div>
					</div>
					<div class="row">
						<div id="group-dialog-members" class="col-xs-offset-1 col-xs-10 memberList">
						</div>
						<div id="group-dialog-member-template" style="display:none;">
							<div class="row">
								<div id="memberId" class="col-xs-8">{memberName}<i class="fa {status}"></i>
								</div>
								<div id="memberType" class="col-xs-3 ">
									<select id="member-role" class="member-role">
										<option class="member-role-owner" value="owner">Owner</option>
										<c:if test="${canHaveAdmins }">
											<option class="member-role-admin" value="admin">Admin</option>
										</c:if>
										<option class="member-role-member" value="member">Member</option>
									</select>
								</div>
								<div class="col-xs-1 remove-x">
									<a href="#" style="color:red;" onclick="removeFromInvite(this)">
										<i class="fa fa-times"></i>
									</a>
								</div>
							</div>
						</div>
						<div id="group-dialog-member-template" style="display:none;">
							<div class="row">
								<div id="memberId" class="col-xs-8">{memberName}<i class="fa {status}"></i>
								</div>
								<div id="memberType" class="col-xs-3 ">
									<select id="member-role" class="member-role">
										<option class="member-role-owner" value="owner">Owner</option>
										<c:if test="${canHaveAdmins }">
											<option class="member-role-admin" value="admin">Admin</option>
										</c:if>
										<option class="member-role-member" value="member">Member</option>
									</select>
								</div>
								<div class="col-xs-1 remove-x">
									<a href="#" style="color:red;" onclick="removeFromInvite(this)">
										<i class="fa fa-times"></i>
									</a>
								</div>
							</div>
						</div>
						<div id="group-dialog-member-readonly-template" style="display:none;">
							<div class="row">
								<div id="memberId" class="col-xs-8">{memberName}<i class="fa {status}"></i>
								</div>
								<div id="memberType" class="col-xs-3 ">
									{memberRole}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div id="group-form-actions-admin" class="place-at-bottom dialog-actions">
		<a id="group-form-save" href="#" onclick="createGroup()" class="btn btn-primary">Save</a><a id="group-form-cancel" href="#" onclick="closeGroupDialog()" class="btn btn-primary">Cancel</a>
	</div>
	<div id="group-form-actions-readonly" class="place-at-bottom dialog-actions" style="display:none">
		<a id="group-form-cancel" href="#" onclick="closeGroupDialog()" class="btn btn-primary">Close</a>
	</div>
</div>