package com.loghobbit.portal.model;

public enum LogViewAction {
	addAlert,
	removeAlert,
	filter
}
