package com.loghobbit.service.clients;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

public class ServiceClientFactory {
	private String endpoint;
	public String getEndpoint(){ return this.endpoint; }
	public ServiceClientFactory setEndpoint(String value)
	{
		this.endpoint = value;
		return this;
	}
	
	private GroupClient groupClient;
	public GroupClient getGroupClient(){ return this.groupClient; }
	
	private LogClient logClient;
	public LogClient getLogClient(){ return this.logClient; }
	
	private UserClient userClient;
	public UserClient getUserClient(){ return this.userClient; }
	
	private SubscriptionClient subscriptionClient;
	public SubscriptionClient getSubscriptionClient(){ return this.subscriptionClient; }
	
	private NotificationClient notificationClient;
	public NotificationClient getNotificationClient(){ return this.notificationClient; }
	
	public void initialize()
	{
		HttpClient client = HttpClientBuilder.create().build();
		this.groupClient = new GroupClient(this.endpoint, client);
		this.logClient = new LogClient(this.endpoint, client);
		this.userClient = new UserClient(this.endpoint, client);
		this.subscriptionClient = new SubscriptionClient(this.endpoint, client);
		this.notificationClient = new NotificationClient(this.endpoint, client);
	}
}
