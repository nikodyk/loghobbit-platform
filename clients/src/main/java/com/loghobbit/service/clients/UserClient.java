package com.loghobbit.service.clients;

import org.apache.cxf.helpers.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;

import com.loghobbit.entities.v1.Credentials;
import com.loghobbit.entities.v1.User;
import com.loghobbit.entities.v1.requests.UpdateUserRequest;

public class UserClient extends ServiceClientBase {
	private final String userLoginEndpoint;
	private final String userSignupEndpoint;
	private final String userUpdateEndpoint;
	private final String userEmailValidation;
	private final String userEmailValidationResend;
	
	public UserClient(String endpoint, HttpClient client)
	{
		super(endpoint, client);
		this.userLoginEndpoint = String.format("%s/v1/user/login", this.endpoint);
		this.userSignupEndpoint = String.format("%s/v1/user/signup", this.endpoint);
		this.userUpdateEndpoint = String.format("%s/v1/user", this.endpoint);
		this.userEmailValidation = String.format("%s/v1/user/verify/email", this.endpoint);
		this.userEmailValidationResend = String.format("%s/v1/user/verify/email/resend", this.endpoint);
	}
	
	public User login(Credentials credentials) throws Exception
	{
		HttpPost post = new HttpPost(this.userLoginEndpoint);
		post.setEntity(new StringEntity(mapper.writeValueAsString(credentials)));
		post.setHeader("Content-Type", "application/json");
		post.setHeader("Accept", "application/json");
		HttpResponse response = this.client.execute(post);
		
		this.handleErrorResponse(response);
		
		if(response.getStatusLine().getStatusCode() == 204)
		{
			return null;
		}
		
		User retval = mapper.readValue(response.getEntity().getContent(), User.class);
		response.getEntity().getContent().close();
		return retval;
	}

	public String signup(User user) throws Exception {
		HttpPut put = new HttpPut(this.userSignupEndpoint);
		put.setEntity(new StringEntity(mapper.writeValueAsString(user)));
		put.setHeader("Content-Type", "application/json");
		put.setHeader("Accept", "application/json");
		
		HttpResponse response = this.client.execute(put);
		
		this.handleErrorResponse(response);
		
		if(response.getStatusLine().getStatusCode() == 204)
		{
			return null;
		}
		
		String retval = IOUtils.toString(response.getEntity().getContent());
		response.getEntity().getContent().close();
		
		return retval;
	}

	public User updateUser(UpdateUserRequest updates, User user) throws Exception {
		HttpPut put = new HttpPut(this.userUpdateEndpoint);
		this.addCommonHeaders(put, user);
		put.setEntity(new StringEntity(mapper.writeValueAsString(updates)));
		
		HttpResponse response = this.client.execute(put);
		
		this.handleErrorResponse(response);
		
		if(response.getStatusLine().getStatusCode() == 204)
		{
			return null;
		}
		
		User retval = mapper.readValue(response.getEntity().getContent(), User.class);
		response.getEntity().getContent().close();
		
		return retval;
	}

	public boolean validateEmail(String emailToken, User user) throws Exception {
		HttpPost post = new HttpPost(this.userEmailValidation);
		this.addCommonHeaders(post, user);
		post.setEntity(new StringEntity(emailToken));
		
		HttpResponse response = this.client.execute(post);
		
		this.handleErrorResponse(response);
		
		return response.getStatusLine().getStatusCode() != 200;
	}

	public boolean resendValidateEmail(User user) throws Exception {
		HttpPost post = new HttpPost(this.userEmailValidationResend);
		this.addCommonHeaders(post, user);
		
		HttpResponse response = this.client.execute(post);
		
		this.handleErrorResponse(response);
		
		return response.getStatusLine().getStatusCode() != 200;
	}
}
