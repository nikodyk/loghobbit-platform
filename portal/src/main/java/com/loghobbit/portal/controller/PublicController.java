package com.loghobbit.portal.controller;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.loghobbit.entities.v1.User;
import com.loghobbit.portal.model.Credentials;
import com.loghobbit.portal.model.RegisterUserRequest;
import com.loghobbit.portal.services.EmailService;
import com.loghobbit.portal.services.LogHobbitSystem;
import com.loghobbit.service.clients.ServiceClientFactory;

@Controller
public class PublicController {
	public static final String PAGE = "page";
	private static final Logger logger = Logger.getLogger(PublicController.class);
	
	@Autowired
	@Qualifier("ServiceClientFactory")
	public ServiceClientFactory clientFactory;
	public PublicController setClientFactory(ServiceClientFactory value)
	{
		this.clientFactory = value;
		return this;
	}
	
	@Autowired
	@Qualifier("EmailService")
	private EmailService emailService;
	public PublicController setEmailService(EmailService value)
	{
		this.emailService = value;
		return this;
	}

	@RequestMapping(value="/")
	public ModelAndView index(HttpServletResponse response) throws IOException {
		ModelAndView retval = new ModelAndView("public/index");
		retval.addObject(PAGE, "home");
		retval.addObject("failed", false);
		return retval;
	}
	
	@RequestMapping(value="/", method= RequestMethod.POST)
	public ModelAndView postLogin(Credentials credentials, HttpServletRequest request, HttpServletResponse response) throws IOException, NoSuchAlgorithmException
	{
		if(credentials == null || credentials.getEmail() == null || credentials.getPassword() == null)
		{
			ModelAndView retval = this.index(response);
			retval.addObject("failed", true);
			return retval;
		}
		
		User user = null;
		try
		{
			user = this.clientFactory.getUserClient().login(new com.loghobbit.entities.v1.Credentials(credentials.getEmail(), credentials.getPassword()));
		}
		catch(Exception e)
		{
		}
		
		if(user == null)
		{
			ModelAndView retval = this.index(response);
			retval.addObject("failed", true);
			logger.warn("Login failed for " + credentials.getEmail());
			return retval;
		}
		
		return this.initializeUser(request, user);
	}
	
	@RequestMapping(value="/signup", method= RequestMethod.GET)
	public ModelAndView getSignup() throws IOException
	{
		ModelAndView retval = new ModelAndView("public/sign-up");
		retval.addObject(PublicController.PAGE, "login");
		return retval;
	}
	
	@RequestMapping(value="/signup", method= RequestMethod.POST)
	public ModelAndView postSignUp(RegisterUserRequest registerRequest, HttpServletRequest request) throws IOException, NoSuchAlgorithmException
	{
		boolean error = false;
		ModelAndView errorModel = this.getSignup();
		if(this.emailService.validate(registerRequest.getEmail()))
		{
			errorModel.addObject("emailFlag", "has-error");
			error = true;
		}
		if(registerRequest.getPassword() == null || registerRequest.getPassword().length() < 6)
		{
			errorModel.addObject("passwordFlag", "has-error");
			errorModel.addObject("passwordError", "password has to be at least 6 characters");
			error = true;
		}
		if(registerRequest.getFullName() == null || registerRequest.getFullName().length() == 0)
		{
			errorModel.addObject("fullNameFlag", "has-error");
			error = true;
		}
		
		if(error)
		{
			errorModel.addObject("details", registerRequest);
			return errorModel;
		}
		
		User user = new User();
		user.setEmail(registerRequest.getEmail());
		user.setFullName(registerRequest.getFullName());
		user.setPassword(registerRequest.getPassword().getBytes());
		
		try
		{
			String securityToken = this.clientFactory.getUserClient().signup(user);
			user.setSecurityToken(securityToken);
		}
		catch(Exception e)
		{
			errorModel.addObject("emailFlag", "has-error");
			errorModel.addObject("emailError", e.getMessage());
			return errorModel;
		}
		
		request.getSession().setAttribute(SessionItems.USER, user);
		return this.initializeUser(request, user);
	}
	
	private ModelAndView initializeUser(HttpServletRequest request, User user)
	{
		HttpSession session = request.getSession();
		session.setAttribute(SessionItems.USER, user);
		session.setAttribute(SessionItems.WEBSITE_URL, LogHobbitSystem.instance.getWebsiteUrl());
		session.setAttribute(SessionItems.ACCOUNT_URL, LogHobbitSystem.instance.getAccountUrl());
		session.setAttribute(SessionItems.GOOGLE_ANALYTICS, LogHobbitSystem.instance.getGoogleAnalyticsId());
		
		logger.info("Login succeeded");
		
		String initialUrl = (String)request.getSession().getAttribute(SessionItems.INITIAL_URL);
		if(initialUrl != null && !initialUrl.equals(request.getRequestURL().toString()))
		{
			return new ModelAndView("redirect:" + initialUrl);
		}
		
		return new ModelAndView("redirect:home");
	}
}
