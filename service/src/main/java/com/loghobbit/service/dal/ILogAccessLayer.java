package com.loghobbit.service.dal;

import java.util.Date;
import java.util.List;

import com.loghobbit.entities.LogEntry;
import com.loghobbit.entities.v1.AlertCriteria;
import com.loghobbit.entities.v1.GroupStatistics;
import com.loghobbit.entities.v1.LogFilter;
import com.loghobbit.service.dal.entities.LogEntryStorage;
import com.loghobbit.service.dal.entities.UniqueLogEntry;
import com.loghobbit.service.exceptions.AlertExistsException;

public interface ILogAccessLayer {
	/**
	 * Gets logs for the specified key
	 * @param accessKey the access key for the group
	 * @param requestDate the requested date
	 * @param page the page to retrieve
	 * @return a list of log entries
	 */
	List<LogEntryStorage> getLogs(String accessKey, Date requestDate, int page, LogFilter filter);

	/**
	 * Adds a log entry
	 * @param logEntry the log entry to add
	 */
	void addLogEntry(LogEntry logEntry, String groupId);

	/**
	 * Cleans up old logs
	 * @param logId the id of the log
	 * @param time the date in milliseconds to remove anything before
	 */
	void cleanUpOldLogs(String groupId, long time);

	/**
	 * Gets the count of new log entries since the date
	 * @param id the id of the group
	 * @param requestDate the date logs where last requested
	 * @return the count of new log entries
	 */
	int getNewLogCount(String id, Date requestDate);

	/**
	 * Finds or adds the unique log entry, returning the existing or newly created log entry
	 * @param entry the entry to be searched/added
	 * @return the existing or new log entry with id
	 */
	UniqueLogEntry insertUniqueMessage(UniqueLogEntry entry);
	
	/**
	 * Gets the statistics for the specified date range
	 * @param groupId the id of the group
	 * @param startDate the start date
	 * @param endDate the end date
	 * @return the stats for the group
	 */
	List<GroupStatistics> getSizeOfLogsInGroup(List<String> groupId, Date startDate, Date endDate);

	/**
	 * Gets the unique log entries for the group
	 * @param groupId the id of the group
	 * @param entry the log entry to retrieve the unique entry for
	 * @return a unique log entry
	 */
	UniqueLogEntry getUniqueLogEntriesForLogEntry(String groupId, LogEntry entry);

	/**
	 * Gets alert criteria for the alert ids specified
	 * @param alertIds the ids of the alerts
	 * @return a list of alert criteria
	 */
	List<AlertCriteria> getAlertsById(List<String> alertIds);

	/**
	 * Adds alert criteria
	 * @param alertCriteria the criteria to add
	 */
	void addAlert(AlertCriteria alertCriteria) throws AlertExistsException;

	/**
	 * Gets an alert by its id
	 * @param id the id of the alert
	 * @return the alert criteria
	 */
	AlertCriteria getAlertById(String id);

	/**
	 * Updates alert criteria
	 * @param alertCriteria the criteria to update
	 */
	void updateAlert(AlertCriteria alertCriteria);

	/**
	 * Gets a unique log entry which doesn't have the alert criteria, but matches
	 * @param alertCriteria the alert criteria to check
	 * @return a unique log entry
	 */
	UniqueLogEntry getMatchingUniqueWOAlert(AlertCriteria alertCriteria);

	/**
	 * updates the unique log entry with the new information provided
	 * @param entry the content to update
	 */
	void updateUniqueLogEntry(UniqueLogEntry entry);

}
