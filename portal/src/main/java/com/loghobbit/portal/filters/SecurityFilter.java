package com.loghobbit.portal.filters;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.loghobbit.portal.controller.SessionItems;

public class SecurityFilter implements HandlerInterceptor  {
	
	@Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		if(handler != null && HandlerMethod.class.isInstance(handler))
		{
			HandlerMethod method = (HandlerMethod)handler;
			LoggedIn annotation = method.getMethodAnnotation(LoggedIn.class);
			if(annotation != null)
			{
				HttpSession session = request.getSession(false);
				if(session != null && session.getAttribute(SessionItems.USER) != null)
				{
					session.setAttribute("loggedIn", true);
					return true;
				}
				
				request.getSession().setAttribute(SessionItems.INITIAL_URL, request.getHeader("referer"));
				response.sendRedirect("/");
				return false;
			}
		}
		
        return true;
    }

	@Override
	public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3) throws Exception {
		// TODO Auto-generated method stub
	}

	@Override
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3) throws Exception {
		// TODO Auto-generated method stub
		
	}
}
