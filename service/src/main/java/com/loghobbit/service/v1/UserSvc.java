package com.loghobbit.service.v1;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.loghobbit.entities.v1.Credentials;
import com.loghobbit.entities.v1.Group;
import com.loghobbit.entities.v1.TeamMember;
import com.loghobbit.entities.v1.TeamMemberType;
import com.loghobbit.entities.v1.User;
import com.loghobbit.entities.v1.UserSystemOptions;
import com.loghobbit.entities.v1.requests.UpdateUserRequest;
import com.loghobbit.service.AuthenticationFilter;
import com.loghobbit.service.dal.IGroupAccessLayer;
import com.loghobbit.service.dal.IUserAccessLayer;

@Path("/v1/user")
public class UserSvc {
	@Autowired
	private IUserAccessLayer userAccesslayer;
	public UserSvc setUserAccessLayer(IUserAccessLayer value)
	{
		this.userAccesslayer = value;
		return this;
	}
	
	@Autowired
	private IGroupAccessLayer groupAccessLayer;
	public UserSvc setGroupAccessLayer(IGroupAccessLayer value)
	{
		this.groupAccessLayer = value;
		return this;
	}

	@Value("${subscription.default.daysRetained}")
	private int defaultDaysLogsRetained;
	public UserSvc setDefaultDaysLogsRetained(int value)
	{
		this.defaultDaysLogsRetained = value;
		return this;
	}
	
	@Value("${subscription.default.maxMbPerDay}")
	private int defaultMaxMbPerDay;
	public UserSvc setDefaultMaxMbPerDay(int value)
	{
		this.defaultMaxMbPerDay = value;
		return this;
	}
	
	@Value("${subscription.default.maxMbPerMonth}")
	private int defaultMaxMbPerMonth;
	public UserSvc setDefaultMaxMbPerMonth(int value)
	{
		this.defaultMaxMbPerMonth = value;
		return this;
	}
	
	@Autowired
	private NotificationSvc notificationSvc;
	public UserSvc setNotificationSvc(NotificationSvc value)
	{
		this.notificationSvc = value;
		return this;
	}
	
	@PUT
	@Path("/signup")
	public Response signup(User user) throws NoSuchAlgorithmException, UnsupportedEncodingException
	{
		if(this.userAccesslayer.doesEmailExist(user.getEmail()))
		{
			return Response.status(501).entity("the email address supplied already exists").build();
		}
		
		UserSystemOptions systemOptions = new UserSystemOptions();
		systemOptions.setDaysBeforeLogCleanup(this.defaultDaysLogsRetained);
		systemOptions.setMaxMbPerDay(this.defaultMaxMbPerDay);
		systemOptions.setMaxMbPerMonth(this.defaultMaxMbPerMonth);
		user.setSystemOptions(systemOptions);
		
		user.setPassword(this.getPasswordHash(new String(user.getPassword())));
		user.setIsValidated(false);
		user.setEmailVerificationToken(new ObjectId().toHexString());
		
		this.userAccesslayer.registerUser(user);
		
		TeamMember member = new TeamMember();
		member.setMemberId(user.getEmail());
		member.setType(TeamMemberType.owner);
		member.setAccepted(true);
		
		Group group = new Group();
		group.setName(user.getFullName());
		group.getTeamMembers().add(member);
		group.setAccessKey(new ObjectId().toHexString());
		
		this.groupAccessLayer.addOrganization(group);
		
		this.resendEmailVerification(user.getEmail());
		
		// Remove the token before sending this to the client so we only send the token
		// to the user's email address
		user.setEmailVerificationToken(null);
		
		return Response.ok(user.getSecurityToken()).build();
	}
	
	@POST
	@Path("/login")
	public User login(Credentials credentials) throws NoSuchAlgorithmException, UnsupportedEncodingException
	{
		if(credentials == null || credentials.getEmail() == null || credentials.getPassword() == null)
		{
			throw new IllegalStateException("The data provided is invalid");
		}
		
		MessageDigest md = MessageDigest.getInstance("MD5");
		byte[] hash = md.digest(credentials.getPassword().getBytes("UTF-8"));
		
		User user = this.userAccesslayer.login(credentials.getEmail(), hash);
		if(user == null)
		{
			throw new SecurityException("The username or password is invalid");
		}
		
		return user;
	}
	
	@POST
	@Path("verify/email/resend")
	public Response resendEmailVerification(@HeaderParam(AuthenticationFilter.USER_DATA) String userId)
	{
		User user = this.userAccesslayer.getUserFromId(userId);
		this.notificationSvc.sendEmailVerifications(user.getEmail(), user.getEmailVerificationToken());
		
		return Response.ok().build();
	}
	
	@POST
	@Path("/verify/email")
	public Response verifyEmail(String verifyToken, @HeaderParam(AuthenticationFilter.USER_DATA) String userId)
	{
		User user = this.userAccesslayer.getUserFromId(userId);
		if(!user.getEmailVerificationToken().equals(verifyToken))
		{
			return Response.status(Status.BAD_REQUEST).build();
		}
		
		this.userAccesslayer.userVerified(userId);
		
		return Response.ok().build();
	}
	
	@PUT
	public Response updateUser(UpdateUserRequest update, @HeaderParam(AuthenticationFilter.USER_DATA) String userId) throws UnsupportedEncodingException, NoSuchAlgorithmException
	{
		if(update == null)
		{
			return Response.status(Status.BAD_REQUEST).type(MediaType.TEXT_PLAIN).entity("Could not identify what is to be updated").build();
		}
		if(!userId.equals(update.getEmail()))
		{
			return Response.status(Status.FORBIDDEN).type(MediaType.TEXT_PLAIN).entity("The update specified was not for the current user").build();
		}
		
		byte[] newPassword = null;
		byte[] oldPassword = null;
		if(update.getPassword() != null && update.getPassword().length() > 0)
		{
			if(update.getPassword().length() < 6)
			{
				return Response.status(Status.BAD_REQUEST).type(MediaType.TEXT_PLAIN).entity("The password is too small.  The password must be at least 6 characters.").build();
			}
			
			newPassword = this.getPasswordHash(update.getPassword());
			oldPassword = this.getPasswordHash(update.getCurrentPassword());
		}
		
		if(!this.userAccesslayer.updateUser(update, oldPassword, newPassword, userId))
		{
			return Response.status(Status.UNAUTHORIZED).type(MediaType.TEXT_PLAIN).entity("Either the account cannot be found or the password supplied is incorrect.").build();
		}
		
		User user = this.userAccesslayer.getUserFromId(userId);
		user.setPassword(null);
		
		return Response.ok(user).build();
	}
	
	@GET
	@Path("/sso/token")
	public String getTemporarySSOToken(@HeaderParam(AuthenticationFilter.USER_DATA) String userId)
	{
		return this.userAccesslayer.getSSOToken(userId);
	}
	
	@GET
	@Path("/sso/validate/${token}")
	public Response validateSSOToken(@PathParam("token") String token)
	{
		User user = this.userAccesslayer.validateToken(token);
		if(user == null)
		{
			return Response.status(Status.FORBIDDEN).build();
		}
		return Response.ok(user).build();
	}
	
	private byte[] getPasswordHash(String password) throws UnsupportedEncodingException, NoSuchAlgorithmException
	{
		MessageDigest md = MessageDigest.getInstance("MD5");
		byte[] hash = md.digest(new String(password).getBytes("UTF-8"));
		return hash;
	}
}
