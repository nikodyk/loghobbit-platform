package com.loghobbit.portal.controller;

import static org.mockito.Mockito.*;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.ui.Model;

import com.loghobbit.entities.Level;
import com.loghobbit.entities.LogEntry;
import com.loghobbit.entities.v1.Group;
import com.loghobbit.entities.v1.LogFilter;
import com.loghobbit.entities.v1.TeamMember;
import com.loghobbit.entities.v1.TeamMemberType;
import com.loghobbit.entities.v1.User;
import com.loghobbit.entities.v1.UserLogEntry;
import com.loghobbit.entities.v1.UserSystemOptions;
import com.loghobbit.portal.model.LogFilterRequest;
import com.loghobbit.portal.model.LogViewAction;
import com.loghobbit.service.clients.GroupClient;
import com.loghobbit.service.clients.LogClient;
import com.loghobbit.service.clients.ServiceClientFactory;

public class HomeControllerTest {
	private static final String GROUP_ID = "123";
	private static final String ACCESS_TOKEN = "456";

	private HomeController controller;
	private ServiceClientFactory clientFactory;
	private LogClient logClient;
	private GroupClient groupClient;
	
	private Date requestedDate = new Date();
	private User user = new User();
	private UserLogEntry[] entries = new UserLogEntry[1];
	private Group group = new Group();
	private TeamMember member = new TeamMember();
	private LogFilterRequest filterRequest = new LogFilterRequest();
	private LogFilter setFilter = null;
	
	private HttpServletRequest request;
	private HttpSession session;
	private Model model;
	
	@Before
	public void setup() throws Exception
	{
		this.user.setEmail("email@address.com");
		this.user.setFullName("Full Name");
		this.user.setSecurityToken(ACCESS_TOKEN);
		this.user.setSystemOptions(new UserSystemOptions());
		this.user.setTemporaryToken(ACCESS_TOKEN);
		
		this.member.setAccepted(true);
		this.member.setMemberId(this.user.getEmail());
		this.member.setType(TeamMemberType.owner);
		
		this.group.setAccessKey(GROUP_ID);
		this.group.setName("Group Name");
		this.group.setId(GROUP_ID);
		this.group.setTeamMembers(Arrays.asList(this.member));
		this.user.setSecurityToken(ACCESS_TOKEN);
		
		LogEntry logEntry = new LogEntry();
		logEntry.setAccessKey(GROUP_ID);
		logEntry.setDate(new Date());
		logEntry.setLevel(Level.error);
		logEntry.setLogName("Log Name");
		logEntry.setMessage("This is a message");
		logEntry.setServer("server");
		logEntry.setSimpleDate("1234");
		
		UserLogEntry userLogEntry = new UserLogEntry()
										.setLogEntry(logEntry);
		
		this.entries[0] = userLogEntry;
		this.logClient = mock(LogClient.class);
		when(this.logClient.getLogsForGroup(eq(GROUP_ID), eq(requestedDate), anyInt(), eq(this.user), any(LogFilter.class))).thenReturn(this.entries);
		
		this.groupClient = mock(GroupClient.class);
		when(this.groupClient.getUsersGroups(ACCESS_TOKEN)).thenReturn(new Group[]{this.group});
		
		this.clientFactory = mock(ServiceClientFactory.class);
		when(this.clientFactory.getLogClient()).thenReturn(this.logClient);
		when(this.clientFactory.getGroupClient()).thenReturn(this.groupClient);
		
		this.controller = new HomeController()
								.setClientFactory(this.clientFactory);
		
		this.model = mock(Model.class);
		
		this.session = mock(HttpSession.class);
		when(session.getAttribute(SessionItems.LOG_REQUEST_DATE)).thenReturn(this.requestedDate);
		when(session.getAttribute(SessionItems.USER)).thenReturn(this.user);
		when(session.getAttribute(SessionItems.GROUP)).thenReturn(this.group);
		
		this.request = mock(HttpServletRequest.class);
		when(this.request.getSession()).thenReturn(this.session);
	}
	
	@Test
	public void getHome() throws NoSuchAlgorithmException, IOException
	{
		String result = this.controller.getHome(this.request, GROUP_ID, 0, this.model);
		Assert.assertEquals("View Name", "user-level/home", result);
		verify(this.model, times(0)).addAttribute("GetGroupsError", true);
		verify(this.model, times(0)).addAttribute("SystemError", true);
		
		verify(this.model).addAttribute("logs", this.entries);
		verify(this.model).addAttribute("showHeader", true);
	}
	
	@Test
	public void postHome() throws NoSuchAlgorithmException, IOException
	{
		String result = this.controller.postHome(filterRequest, null, request, GROUP_ID, 0, LogViewAction.filter, model);
		Assert.assertEquals("View Name", "user-level/home", result);
		
		verify(this.model, times(0)).addAttribute("GetGroupsError", true);
		verify(this.model, times(0)).addAttribute("SystemError", true);
		
		verify(this.session).setAttribute(SessionItems.FILTER, null);
		
		verify(this.model).addAttribute("logs", this.entries);
		verify(this.model).addAttribute("showHeader", true);
	}
	
	@Test
	public void postHome_ValidFilter_MessageRegex() throws NoSuchAlgorithmException, IOException
	{
		filterRequest.setMessageRegex("foo");
		String result = this.controller.postHome(filterRequest, null, request, GROUP_ID, 0, LogViewAction.filter, model);
		Assert.assertEquals("View Name", "user-level/home", result);
		
		verify(this.model, times(0)).addAttribute("GetGroupsError", true);
		verify(this.model, times(0)).addAttribute("SystemError", true);
		
		verify(this.session).setAttribute(eq(SessionItems.FILTER), any(LogFilter.class));
		
		verify(this.model).addAttribute("logs", this.entries);
		verify(this.model).addAttribute("showHeader", true);
	}
	
	@Test
	public void getLogs_Valid() throws NoSuchAlgorithmException, IOException
	{
		String result = this.controller.getLogs(request, 0, model);
		Assert.assertEquals("View Name", "user-level/log-partial", result);
		
		verify(this.model, times(0)).addAttribute("GetGroupsError", true);
		verify(this.model, times(0)).addAttribute("SystemError", true);
		verify(this.model, times(0)).addAttribute("showHeader", true);
		
		verify(this.model).addAttribute("logs", this.entries);
	}
	
	@Test
	public void getLogs_Valid_PageNull() throws NoSuchAlgorithmException, IOException
	{
		String result = this.controller.getLogs(request, null, model);
		Assert.assertEquals("View Name", "user-level/log-partial", result);
		
		verify(this.model, times(0)).addAttribute("GetGroupsError", true);
		verify(this.model, times(0)).addAttribute("SystemError", true);
		verify(this.model, times(0)).addAttribute("showHeader", true);
		
		verify(this.model).addAttribute("logs", this.entries);
	}
}
