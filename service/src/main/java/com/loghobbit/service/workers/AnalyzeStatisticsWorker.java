package com.loghobbit.service.workers;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.loghobbit.entities.v1.Group;
import com.loghobbit.entities.v1.GroupStatistics;
import com.loghobbit.service.dal.IGroupAccessLayer;
import com.loghobbit.service.dal.ILogAccessLayer;

public class AnalyzeStatisticsWorker extends ContinualWorkerBase {
	@Autowired
	private IGroupAccessLayer groupAccessLayer;
	public AnalyzeStatisticsWorker setGroupAccessLayer(IGroupAccessLayer value)
	{
		this.groupAccessLayer = value;
		return this;
	}
	
	@Autowired
	private ILogAccessLayer logAccessLayer;
	public AnalyzeStatisticsWorker setLogAccessLayer(ILogAccessLayer value)
	{
		this.logAccessLayer = value;
		return this;
	}
	
	public AnalyzeStatisticsWorker()
	{
		super("AnalyzeStatisticsWorker-Thread", "getting group day stats");
		this.idleSleepTime = 60;
	}

	@Override
	protected boolean executeTask() {
		Calendar tomorrow = Calendar.getInstance();
		tomorrow.set(tomorrow.get(Calendar.YEAR), tomorrow.get(Calendar.MONTH), tomorrow.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
		
		Group group = this.groupAccessLayer.getGroupToAnalyze(tomorrow.getTime());
		if(group == null)
		{
			return false;
		}
		
		GroupStatistics lastStat = this.groupAccessLayer.getLastStatisticForGroup(group.getId());
		Date windowStart = new Date(0);
		if(lastStat != null)
		{
			Calendar previousDate = Calendar.getInstance();
			previousDate.setTime(lastStat.getDate());
			previousDate.add(Calendar.DAY_OF_MONTH, 1);
			windowStart = previousDate.getTime();
		}
		Calendar windowEnd = Calendar.getInstance();
		windowEnd.set(windowEnd.get(Calendar.YEAR), windowEnd.get(Calendar.MONTH), windowEnd.get(Calendar.DAY_OF_MONTH), 0, 0);
		
		List<GroupStatistics> stats = this.logAccessLayer.getSizeOfLogsInGroup(Arrays.asList(group.getId()), windowStart, windowEnd.getTime());
		Collections.sort(stats);
		for(GroupStatistics stat: stats)
		{
			this.groupAccessLayer.addGroupStat(stat);
		}
		
		return true;
	}
}
