package com.loghobbit.service.v1;

import static org.mockito.Mockito.*;

import javax.ws.rs.core.Response;

import org.junit.Assert;
import org.junit.Test;

import com.loghobbit.entities.v1.TeamMemberType;

public class GroupSvc_DeleteGroupTest extends GroupSvc_TestBase {
	
	@Test
	public void deleteGroup_Valid() throws Exception
	{
		this.member.setType(TeamMemberType.owner);
		Response response = this.groupSvc.deleteGroup(GROUP_ID, USER_ID);
		
		Assert.assertNotNull(response);
		Assert.assertEquals("Status Code", 200, response.getStatus());
	}
	
	@Test
	public void deleteGroup_Admin() throws Exception
	{
		this.member.setType(TeamMemberType.admin);
		Response response = this.groupSvc.deleteGroup(GROUP_ID, USER_ID);
		
		this.validateErrorResponse(response, 403);
	}
	
	@Test
	public void deleteGroup_Member() throws Exception
	{
		this.member.setType(TeamMemberType.member);
		Response response = this.groupSvc.deleteGroup(GROUP_ID, USER_ID);
		
		this.validateErrorResponse(response, 403);
	}
	
	@Test
	public void deleteGroup_InvalidUser() throws Exception
	{
		this.member.setType(TeamMemberType.member);
		Response response = this.groupSvc.deleteGroup(GROUP_ID, "Bad user id");
		
		this.validateErrorResponse(response, 400);
	}
	
	@Test
	public void deleteGroup_NoGroup() throws Exception
	{
		reset(this.groupAccessLayer);
		this.member.setType(TeamMemberType.member);
		Response response = this.groupSvc.deleteGroup(GROUP_ID, USER_ID);
		
		this.validateErrorResponse(response, 400);
	}
	
	@Test
	public void deleteGroup_NullGroupId() throws Exception
	{
		this.member.setType(TeamMemberType.member);
		Response response = this.groupSvc.deleteGroup(null, USER_ID);
		
		this.validateErrorResponse(response, 400);
	}
}
