function()
{
	var roundToDay = new Date(this.dateMS);
	roundToDay.setHours(0,0,0,0);
	
	var entity = {
		groupId: this.groupId,
		size: this.size,
		date: roundToDay
	};
	emit(this.groupId + "-" + roundToDay, entity);
}