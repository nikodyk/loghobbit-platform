function logAlertCriteria(alertLine) {
	this.alertLine = $(alertLine);
	this.alertDetails = this.alertLine.find('.alert-details');
	var parent = this.alertDetails.parent();
	this.alertLink = parent.find('.log-alert-link');
	this.message = this.alertDetails.find('#messageRegex');
	this.originalMessage = this.message.val();
	this.exception = this.alertDetails.find('#exceptionRegex');
	this.server = this.alertDetails.find('#serverRegex');
	this.edit = this.alertLine.find('.alert-edit');
	this.cancel = this.alertDetails.find('#cancel');
	this.update = this.alertLine.find('#update');
	this.name = this.alertLine.find('#name')
	
	this.toggleArea = function(){
		if(this.alertDetails.is(':visible')){
			this.alertDetails.slideUp();
		} else {
			this.message.val(this.originalMessage);
			this.exception.val('');
			this.server.val('');
			this.alertDetails.slideDown();
		}
	};
	
	this.validateAndSubmit = function(){
		this.alertDetails.find('.has-error').removeClass('has-error')
		
		if(this.name.val() == ''){
			this.name.parent().addClass('has-error');
		}
		if(this.message.val() == ''){
			this.message.parent().addClass('has-error');
		}
		
		if(this.alertDetails.find('.has-error').length == 0){
			this.alertDetails.find('form').submit();
		} else {
			alert('There are errors in the alert.');
		}
	};
	
	var self = this;
	this.edit.click(function(){ 
		self.toggleArea(); 
	});
	this.cancel.click(function(){ self.toggleArea(); });
	this.update.click(function(){ self.validateAndSubmit(); });
}

var alertCriteria = new Array();
var alertAreas = new Array();
$(document).ready(function(){
	$('.log-alert-details').each(function(index){
		alertCriteria[alertCriteria.length] = new logAlertCriteria(this);
	});
});