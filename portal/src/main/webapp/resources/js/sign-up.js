function signUp() {
	var errorItems = $("#userData").find(".has-error");
	if(errorItems.length > 0) {
		return;
	}
	
	$("#userData").submit();
}