package com.loghobbit.portal.services;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class LogHobbitSystem {
	public static final LogHobbitSystem instance = new LogHobbitSystem();
	
	private String websiteUrl;
	public String getWebsiteUrl(){ return this.websiteUrl; }
	
	private String accountUrl;
	public String getAccountUrl(){ return this.accountUrl; }
	
	private String googleAnalyticsId;
	public String getGoogleAnalyticsId() { return this.googleAnalyticsId; }
	
	private LogHobbitSystem()
	{
		Properties properties = new Properties();
		
		InputStream stream = this.getClass().getResourceAsStream("/configuration.properties");
		try {
			properties.load(stream);
		} catch (IOException e) {
			throw new IllegalStateException("Could not initialize the system configurations", e);
		}
		
		this.websiteUrl = properties.getProperty("websiteUrl");
		this.accountUrl = properties.getProperty("accountUrl");
		this.googleAnalyticsId = properties.getProperty("google.analytics");
	}

}
