package com.loghobbit.service.dal;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.IndexOperations;
import org.springframework.data.mongodb.core.index.Index;
import org.springframework.data.mongodb.core.mapreduce.MapReduceResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.loghobbit.entities.LogEntry;
import com.loghobbit.entities.v1.AlertCriteria;
import com.loghobbit.entities.v1.GroupStatistics;
import com.loghobbit.entities.v1.LogFilter;
import com.loghobbit.entities.v1.MessagePart;
import com.loghobbit.service.dal.entities.GroupStatisticsStorage;
import com.loghobbit.service.dal.entities.LogEntryStorage;
import com.loghobbit.service.dal.entities.UniqueLogEntry;
import com.loghobbit.service.exceptions.AlertExistsException;

public class LogAccessLayer extends MongoDaoBase implements ILogAccessLayer {
	private static final int PAGE_SIZE = 25;
	private static final LogFilter EMPTY_FILTER = new LogFilter();
	private String groupSizeMap;
	private String groupSizeReduce;
	
	public void initialize()
	{
		IndexOperations logEntryStorage = this.getOperations().indexOps(LogEntryStorage.class);
		logEntryStorage.ensureIndex(new Index().on(LogEntryStorage.GROUP_ID, Direction.ASC).on(LogEntryStorage.DATE_MS, Direction.DESC));
		
		IndexOperations uniqueLogEntry = this.getOperations().indexOps(UniqueLogEntry.class);
		uniqueLogEntry.ensureIndex(new Index().on(UniqueLogEntry.GROUP_ID, Direction.ASC).on(UniqueLogEntry.MESSAGE, Direction.ASC).unique());
		
		IndexOperations alertCriteriaOps = this.getOperations().indexOps(AlertCriteria.class);
		alertCriteriaOps.ensureIndex(new Index().on(AlertCriteria.MESSAGE_REGEX, Direction.ASC)
												.on(AlertCriteria.EXCEPTION_REGEX, Direction.ASC)
												.on(AlertCriteria.SERVER_REGEX, Direction.ASC)
												.on(AlertCriteria.GROUP_ID, Direction.ASC)
												.unique());
		alertCriteriaOps.ensureIndex(new Index().on(AlertCriteria.ID, Direction.ASC));
		
		//
		// Upgrade data that needs it
		//
		Query getItemWNoSize = new Query(Criteria.where(LogEntryStorage.SIZE).exists(false));
		LogEntryStorage storage = null;
		while((storage = this.getOperations().findOne(getItemWNoSize, LogEntryStorage.class)) != null)
		{
			Query updateQuery = new Query(Criteria
					.where(LogEntryStorage.MESSAGE).is(storage.getLogEntry().getMessage())
					.and(LogEntryStorage.EXCEPTION).is(storage.getLogEntry().getException())
					.and(LogEntryStorage.SERVER).is(storage.getLogEntry().getServer())
					.and(LogEntryStorage.DATE_MS).is(storage.getDateMS()));
			
			Update update = new Update().set(LogEntryStorage.SIZE, this.calculateLogSize(storage.getLogEntry()));
			
			if(storage.getId() == null)
			{
				update.set(LogEntryStorage.ID, new ObjectId());
			}
			
			this.getOperations().updateFirst(updateQuery, update, LogEntryStorage.class);
		}
		
		this.groupSizeMap = this.getResourceString("/mapreduce/groupSizeMap.js");
		this.groupSizeReduce = this.getResourceString("/mapreduce/groupSizeReduce.js");
	}

	@Override
	public List<LogEntryStorage> getLogs(String groupId, Date requestDate, int page, LogFilter filter) {
		Criteria criteria = Criteria.where(LogEntryStorage.GROUP_ID).is(groupId);
		if(filter == null)
		{
			filter = EMPTY_FILTER;
		}
		if(filter.getBeforeDate() != null)
		{
			criteria.and(LogEntryStorage.DATE_MS).lt(filter.getBeforeDate().getTime());
		}
		else
		{
			criteria.and(LogEntryStorage.DATE_MS).lt(requestDate.getTime());
		}
		if(filter.getLevels().size() > 0)
		{
			criteria.and(LogEntryStorage.LEVEL).in(filter.getLevels());
		}
		if(filter.getMessageParts().size() > 0)
		{
			if(!filter.getMessageParts().contains(MessagePart.exception))
			{
				criteria.and(LogEntryStorage.EXCEPTION).is(null);
			}
			else if(!filter.getMessageParts().contains(MessagePart.message))
			{
				criteria.and(LogEntryStorage.EXCEPTION).ne(null);
			}
		}
		if(filter.getMessageRegex() != null)
		{
			criteria.and(LogEntryStorage.MESSAGE).regex(filter.getMessageRegex(), "i");
		}
		if(filter.getServerRegex() != null)
		{
			criteria.and(LogEntryStorage.SERVER).regex(filter.getServerRegex(), "i");
		}
		
		Query query = new Query(criteria);
		query.with(new Sort(Direction.DESC, LogEntryStorage.DATE_MS));
		query.limit(PAGE_SIZE);
		query.skip(page * PAGE_SIZE);
		List<LogEntryStorage> results = this.getOperations().find(query, LogEntryStorage.class);
		
		if(results == null) return new ArrayList<LogEntryStorage>(0);
		
		return results;
	}

	@Override
	public void addLogEntry(LogEntry logEntry, String groupId) {
		LogEntryStorage storage = new LogEntryStorage();
		storage.setLogEntry(logEntry);
		storage.setGroupId(groupId);
		storage.setDateMS(logEntry.getDate().getTime());
		storage.setSize(this.calculateLogSize(logEntry));
		
		this.getOperations().insert(storage);
	}

	@Override
	public void cleanUpOldLogs(String groupId, long time) {
		this.getOperations().remove(new Query(Criteria.where(LogEntryStorage.GROUP_ID).is(groupId)
														.and(LogEntryStorage.DATE_MS).lt(time)), LogEntryStorage.class);
	}

	@Override
	public int getNewLogCount(String id, Date requestDate) {
		return (int)this.getOperations().count(new Query(Criteria.where(LogEntryStorage.GROUP_ID).is(id)
													.and(LogEntryStorage.DATE_MS).gt(requestDate.getTime())), 
												LogEntryStorage.class);
	}

	@Override
	public UniqueLogEntry insertUniqueMessage(UniqueLogEntry entry) {
		entry.setMessageId(new ObjectId().toHexString());
		try
		{
			this.getOperations().insert(entry);
			return entry;
		}
		catch(Throwable e)
		{
			return this.getUniqueLogEntriesForLogEntry(entry.getGroupId(), entry.getLogEntry());
		}
	}

	@Override
	public UniqueLogEntry getUniqueLogEntriesForLogEntry(String groupId, LogEntry entry) {
		return this.getOperations().findOne(new Query(
				Criteria.where(UniqueLogEntry.GROUP_ID).is(groupId)
						.and(UniqueLogEntry.MESSAGE).is(entry.getMessage())), 
				UniqueLogEntry.class);
	}
	
	public List<GroupStatistics> getSizeOfLogsInGroup(List<String> groupIds, Date startDate, Date endDate)
	{
		Query query = new Query(Criteria.where(LogEntryStorage.GROUP_ID).in(groupIds)
									.and(LogEntryStorage.DATE_MS).gte(startDate.getTime()).lt(endDate.getTime()));
		
		MapReduceResults<GroupStatisticsStorage> results = this.getOperations().mapReduce(query, "LogEntry", this.groupSizeMap, this.groupSizeReduce, GroupStatisticsStorage.class);
		Iterator<GroupStatisticsStorage> iterator = results.iterator();
		List<GroupStatistics> retval = new ArrayList<GroupStatistics>();
		while(iterator.hasNext())
		{
			GroupStatisticsStorage item = iterator.next();
			if(item == null)
			{
				continue;
			}
			if(item.getValue().getLogCount() == 0)
			{
				item.getValue().setLogCount(1);
			}
			
			retval.add(item.getValue());
		}
		
		return retval;
	}
	
	private long calculateLogSize(LogEntry logEntry)
	{
		int size = 0;
		if(logEntry.getMessage() != null)
		{
			size += logEntry.getMessage().length();
		}
		if(logEntry.getException() != null)
		{
			size += logEntry.getException().length();
		}
		if(logEntry.getServer() != null)
		{
			size += logEntry.getServer().length();
		}
		if(logEntry.getLogName() != null)
		{
			size += logEntry.getLogName().length();
		}
		return size;
	}

	@Override
	public List<AlertCriteria> getAlertsById(List<String> alertIds) {
		Query query = new Query(Criteria.where(AlertCriteria.ID).in(alertIds));
		
		return this.getOperations().find(query, AlertCriteria.class);
	}

	@Override
	public void addAlert(AlertCriteria alertCriteria) throws AlertExistsException {
		try
		{
			alertCriteria.setId(new ObjectId().toHexString());
			this.getOperations().insert(alertCriteria);
		}
		catch(DuplicateKeyException e)
		{
			Query query = new Query(Criteria.where(AlertCriteria.MESSAGE_REGEX).is(alertCriteria.getMessageRegex())
											.and(AlertCriteria.EXCEPTION_REGEX).is(alertCriteria.getExceptionRegex())
											.and(AlertCriteria.SERVER_REGEX).is(alertCriteria.getServerRegex())
											.and(AlertCriteria.GROUP_ID).is(alertCriteria.getGroupId()));
			AlertCriteria existing = this.getOperations().findOne(query, AlertCriteria.class);
			
			throw new AlertExistsException().setExistingAlert(existing);
		}
	}

	@Override
	public AlertCriteria getAlertById(String id) {
		return this.getOperations().findById(id, AlertCriteria.class);
	}

	@Override
	public void updateAlert(AlertCriteria alertCriteria) {
		Query query = new Query(Criteria.where(AlertCriteria.ID).is(alertCriteria.getId()));
		Update update = new Update()
								.set(AlertCriteria.MESSAGE_REGEX, alertCriteria.getMessageRegex())
								.set(AlertCriteria.EXCEPTION_REGEX, alertCriteria.getExceptionRegex())
								.set(AlertCriteria.SERVER_REGEX, alertCriteria.getServerRegex())
								.set(AlertCriteria.NAME, alertCriteria.getName())
								.set(AlertCriteria.USERS, alertCriteria.getUsers());
		
		this.getOperations().updateFirst(query, update, AlertCriteria.class);
	}

	@Override
	public UniqueLogEntry getMatchingUniqueWOAlert(AlertCriteria alertCriteria) {
		Criteria criteria = Criteria.where(UniqueLogEntry.MESSAGE).regex(alertCriteria.getMessageRegex(), "i")
									.and(UniqueLogEntry.ALERT_IDS).nin(alertCriteria.getId());
		
		return this.getOperations().findOne(new Query(criteria), UniqueLogEntry.class);
	}

	@Override
	public void updateUniqueLogEntry(UniqueLogEntry entry) {
		Update update = new Update().set(UniqueLogEntry.ALERT_IDS, entry.getAlertCriteriaIds());
		Query query = new Query(Criteria.where(UniqueLogEntry.ID).is(entry.getMessageId()));
		
		this.getOperations().updateFirst(query, update, UniqueLogEntry.class);
	}
}
