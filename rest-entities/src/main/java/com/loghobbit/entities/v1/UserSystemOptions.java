package com.loghobbit.entities.v1;

public class UserSystemOptions {
	/**
	 * Specifies if the user has the ability to delegate admins
	 */
	private boolean canHaveAdmins;
	public boolean getCanHaveAdmins(){ return this.canHaveAdmins; }
	public void setCanHaveAdmins(boolean value){ this.canHaveAdmins = value; }
	
	/**
	 * The number of days before log messages are cleaned up
	 */
	private int daysBeforeLogCleanup;
	public int getDaysBeforeLogCleanup(){ return this.daysBeforeLogCleanup; }
	public void setDaysBeforeLogCleanup(int value){ this.daysBeforeLogCleanup = value; }
	
	/**
	 * The max number of megabytes the user can upload per day.
	 * -1 means no limit
	 */
	private int maxMbPerDay;
	public int getMaxMbPerDay(){ return this.maxMbPerDay; }
	public void setMaxMbPerDay(int value){ this.maxMbPerDay = value; }
	
	/**
	 * The max number of megabytes the user can upload per month
	 * -1 means no limit
	 */
	private int maxMbPerMonth;
	public int getMaxMbPerMonth(){ return this.maxMbPerMonth; }
	public void setMaxMbPerMonth(int value){ this.maxMbPerMonth = value; }
}
