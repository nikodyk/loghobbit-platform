package com.loghobbit.service.v1;

import static org.mockito.Mockito.*;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.security.InvalidParameterException;
import java.util.Arrays;

import com.loghobbit.entities.v1.Group;
import com.loghobbit.entities.v1.Notification;
import com.loghobbit.entities.v1.NotificationResponse;
import com.loghobbit.entities.v1.NotificationType;
import com.loghobbit.entities.v1.TeamMember;
import com.loghobbit.entities.v1.TeamMemberType;
import com.loghobbit.entities.v1.User;
import com.loghobbit.service.MailService;
import com.loghobbit.service.dal.IGroupAccessLayer;
import com.loghobbit.service.dal.INotificationAccessLayer;
import com.loghobbit.service.dal.IUserAccessLayer;

public class NotificationSvc_GroupNotificationResponse {
	private static final String GROUP_ID = "123";
	private static final String USER_ID = "456";
	private static final String PORTAL_URL = "http://localhost:1200/";
	
	private NotificationSvc notificationSvc;
	private IGroupAccessLayer groupAccessLayer;
	private INotificationAccessLayer notificationAccessLayer;
	private IUserAccessLayer userAccessLayer;
	private MailService mailService;
	
	private Notification notification;
	private Group group;
	private TeamMember owner;
	private TeamMember nonowner;
	private User user;
	
	@Before
	public void setup() throws IOException
	{
		this.groupAccessLayer = mock(IGroupAccessLayer.class);
		this.notificationAccessLayer = mock(INotificationAccessLayer.class);
		this.userAccessLayer = mock(IUserAccessLayer.class);
		this.mailService = mock(MailService.class);
		
		this.notificationSvc = new NotificationSvc()
									.setGroupAccessLayer(this.groupAccessLayer)
									.setNotificationAccessLayer(this.notificationAccessLayer)
									.setPortalUrl(PORTAL_URL)
									.setUserAccessLayer(this.userAccessLayer)
									.setMailService(this.mailService);
		this.notificationSvc.initialize();
		
		this.notification = new Notification();
		this.notification.setType(NotificationType.JoinGroup);
		when(this.notificationAccessLayer.getUserNotificationForGroup(GROUP_ID, USER_ID)).thenReturn(this.notification);
		
		this.owner = new TeamMember();
		this.owner.setAccepted(true);
		this.owner.setMemberId("owner@loghobbit.com");
		this.owner.setType(TeamMemberType.owner);
		
		this.nonowner = new TeamMember();
		this.nonowner.setAccepted(true);
		this.nonowner.setMemberId("nonowner@loghobbit.com");
		this.nonowner.setType(TeamMemberType.member);
		
		this.group = new Group();
		this.group.setAccessKey("567");
		this.group.setDescription("Test Group");
		this.group.setName("Test Group");
		this.group.setTeamMembers(Arrays.asList(this.owner, this.nonowner));
		when(this.groupAccessLayer.getGroupById(GROUP_ID)).thenReturn(this.group);
		
		this.user = new User();
		this.user.setFullName("Test User");
		this.user.setEmail(USER_ID);
		when(this.userAccessLayer.getUserFromId(USER_ID)).thenReturn(this.user);
	}
	
	@Test(expected=InvalidParameterException.class)
	public void groupNotificationResponse_NullResponseText()
	{
		this.notificationSvc.groupNotificationResponse(null, GROUP_ID, USER_ID);
	}
	
	@Test
	public void groupNotificationResponse_InvalidResponseText()
	{
		try
		{
			this.notificationSvc.groupNotificationResponse("test", GROUP_ID, USER_ID);
			Assert.assertTrue("No exception was thrown", false);
		}
		catch(InvalidParameterException e)
		{
		}
		
		verify(this.groupAccessLayer, times(0)).updateGroupMembers(this.group);
		verify(this.mailService, times(0)).sendMail(eq(this.owner.getMemberId()), anyString(), anyString());;
	}
	
	@Test
	public void groupNotificationResponse_NoGroup()
	{
		try
		{
			reset(this.groupAccessLayer);
			this.notificationSvc.groupNotificationResponse(NotificationResponse.Accept.name(), GROUP_ID, USER_ID);
			Assert.assertTrue("No exception was thrown", false);
		}
		catch(IllegalStateException e)
		{
		}
		
		verify(this.groupAccessLayer, times(0)).updateGroupMembers(this.group);
		verify(this.mailService, times(0)).sendMail(eq(this.owner.getMemberId()), anyString(), anyString());;
	}
	
	@Test
	public void groupNotificationResponse_Valid()
	{
		this.notificationSvc.groupNotificationResponse(NotificationResponse.Accept.name(), GROUP_ID, USER_ID);
		verify(this.groupAccessLayer).updateGroupMembers(this.group);
		verify(this.mailService).sendMail(eq(this.owner.getMemberId()), anyString(), anyString());;
	}
}