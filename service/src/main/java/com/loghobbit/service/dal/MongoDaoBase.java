package com.loghobbit.service.dal;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.data.mongodb.core.MongoOperations;

public class MongoDaoBase {
	private static final Logger logger = Logger.getLogger(MongoDaoBase.class);
	protected MongoConnection connection;
	public MongoConnection getConnection(){ return this.connection; }
	public MongoDaoBase setConnection(MongoConnection value)
	{
		this.connection = value;
		return this;
	}
	
	protected MongoOperations getOperations()
	{
		return this.connection.getOperations();
	}
	
	protected String getResourceString(String path)
	{
		InputStream stream = this.getClass().getResourceAsStream(path);
		if(stream == null)
		{
			return null;
		}
		
		String retval = null;
		try {
			retval = IOUtils.toString(stream);
			stream.close();
		} catch (IOException e) {
			logger.error("Could not open resource " + path, e);
		}
		return retval;
	}
}
