var globalGroupId;
var teamMembers = null;

function showGroupDialog(groupId, adminMode)
{
	globalGroupId = groupId;
	$("#group-dialog-members").empty();
	$("#group-form-name").val("");
	$("#group-form-description").val("");
	$("#group-form-invite-text").val("");
	
	$("#group-form-dialog-title").text((groupId == null)? "Create Group" : "Edit Group");
	if(groupId == null)
	{
		$("#group-form-dialog-title").text("Create Group");
	}
	else if(adminMode)
	{
		$("#group-form-dialog-title").text("Edit Group");
	}
	else
	{
		$("#group-form-dialog-title").text("View Group");
	}
	
	if(adminMode)
	{
		$("#group-dialog-invite").show();
		$("#group-form-actions-readonly").hide();
		$("#group-form-actions-admin").show();
	}
	else
	{
		$("#group-dialog-invite").hide();
		$("#group-form-actions-readonly").show();
		$("#group-form-actions-admin").hide();
	}
	
	if(groupId != null)
	{
		$.ajax({
			type : "get",
			url : "/group/" + groupId,
			accept: "application/json",
			success:function(data){
				$("#group-form-name").val(data.name);
				$("#group-form-description").val(data.description);
				teamMembers = null;
				for(var i = 0; i < data.teamMembers.length; i++)
				{
					var member = data.teamMembers[i];
					addMemberToDisplay(member.memberId, member.type, member.accepted, adminMode);
				}
				
				animateShowGroupDialog();
			},
			failed:function(){
				alert("An error occured while opening the group for edit");
			}
		});
	}
	else
	{
		animateShowGroupDialog();
	}
}

function animateShowGroupDialog()
{
	$("#group-form-dialog").show();
	$("#group-form-backdrop").show();
}

function closeGroupDialog()
{
	$("#group-form-dialog").hide();
	$("#group-form-backdrop").hide();
}

function inviteToGroup()
{
	var membersArea = $("#group-dialog-members");
	var inviteText = $("#group-form-invite-text");
	
	var inviteData = inviteText.val();
	if(inviteData.trim() == "")
	{
		return;
	}
	var parts = inviteData.split(";");
	for(var i = 0; i < parts.length; i++) {
		var memberFound = false;
		var children = membersArea.children();
		for(var ii = 0; ii < children.length; ii++){
			if($(children[ii]).text() == parts[i]){
				memberFound = true;
				break;
			}
		}
		if(memberFound == false){
			addMemberToDisplay(parts[i], "member", false, true);
		}
	}
	inviteText.val("");
}

function addMemberToDisplay(memberName, role, accepted, adminMode)
{
	var templateName = "#group-dialog-member-template";
	if(adminMode != true)
	{
		templateName = "#group-dialog-member-readonly-template";
	}
	
	var memberText = $(templateName).html();
	memberText = memberText.replace("{memberName}", memberName);
	
	memberText = memberText.replace("{status}", (accepted)? "fa-user success-icon" : "fa-envelope");
	var memberElement = null;
	
	if(adminMode == true)
	{
		memberElement = $(memberText);
		var selectedRole = "Member";
		switch(role)
		{
		case "owner":
			selectedRole = "member-role-owner";
			break;
		case "admin":
			selectedRole = "member-role-admin";
			break;
		case "member":
			selectedRole = "member-role-member";
			break;
		}
		
		var memberRole = memberElement.find("#member-role");
		memberRole.val(role);
		memberRole.on("change", teamRoleChanged);
	}
	else
	{
		memberText = memberText.replace("{memberRole}", role);
		memberElement = $(memberText);
	}
		
	$("#group-dialog-members").append(memberElement);
}

function teamRoleChanged()
{
	var results = $($($(this).parent()).parent()).find(".fa-envelope");
	if(this.value != 'member' && results.length > 0)
	{
		alert('The member must accept before they can be made an admin or owner')
		this.value = 'member';
		return;
	}
	if(this.value == 'owner')
	{
		$("#group-dialog-members").find(".member-role").each(function(){
			if(this.value == "owner")
			{
				this.value = 'member';
			}
		});
		this.value = 'owner';
	}
}

function removeFromInvite(item){
	$($($(item).parent()).parent()).remove();
}

function createGroup()
{
	var saveButton = $("#group-form-save"); 
	saveButton.empty();
	saveButton.append('<img src="/resources/images/small-spinner.gif"/>')
	var name = $("#group-form-name").val();
	var description = $("#group-form-description").val();
	var members = new Array();
	
	var membersArea = $("#group-dialog-members");
	var children = membersArea.children();
	for(var ii = 0; ii < children.length; ii++){
		var member = new Object();
		member.memberId = $(children[ii]).find("#memberId").text();
		var memberType = $(children[ii]).find("#memberType option:selected");
		member.type = memberType.val();
		members[ii] = member;
	}
	
	var postData = JSON.stringify({ id:globalGroupId, name:name, description:description, teamMembers:members });
	
	$.ajax({
		type : "PUT",
		url : "/group",
		data : postData,
		contentType: "application/json",
		success:function(data){
			$("#group-form-dialog").hide();
			$("#group-form-backdrop").hide();
			if(globalGroupId == null && data != null)
			{
				window.location = "/home?groupId=" + data;
			}
			else
			{
				location.reload();
			}
		},
		failed:function(){
			alert("An error occured while saving")
			saveButton.text("Save");
		}
	});
}