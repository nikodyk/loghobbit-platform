package com.loghobbit.service.v1;

import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.ws.rs.core.Response;

import org.bson.types.ObjectId;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.loghobbit.entities.LogEntry;
import com.loghobbit.entities.v1.AlertCriteria;
import com.loghobbit.entities.v1.Group;
import com.loghobbit.entities.v1.LogFilter;
import com.loghobbit.entities.v1.UserLogEntry;
import com.loghobbit.service.LogMessagingService;
import com.loghobbit.service.dal.IGroupAccessLayer;
import com.loghobbit.service.dal.ILogAccessLayer;
import com.loghobbit.service.dal.entities.LogEntryStorage;
import com.loghobbit.service.dal.entities.UniqueLogEntry;

public class LogSvc_GetLogsForGroup {
	private static final String GROUP_ID = "123";
	private static final String USER_ID = "456";
	private static final String ALERT_ID = "789";
	private static final String MESSAGE_ID = "012";
	
	private static final Date date = new Date();
	
	private LogSvc logSvc;
	private IGroupAccessLayer groupAccessLayer;
	private ILogAccessLayer logAccessLayer;
	private LogMessagingService logMessagingService;
	private List<LogEntryStorage> logs;
	private LogFilter filter;
	private Group group;
	
	@Before
	public void setup()
	{
		this.logs = new ArrayList<LogEntryStorage>();
		
		this.logAccessLayer = mock(ILogAccessLayer.class);
		when(this.logAccessLayer.insertUniqueMessage(any(UniqueLogEntry.class))).thenAnswer(new Answer<UniqueLogEntry>(){

			@Override
			public UniqueLogEntry answer(InvocationOnMock invocation) throws Throwable {
				return invocation.getArgumentAt(0, UniqueLogEntry.class);
			}
			
		});
		
		this.group = new Group();
		this.group.setId(GROUP_ID);
		this.groupAccessLayer = mock(IGroupAccessLayer.class);
		when(this.groupAccessLayer.getGroupByAccessKey(GROUP_ID)).thenReturn(this.group);
		
		this.logMessagingService = mock(LogMessagingService.class);
		
		this.logSvc = new LogSvc()
							.setGroupAccessLayer(this.groupAccessLayer)
							.setLal(this.logAccessLayer)
							.setLogMessagingService(this.logMessagingService);
	}
	
	private void setupLogs(int logCount)
	{
		if(this.logs != null)
		{
			for(int i = 0; i < logCount; i++)
			{
				LogEntryStorage log = new LogEntryStorage();
				log.setGroupId(GROUP_ID);
				log.setId(new ObjectId());
				log.setLogEntry(new LogEntry());
				log.getLogEntry().setMessage("test " + i);
				this.logs.add(log);
			}
		}
		
		when(this.logAccessLayer.getLogs(GROUP_ID, date, 0, filter)).thenReturn(this.logs);
	}
	
	@Test
	public void getLogsForGroup_Success()
	{
		this.setupLogs(1);
		Response response = this.logSvc.getLogsForGroup(this.filter, GROUP_ID, date, 0, USER_ID);

		this.verifyLogs(response, 1);
	}
	
	@Test
	public void getLogsForGroup_NullLogs()
	{
		this.logs = null;
		this.setupLogs(0);
		Response response = this.logSvc.getLogsForGroup(this.filter, GROUP_ID, date, 0, USER_ID);

		this.verifyLogs(response, 0);
	}
	
	@Test
	public void getLogsForGroup_EmptyLogs()
	{
		this.setupLogs(0);
		Response response = this.logSvc.getLogsForGroup(this.filter, GROUP_ID, date, 0, USER_ID);
		
		this.verifyLogs(response, 0);
	}
	
	@Test
	public void getLogsForGroup_Alerts()
	{
		this.setupLogs(1);
		UniqueLogEntry logEntry = new UniqueLogEntry();
		logEntry.setAlertCriteria(Arrays.asList(ALERT_ID));
		logEntry.setLogEntry(this.logs.get(0).getLogEntry());
		logEntry.setMessageId(MESSAGE_ID);
		
		when(this.logAccessLayer.getUniqueLogEntriesForLogEntry(GROUP_ID, logEntry.getLogEntry())).thenReturn(logEntry);
		
		AlertCriteria alert = new AlertCriteria();
		alert.setId(ALERT_ID);
		when(this.logAccessLayer.getAlertsById(Arrays.asList(ALERT_ID))).thenReturn(Arrays.asList(alert));
		
		Response response = this.logSvc.getLogsForGroup(this.filter, GROUP_ID, date, 0, USER_ID);
		
		List<UserLogEntry> results = this.verifyLogs(response, 1);
		Assert.assertEquals("Alert Count", 1, results.get(0).getAlertCriteria().size());
	}
	
	private List<UserLogEntry> verifyLogs(Response response, int logSize)
	{
		Assert.assertNotNull(response);
		Assert.assertEquals("Status Code", 200, response.getStatus());
		Assert.assertNotNull(response.getEntity());
		@SuppressWarnings("unchecked")
		List<UserLogEntry> logEntries = (List<UserLogEntry>)response.getEntity();
		Assert.assertEquals("Log Count", logSize, logEntries.size());
		
		return logEntries;
	}
}
