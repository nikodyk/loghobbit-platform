package com.loghobbit.service.workers;

import org.apache.log4j.Logger;

public abstract class ContinualWorkerBase extends Thread {
	protected static final Logger logger = Logger.getLogger(ContinualWorkerBase.class);
	
	protected boolean isRunning;
	protected int idleSleepTime = 5;
	private String taskDescription;
	
	public ContinualWorkerBase(String threadName, String taskDescription)
	{
		this.setName(threadName);
		this.taskDescription = taskDescription;
	}
	
	public void initialize()
	{
		this.isRunning = true;
		this.start();
	}
	
	public void shutdown()
	{
		this.isRunning = false;
	}
	
	@Override
	public void run()
	{
		while(this.isRunning)
		{
			try
			{
				if(!this.executeTask())
				{
					Thread.sleep(this.idleSleepTime * 1000);
				}
			}
			catch(Throwable e)
			{
				logger.error("An error occured while tryiing to " + this.taskDescription, e);
				try {
					Thread.sleep(this.idleSleepTime * 1000);
				} catch (InterruptedException e1) {
				}
			}
		}
	}
	
	protected abstract boolean executeTask();
}
