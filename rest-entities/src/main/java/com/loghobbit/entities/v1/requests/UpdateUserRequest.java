package com.loghobbit.entities.v1.requests;

public class UpdateUserRequest {
	private String email;
	public String getEmail(){ return this.email; }
	public UpdateUserRequest setEmail(String value)
	{
		this.email = value;
		return this;
	}
	
	private String fullName;
	public String getFullName(){ return this.fullName; }
	public UpdateUserRequest setFullName(String value)
	{
		this.fullName = value;
		return this;
	}
	
	private String currentPassword;
	public String getCurrentPassword(){ return this.currentPassword; }
	public UpdateUserRequest setCurrentPassword(String value)
	{
		this.currentPassword = value;
		return this;
	}
	
	private String password;
	public String getPassword(){ return this.password; }
	public UpdateUserRequest setPassword(String value)
	{
		this.password = value;
		return this;
	}
}
