package com.loghobbit.entities.v1.requests;

public class GetUserDetailsRequest {
	private String userToken;
	public String getUserToken(){ return this.userToken; }
	public GetUserDetailsRequest setUserToken(String value)
	{
		this.userToken = value;
		return this;
	}
	
	private String applicationId;
	public String getApplicationId(){ return this.applicationId; }
	public GetUserDetailsRequest setApplicationId(String value)
	{
		this.applicationId = value;
		return this;
	}
}
