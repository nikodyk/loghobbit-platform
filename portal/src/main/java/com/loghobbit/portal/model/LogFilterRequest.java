package com.loghobbit.portal.model;

public class LogFilterRequest {
	private String messageRegex;
	public String getMessageRegex(){ return this.messageRegex; }
	public LogFilterRequest setMessageRegex(String value)
	{ 
		this.messageRegex = value; 
		return this;
	}
	
	private String serverRegex;
	public String getServerRegex(){ return this.serverRegex; }
	public LogFilterRequest setServerRegex(String value)
	{
		this.serverRegex = value;
		return this;
	}
	
	private String beforeDate;
	public String getBeforeDate(){ return this.beforeDate; }
	public LogFilterRequest setBeforeDate(String value)
	{ 
		this.beforeDate = value;
		return this;
	}
	
	private String levels;
	public String getLevels(){ return this.levels; }
	public LogFilterRequest setLevels(String value)
	{
		this.levels = value;
		return this;
	}
	
	private String messageParts;
	public String getMessageParts(){ return this.messageParts; }
	public LogFilterRequest setMessageParts(String value){
		this.messageParts = value;
		return this;
	}
}
