<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
    	<title>Home</title>
        <link href="/resources/bootstrap/css/bootstrap.css" rel="stylesheet">
        <link href="/resources/style.css" rel="stylesheet">
    </head>
    <body>
        <div class="container row col-sm-3 col-sm-offset-4">
			<div class="row text-center">
				<h3>Login</h3>
			</div>
			<form method="post">
				<div class="form-group">
					<label class="control-label" for="email">Email:</label>
					<input type="email" id="email" name="email" class="form-control" placeholder="your.email@provider.com" />
				</div>
				<div class="form-group">
					<label class="control-label" for="password">Password:</label>
					<input type="password" id="password" name="password" class="form-control" placeholder="Password" />
				</div>
				<c:if test="${failed==true}">
					<div class="form-group has-error">
						<div class="control-label">Either the email or password was incorrect</div>
					</div>
				</c:if>
				<div class="form-group text-center">
					<input type="submit" class="btn btn-primary" value="Log-In"> <a href="signup" class="btn btn-primary">Sign Up</a>
				</div>
			</form>
		</div>
    </body>
</html>
