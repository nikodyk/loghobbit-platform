package com.loghobbit.portal.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.loghobbit.entities.v1.User;
import com.loghobbit.portal.filters.LoggedIn;
import com.loghobbit.service.clients.ServiceClientFactory;

@Controller
public class UserActionsController {
	private static Logger logger = Logger.getLogger(UserActionsController.class);
	
	@Autowired
	@Qualifier("ServiceClientFactory")
	public ServiceClientFactory clientFactory;
	public UserActionsController setClientFactory(ServiceClientFactory value)
	{
		this.clientFactory = value;
		return this;
	}
	
	@LoggedIn
	@RequestMapping(value="/verify/email", method= RequestMethod.GET)
	public String userEmailVerification(@RequestParam(required = true) String emailToken, HttpServletRequest request)
	{
		User user = (User) request.getSession().getAttribute(SessionItems.USER);
		try {
			if(this.clientFactory.getUserClient().validateEmail(emailToken, user))
			{
				user.setIsValidated(true);
			}
		} catch (Exception e) {
			logger.error("An error occured while verifying the email address", e);
		}
		
		return "redirect:/home";
	}
	
	@LoggedIn
	@RequestMapping(value="/verify/resend", method=RequestMethod.GET)
	public String userEmailVerification(HttpServletRequest request)
	{
		User user = (User) request.getSession().getAttribute(SessionItems.USER);
		try
		{
			this.clientFactory.getUserClient().resendValidateEmail(user);
		}
		catch(Exception e)
		{
			return "redirect:/home?emailSent=failed";
		}
		
		return "redirect:/home?emailSent=true";
	}
}
