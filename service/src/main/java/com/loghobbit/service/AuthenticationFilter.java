package com.loghobbit.service;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.loghobbit.service.dal.IUserAccessLayer;

public class AuthenticationFilter implements ContainerRequestFilter {
	private static final Logger logger = Logger.getLogger(AuthenticationFilter.class);
	private static final String LOGIN_PATH = "v1/user/login";
	private static final String SIGNUP_PATH = "v1/user/signup";
	private static final int METHOD_NULL = 400;
	
	public static final String USER_DATA = "user";

	@Autowired
	private IUserAccessLayer userAccessLayer;
	public AuthenticationFilter setUserAccessLayer(IUserAccessLayer value)
	{
		this.userAccessLayer = value;
		return this;
	}
	
	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		String path = requestContext.getUriInfo().getPath();
		if(path == null)
		{
			requestContext.abortWith(Response.status(METHOD_NULL).build());
			return;
		}
		
		logger.info(path);
		if(path.endsWith(LOGIN_PATH) || path.endsWith(SIGNUP_PATH) || path.endsWith("version") || path.endsWith("v1/logs/entry") || path.startsWith("v1/subscription"))
		{
			// This is valid, as it is a request to login
			return;
		}
		
		String token = requestContext.getHeaderString("token");
		if(token == null || token.length() == 0)
		{
			logger.warn("There was no token provided");
			requestContext.abortWith(Response.status(Status.FORBIDDEN).build());
			return;
		}
		
		String userId = this.userAccessLayer.getUserId(token);
		if(userId == null || userId.length() == 0)
		{
			logger.warn(String.format("The user request for '%s' failed to pass validation", path));
			requestContext.abortWith(Response.status(Status.FORBIDDEN).build());
			return;
		}
		
		requestContext.getHeaders().add(USER_DATA, userId);
	}
}
