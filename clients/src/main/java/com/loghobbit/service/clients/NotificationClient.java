package com.loghobbit.service.clients;

import org.apache.cxf.helpers.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;

import com.loghobbit.entities.v1.Notification;
import com.loghobbit.entities.v1.NotificationResponse;
import com.loghobbit.entities.v1.User;

public class NotificationClient extends ServiceClientBase {
	public NotificationClient(String endpoint, HttpClient client)
	{
		super(endpoint, client);
	}

	public Notification[] getNotifications(User user) throws Exception {
		HttpGet get = new HttpGet(String.format("%s/notifications", this.endpoint));
		this.addCommonHeaders(get, user);
		
		HttpResponse response = this.client.execute(get);
		this.handleErrorResponse(response);
		
		if(response.getStatusLine().getStatusCode() == 204)
		{
			return null;
		}
		
		Notification[] retval = mapper.readValue(response.getEntity().getContent(), Notification[].class);
		response.getEntity().getContent().close();
		
		return retval;
	}

	public void groupJoinResponse(String groupId, NotificationResponse notificationResponse, User user) throws Exception {
		HttpPut put = new HttpPut(String.format("%s/notifications/group/%s", this.endpoint, groupId));
		put.setEntity(new StringEntity(notificationResponse.toString()));
		this.addCommonHeaders(put, user);
		
		HttpResponse response = this.client.execute(put);
		
		this.handleErrorResponse(response);
		
		IOUtils.consume(response.getEntity().getContent());
		response.getEntity().getContent().close();
	}
}
