package com.loghobbit.service.dal;

import com.loghobbit.entities.v1.User;
import com.loghobbit.entities.v1.UserSystemOptions;
import com.loghobbit.entities.v1.requests.UpdateUserRequest;

public interface IUserAccessLayer {
	User login(String email, byte[] password);

	/**
	 * Checks to see if email is already registered for a user
	 * @param email the email address
	 * @return true if the email exists
	 */
	boolean doesEmailExist(String email);

	/**
	 * Registers a user
	 * @param user the user information to register
	 */
	void registerUser(User user);

	/**
	 * Gets the user's identifier based off of the token
	 * @param token the token of the user
	 * @return the user's id
	 */
	String getUserId(String token);

	/**
	 * Updates the subscription level for the current user
	 * @param userId the id of the user
	 * @param level the subscription level
	 */
	void updateSubscription(String userId, UserSystemOptions options);

	/**
	 * Gets a user which needs its logs examined for cleanup
	 * @return a user object
	 */
	User getUserForCleanup();

	/**
	 * Gets a user from their id
	 * @param userId the id of the user
	 * @return the User object
	 */
	User getUserFromId(String userId);

	/**
	 * Updates the user with the information provided
	 * @param update the update to make
	 * @param oldPassword the old password (this is nessisary if a new password is being given)
	 * @param newPassword the new password (hashed) to override the old one with
	 * @param userId the id of the user
	 */
	boolean updateUser(UpdateUserRequest user, byte[] oldPassword, byte[] newPassword, String userId);

	/**
	 * Gets an sso token to use
	 * @param userId the id of the user
	 * @return the sso token
	 */
	String getSSOToken(String userId);

	/**
	 * Validates an sso token
	 * @param token the token to verify
	 * @return the user object
	 */
	User validateToken(String token);

	/**
	 * This method will mark the user as having been verified
	 * @param userId the id of the user
	 */
	void userVerified(String userId);
}
