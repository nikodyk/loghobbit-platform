package com.loghobbit.service.v1;

import java.io.UnsupportedEncodingException;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.http.entity.StringEntity;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.loghobbit.entities.v1.User;
import com.loghobbit.entities.v1.requests.GetUserDetailsRequest;
import com.loghobbit.entities.v1.requests.SubscriptionUpdateRequest;
import com.loghobbit.service.dal.IUserAccessLayer;

@Path("/v1/subscription")
public class SubscriptionSvc {
	private static final Logger logger = Logger.getLogger(SubscriptionSvc.class);
	
	@Autowired
	private IUserAccessLayer userAccesslayer;
	public SubscriptionSvc setUserAccessLayer(IUserAccessLayer value)
	{
		this.userAccesslayer = value;
		return this;
	}
	
	@Value("${subscription.applicationId}")
	private String adminApplicationId;
	public void setAdminApplicationId(String value){ this.adminApplicationId = value; }
	
	@Path("/update")
	@POST
	public void updateSubscription(SubscriptionUpdateRequest request)
	{
		this.authenticate(request.getApplicationId());
		
		this.userAccesslayer.updateSubscription(request.getUserId(), request.getSystemOptions());
	}
	
	@Path("/details")
	@POST
	public Response getSubscriptionDetails(GetUserDetailsRequest request) throws UnsupportedEncodingException
	{
		this.authenticate(request.getApplicationId());
		String userId = this.userAccesslayer.getUserId(request.getUserToken());
		if(userId == null)
		{
			return Response.status(Status.BAD_REQUEST).header("content-type", "text/plain").entity(new StringEntity("Could not find the user from the token specified")).build();
		}
		
		User user = this.userAccesslayer.getUserFromId(userId);
		return Response.ok(user).build();
	}
	
	private void authenticate(String appId)
	{
		if(this.adminApplicationId == null || !this.adminApplicationId.equals(appId))
		{
			logger.warn("A call was made to the admin api which isn't valid (appId: " + ((appId == null)? "" : appId) );
			throw new SecurityException();
		}
	}
}
