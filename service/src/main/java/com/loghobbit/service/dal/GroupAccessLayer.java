package com.loghobbit.service.dal;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.mongodb.core.IndexOperations;
import org.springframework.data.mongodb.core.index.Index;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.loghobbit.entities.v1.Group;
import com.loghobbit.entities.v1.GroupNotificationSetting;
import com.loghobbit.entities.v1.GroupStatistics;
import com.loghobbit.entities.v1.TeamMember;
import com.loghobbit.entities.v1.TeamMemberType;
import com.loghobbit.service.dal.entities.GroupNotificationSettingStorage;

public class GroupAccessLayer extends MongoDaoBase implements IGroupAccessLayer {
	public void initialize()
	{
		IndexOperations organization = this.getOperations().indexOps(Group.class);
		organization.ensureIndex(new Index().on(Group.ACCESS_KEY, Direction.ASC).unique());
		organization.ensureIndex(new Index().on(Group.USER_EMAIL, Direction.ASC));
	}

	@Override
	public void addOrganization(Group organization) {
		this.getOperations().insert(organization);
	}

	@Override
	public List<Group> getGroupsOwnedByUser(String email) {
		Query query = new Query(Criteria.where(Group.USER_EMAIL).is(email));
		query.with(new Sort(Direction.ASC, Group.NAME));
		List<Group> retval = this.getOperations().find(query, Group.class);
		
		Iterator<Group> iterator = retval.iterator();
		while(iterator.hasNext())
		{
			Group group = iterator.next();
			
			for(TeamMember member : group.getTeamMembers())
			{
				if(member.getMemberId().equals(email) && member.getType() != TeamMemberType.owner)
				{
					iterator.remove();
					break;
				}
			}
		}
		
		return retval;
	}
	
	@Override
	public List<Group> getGroupsForMember(String email) {
		Query query = new Query(Criteria.where(Group.USER_EMAIL).is(email));
		query.with(new Sort(Direction.ASC, Group.NAME));
		List<Group> retval = this.getOperations().find(query, Group.class);

		Iterator<Group> iterator = retval.iterator();
		while(iterator.hasNext())
		{
			Group group = iterator.next();
			
			for(TeamMember member : group.getTeamMembers())
			{
				if(member.getMemberId().equals(email))
				{
					if(!member.getAccepted())
					{
						iterator.remove();
					}
					break;
				}
			}
		}
		
		return retval;
	}
	
	@Override
	public void updateGroup(Group group) {
		Update update = new Update()
							.set(Group.NAME, group.getName())
							.set(Group.ACCESS_KEY, group.getAccessKey())
							.set(Group.DESCRIPTION, group.getDescription())
							.set(Group.TEAM_MEMBERS, group.getTeamMembers());
		this.getOperations().updateFirst(new Query(Criteria.where(Group.ID).is(group.getId())), update, Group.class);
	}
	
	@Override
	public void updateGroupMembers(Group group) {
		Update update = new Update()
			.set(Group.TEAM_MEMBERS, group.getTeamMembers());
		this.getOperations().updateFirst(new Query(Criteria.where(Group.ID).is(group.getId())), update, Group.class);
	}

	@Override
	public Group getGroupByAccessKey(String accessKey) {
		return this.getOperations().findOne(new Query(Criteria.where(Group.ACCESS_KEY).is(accessKey)), Group.class);
	}

	@Override
	public String addGroup(Group group) {
		group.setAccessKey(new ObjectId().toHexString());
		group.setId(group.getAccessKey());
		this.getOperations().insert(group);
		return group.getAccessKey();
	}

	@Override
	public Group getGroupById(String id) {
		return this.getOperations().findOne(new Query(Criteria.where(Group.ID).is(id)), Group.class);
	}

	@Override
	public void deleteGroup(String groupId) {
		this.getOperations().remove(new Query(Criteria.where(Group.ID).is(groupId)), Group.class);
	}

	@Override
	public GroupNotificationSetting getGroupMemberNotificationSetting(String groupId, String userId) {
		GroupNotificationSettingStorage result = this.getOperations().findOne(
									new Query(
										Criteria.where(GroupNotificationSettingStorage.GROUP_ID).is(groupId)
												.and(GroupNotificationSettingStorage.USER_ID).is(userId)), 
									GroupNotificationSettingStorage.class);
		return (result != null)? result.getSetting() : null;
	}

	@Override
	public Group getGroupToAnalyze(Date tomorrowsDate) {
		Query query = new Query(new Criteria().orOperator(
				Criteria.where(Group.LAST_ANALYZED_DATE).exists(false),
				Criteria.where(Group.LAST_ANALYZED_DATE).lt(tomorrowsDate.getTime())));
		query.limit(1);
		
		Update update = new Update().set(Group.LAST_ANALYZED_DATE, tomorrowsDate.getTime());
		
		return this.getOperations().findAndModify(query, update, Group.class);
	}

	@Override
	public GroupStatistics getLastStatisticForGroup(String groupId) {
		Query query = new Query(Criteria.where(GroupStatistics.GROUP_ID).is(groupId));
		query.with(new Sort(new Order(Direction.DESC, GroupStatistics.DATE)));
		
		return this.getOperations().findOne(query, GroupStatistics.class);
	}

	@Override
	public void addGroupStat(GroupStatistics stat) {
		this.getOperations().insert(stat);
	}

	@Override
	public List<GroupStatistics> getGroupsStats(List<String> groupIds, Date start, Date end) {
		Query query = new Query(Criteria.where(GroupStatistics.GROUP_ID).in(groupIds)
										.and(GroupStatistics.DATE).gte(start).lte(end));
		
		return this.getOperations().find(query, GroupStatistics.class);
	}
}
