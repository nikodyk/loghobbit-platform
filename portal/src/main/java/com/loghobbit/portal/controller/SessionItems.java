package com.loghobbit.portal.controller;

public class SessionItems {
	public static final String USER = "user";
	public static final String LAST_LOG_GET = "logs.last_get";
	public static final String GROUP = "log.group";
	public static final String LOG_REQUEST_DATE = "log.group.requestdate";
	public static final String LOG_LATEST_DATE = "log.group.latestdate";
	public static final String LOG_ON_PAGE = "log.group.onpage";
	public static final String GROUPS = "user.groups";
	public static final String INITIAL_URL = "initial.url";
	public static final String WEBSITE_URL = "websiteUrl";
	public static final String ACCOUNT_URL = "accountUrl";
	public static final String GOOGLE_ANALYTICS = "analyticsId";
	public static final String FILTER = "filter";
}
