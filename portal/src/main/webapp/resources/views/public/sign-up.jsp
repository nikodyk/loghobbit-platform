<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Home</title>
	<link href="/resources/bootstrap/css/bootstrap.css" rel="stylesheet">
	<link href="/resources/style.css" rel="stylesheet">
	<script type="text/javascript" src="/resources/js/jquery-2.1.4.min.js"></script>
	<script type="text/javascript" src="/resources/js/userVerification.js"></script>
	<script type="text/javascript" src="/resources/js/sign-up.js"></script>
</head>
<body>
	<div class="container row col-sm-3 col-sm-offset-4">
		<div class="row text-center">
			<h3>Sign Up</h3>
		</div>
		<form id="userData" action="/signup" method="post">
			<div class="form-group ${fullNameFlag}">
				<div class="control-label">Full Name:</div>
				<input type="text" name="fullName" class="form-control fullName" placeholder="John Doe" value="${details.fullName}" />
			</div>
			<div class="form-group ${emailFlag}">
				<div class="control-label">Email:</div>
				<input type="email" name="email" class="form-control email" placeholder="your.email@provider.com" value="${details.email}" />
			</div>
			<div class="form-group ${passwordFlag}">
				<div class="control-label">Password:</div>
				<input type="password" name="password" class="form-control password" placeholder="Password" />
				<c:if test="${passwordError}">
					<div class="control-label">${passwordError}</div>
				</c:if>
			</div>
		</form>
		<div class="form-group">
			<div class="control-label">Comfirm Password:</div>
			<input type="password" name="password" class="form-control password-verify" placeholder="Re-type Password" />
		</div>
		<p>
			By clicking "Sign Up" you are agreeing to LogHobbit's <a
				href="termsofuse">terms of use</a>.
		</p>
		<div class="form-group text-center">
			<a onClick="signUp()" class="btn btn-primary">Sign Up</a>
		</div>
	</div>
</body>
</html>
