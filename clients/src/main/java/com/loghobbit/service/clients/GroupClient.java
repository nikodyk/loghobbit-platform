package com.loghobbit.service.clients;

import java.util.Date;

import org.apache.cxf.helpers.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;

import com.loghobbit.entities.v1.Group;
import com.loghobbit.entities.v1.GroupStatistics;
import com.loghobbit.entities.v1.User;

public class GroupClient extends ServiceClientBase {
	
	private String usersGroupsEndpoint;
	private String usersGroupEndpoint;
	
	public GroupClient(String endpoint, HttpClient client)
	{
		super(endpoint, client);
		
		this.usersGroupsEndpoint = String.format("%s/v1/groups", this.endpoint);
		this.usersGroupEndpoint = String.format("%s/v1/groups", this.endpoint);
	}
	
	public Group[] getUsersGroups(String accessToken) throws Exception
	{
		HttpGet get = new HttpGet(this.usersGroupsEndpoint);
		get.setHeader("token", accessToken);
		get.setHeader("Content-Type", "application/json");
		get.setHeader("Accept", "application/json");
		
		HttpResponse response = this.client.execute(get);
		if(response.getStatusLine().getStatusCode() == 204)
		{
			return null;
		}
		
		this.handleErrorResponse(response);
		
		Group[] retval = mapper.readValue(response.getEntity().getContent(), Group[].class);
		response.getEntity().getContent().close();
		return retval;
	}

	public String putGroup(String content, User user) throws Exception {
		HttpPut put = new HttpPut(this.usersGroupEndpoint);
		put.setHeader("token", user.getSecurityToken());
		put.setHeader("Content-Type", "application/json");
		put.setHeader("Accept", "application/json");
		put.setEntity(new StringEntity(content));
		
		HttpResponse response = this.client.execute(put);
		
		this.handleErrorResponse(response);
		
		if(response.getStatusLine().getStatusCode() == 204)
		{
			return null;
		}
		
		String id = IOUtils.toString(response.getEntity().getContent());
		response.getEntity().getContent().close();
		return id;
	}

	public void deleteGroup(String groupId, User user) throws Exception
	{
		HttpDelete delete = new HttpDelete(String.format("%s/v1/groups/%s", this.endpoint, groupId));
		delete.setHeader("token", user.getSecurityToken());
		delete.setHeader("Accept", "text/plain");
		
		HttpResponse response = this.client.execute(delete);
		
		if(response.getStatusLine().getStatusCode() != 200)
		{
			String message = response.getEntity().getContent().toString();
			throw new Exception(message);
		}
	}

	public void leaveGroup(String groupId, User user) throws Exception {
		HttpGet get = new HttpGet(String.format("%s/v1/groups/%s/leave", this.endpoint, groupId));
		//this.addCommonHeaders(post, user);
		get.setHeader("token", user.getSecurityToken());
		
		HttpResponse response = this.client.execute(get);
		
		this.handleErrorResponse(response);
	}

	public GroupStatistics[] getStats(Date startDate, Date endDate, User user) throws Exception 
	{
		HttpGet get = new HttpGet(String.format("%s/v1/groups/stats", this.endpoint));
		this.addCommonHeaders(get, user);
		get.setHeader("startDate", startDate.toString());
		get.setHeader("endDate", endDate.toString());
		
		HttpResponse response = this.client.execute(get);
		
		this.handleErrorResponse(response);
		
		GroupStatistics[] retval = mapper.readValue(response.getEntity().getContent(), GroupStatistics[].class);
		response.getEntity().getContent().close();
		return retval;
	}
}
