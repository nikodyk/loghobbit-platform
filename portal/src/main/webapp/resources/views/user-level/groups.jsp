<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>LogHobbit - Home</title>
	<jsp:include page="../../fragments/header.jsp"/>
	
	<script type="text/javascript" src="/resources/js/userVerification.js"></script>
	<script type="text/javascript" src="/resources/js/sign-up.js"></script>
	<script type="text/javascript" src="/resources/js/groups.js"></script>
</head>
<body>
	<jsp:include page="../../fragments/navbar.jsp" />    
	<div class="container">
		<div><h1>Your Groups</h1></div>

		<table class="table table-condensed table-striped">
			<tr><td><b>Group Name</b></td><td class="text-center"><b>Actions</b></td></tr>
			<c:forEach var="item" items="${groups}">
				<tr>
					<td>${item.group.name}</td>
					<td class="group-options text-center">
						<a href="/home?groupId=${item.group.id}">View</a>
						<a id="detailsLink_${item.group.id }">Details</a>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="group-details" style="${(item.group.id == null)? '' : 'display:none'}">
						<div id="group-content" style="${(item.group.id == null)? '' : 'display:none'}">
							<input id="group-id" type="hidden" value="${item.group.id }"/>
							<input id="member-type" type="hidden" value="${item.userMemberType }"/>
							<div class="col-sm-5">
								<div>
									<div class="control-label spacer">
										<strong>Group Name:</strong>
									</div>
									<input id="group-name" class="form-control" type="text" value="${(item.group.id == null)? '' : item.group.name }" ${(item.userMemberType == "member")? "readonly" : "" } ${(item.group.id == null)? 'autofocus' : '' }/>
								</div>
								<div>
									<div class="control-label spacer">
										<strong>Access Key:</strong>
									</div>
									<input id="group-access-key" class="form-control" type="text" value="${item.group.accessKey }" ${(item.userMemberType == "member" || item.group.id == null)? "readonly" : "" } />
								</div>
								<div>
									<div class="control-label spacer">
										<strong>Description:</strong>
									</div>
									<textarea id="group-description" class="form-control" ${(item.userMemberType == "member")? "readonly" : "" }>${item.group.description }</textarea>
								</div>
							</div>
							<div class="col-sm-7">
								<div class="spacer"><strong>Members:</strong></div>
								<c:if test="${item.userMemberType != 'member' }">
									<div id="group-dialog-invite" class="row">
										<div class="col-xs-10"><input id="group-invite-text" type="text" class="form-control" placeholder="Email address" /></div> 
										<div class="col-xs-2"><a id="group-invite-button" class="btn btn-primary">Invite</a></div>
									</div>
								</c:if>
								<div>
									<div id="group-members" class="memberList memberList-large">
										<c:forEach var="member" items="${item.group.teamMembers}">
											<div class="row">
												<div id="memberId" class="col-xs-9">${member.memberId} <i class="fa ${(member.accepted)? "fa-user success-icon" : "fa-envelope" }"></i></div>
												<div id="memberType" class="col-xs-2 ">
													<c:choose>
														<c:when test="${item.userMemberType != 'member'}">
															<select id="member-role" class="member-role">
																<option class="member-role-owner" value="owner" ${(member.type == 'owner')? 'selected' : '' }>Owner</option>
																<c:if test="${canHaveAdmins}">
																	<option class="member-role-admin" value="admin" ${(member.type == 'admin')? 'selected' : '' }>Admin</option>
																</c:if>
																<option class="member-role-member" value="member" ${(member.type == 'member')? 'selected' : '' }>Member</option>
															</select>
															<a href="#" style="color:red;" onclick="removeFromInvite(this)">
																<i class="fa fa-times"></i>
															</a>
														</c:when>
														<c:otherwise>
															${member.type}
														</c:otherwise>
													</c:choose>
												</div>
											</div>
										</c:forEach>
									</div>
								</div>
							</div>
							<div class="col-sm-12 spacer actions">
								<c:choose>
									<c:when test="${item.group.id == null}">
										<a id="details-update" class="btn btn-primary">Create</a>
										<a href="/groups" class="btn btn-primary">Cancel</a>
									</c:when>
									<c:otherwise>
										<c:choose>
											<c:when test="${item.userMemberType == 'member' }">
												<a href="?leaveId=${item.group.id }" class="btn btn-primary">Leave</a>
											</c:when>
											<c:when test="${item.userMemberType== 'admin' }">
												<a id="details-update" class="btn btn-primary">Update</a>
												<a href="?leaveId=${item.group.id }" class="btn btn-primary">Leave</a>
											</c:when>
											<c:otherwise>
												<a id="details-update" class="btn btn-primary">Update</a>
												<a href="?deleteId=${item.group.id }" class="btn btn-primary">Delete</a>
											</c:otherwise>
										</c:choose>
										<c:if test="${item.group.id != null }">
											<a id="details-close" class="btn btn-primary">Close</a>
									</c:if>
									</c:otherwise>
								</c:choose>
							</div>
						</div>
					</td>
				</tr>
			</c:forEach>
		</table>
		<a href="?isNew=true" class="btn btn-primary">Create New</a>
		
		<div id="group-dialog-member-template" style="display:none;">
			<div class="row">
				<div id="memberId" class="col-xs-9">${member.memberId}<i class="fa {status}"></i></div>
				<div id="memberType" class="col-xs-2 ">
					<select id="member-role" class="member-role">
						<option class="member-role-owner" value="owner">Owner</option>
						<c:if test="${canHaveAdmins}">
							<option class="member-role-admin" value="admin">Admin</option>
						</c:if>
						<option class="member-role-member" value="member" selected>Member</option>
					</select>
					<a href="#" style="color:red;" onclick="removeFromInvite(this)">
						<i class="fa fa-times"></i>
					</a>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../../fragments/group-edit-form.jsp" />
</body>
</html>