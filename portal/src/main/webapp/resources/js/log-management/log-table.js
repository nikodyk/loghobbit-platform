function toggleFilter()
{
	var filterRow = $("#log-filters");
	
	if(filterRow.is(":visible")){
		filterRow.hide();
	} else {
		filterRow.show();
	}
}