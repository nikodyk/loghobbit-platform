package com.loghobbit.entities.v1;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.loghobbit.entities.Level;

public class LogFilter {
	private String messageRegex;
	public String getMessageRegex(){ return this.messageRegex; }
	public LogFilter setMessageRegex(String value)
	{
		this.messageRegex = value;
		return this;
	}
	
	private String serverRegex;
	public String getServerRegex(){ return this.serverRegex; }
	public LogFilter setServerRegex(String value)
	{
		this.serverRegex = value;
		return this;
	}
	
	private Date beforeDate;
	public Date getBeforeDate(){ return this.beforeDate; }
	public LogFilter setBeforeDate(Date value)
	{
		this.beforeDate = value;
		return this;
	}
	
	private List<Level> levels;
	public List<Level> getLevels()
	{
		if(this.levels == null) this.levels = new ArrayList<Level>();
		return this.levels;
	}
	public LogFilter setLevels(List<Level> value)
	{
		this.levels = value;
		return this;
	}
	
	private List<MessagePart> messageParts;
	public List<MessagePart> getMessageParts()
	{
		if(this.messageParts == null) this.messageParts = new ArrayList<MessagePart>();
		return this.messageParts;
	}
	public LogFilter setMessageParts(List<MessagePart> value)
	{
		this.messageParts = value;
		return this;
	}
}
