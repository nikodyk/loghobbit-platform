package com.loghobbit.service.workers;

import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;

import com.loghobbit.entities.Level;
import com.loghobbit.entities.LogEntry;
import com.loghobbit.entities.v1.Group;
import com.loghobbit.entities.v1.GroupNotificationSetting;
import com.loghobbit.entities.v1.TeamMember;
import com.loghobbit.service.dal.IGroupAccessLayer;
import com.loghobbit.service.dal.ILogAccessLayer;
import com.loghobbit.service.dal.entities.UniqueLogEntry;
import com.loghobbit.service.v1.NotificationSvc;

public class AnalyzeUniqueLogEntryWorkerTest {
	private static final String GROUP_ID = "1234";
	private static final String MEMBER_ID = "567";
	
	private AnalyzeUniqueLogEntryWorker worker;
	private IGroupAccessLayer groupAccessLayer;
	private ILogAccessLayer logAccessLayer;
	private NotificationSvc notificationSvc;
	private UniqueLogEntry logEntry;
	private Group group;
	private GroupNotificationSetting setting = new GroupNotificationSetting();
	
	@Before
	public void setup()
	{
		this.groupAccessLayer = mock(IGroupAccessLayer.class);
		this.logAccessLayer = mock(ILogAccessLayer.class);
		this.notificationSvc = mock(NotificationSvc.class);
		
		LogEntry entry = new LogEntry();
		entry.setLevel(Level.error);
		entry.setLogName("Log Name");
		
		this.logEntry = new UniqueLogEntry();
		this.logEntry.setGroupId(GROUP_ID);
		this.logEntry.setLogEntry(entry);
		when(this.logAccessLayer.insertUniqueMessage(this.logEntry)).thenReturn(this.logEntry);
		
		this.group = new Group();
		
		TeamMember member = new TeamMember();
		member.setMemberId(MEMBER_ID);
		member.setAccepted(true);
		this.group.getTeamMembers().add(member);
		when(this.groupAccessLayer.getGroupById(GROUP_ID)).thenReturn(this.group);
		
		this.worker = new AnalyzeUniqueLogEntryWorker()
							.setGroupAccessLayer(this.groupAccessLayer)
							.setLogAccessLayer(this.logAccessLayer)
							.setNotificationSvc(this.notificationSvc)
							.setEntry(this.logEntry);
	}
	
	@Test
	public void testNotify()
	{
		this.worker.run();
		
		verify(this.notificationSvc).sendLogAlert(eq(this.logEntry.getLogEntry()), eq(MEMBER_ID), eq(this.group), any(GroupNotificationSetting.class));
	}
	
	@Test
	public void memberNotAccepted()
	{
		this.group.getTeamMembers().get(0).setAccepted(false);
		this.worker.run();
		
		verify(this.notificationSvc, times(0)).sendLogAlert(eq(this.logEntry.getLogEntry()), eq(MEMBER_ID), eq(this.group), any(GroupNotificationSetting.class));
	}
	
	@Test
	public void ExistingSetting()
	{
		when(this.groupAccessLayer.getGroupMemberNotificationSetting(anyString(), anyString())).thenReturn(this.setting);
		this.worker.run();
		
		verify(this.notificationSvc).sendLogAlert(eq(this.logEntry.getLogEntry()), eq(MEMBER_ID), eq(this.group), any(GroupNotificationSetting.class));
	}
	
	@Test
	public void ExistingSetting_OptOut()
	{
		this.setting.setOptOutNotifications(true);
		when(this.groupAccessLayer.getGroupMemberNotificationSetting(anyString(), anyString())).thenReturn(this.setting);
		this.worker.run();

		verify(this.groupAccessLayer).getGroupMemberNotificationSetting(anyString(), anyString());
		verify(this.notificationSvc, times(0)).sendLogAlert(eq(this.logEntry.getLogEntry()), eq(MEMBER_ID), eq(this.group), any(GroupNotificationSetting.class));
	}
	
	@Test
	public void GroupDoesNotExist()
	{
		reset(this.groupAccessLayer);
		when(this.groupAccessLayer.getGroupMemberNotificationSetting(anyString(), anyString())).thenReturn(this.setting);
		this.worker.run();

		verify(this.groupAccessLayer, times(0)).getGroupMemberNotificationSetting(anyString(), anyString());
		verify(this.notificationSvc, times(0)).sendLogAlert(eq(this.logEntry.getLogEntry()), eq(MEMBER_ID), eq(this.group), any(GroupNotificationSetting.class));
	}
	
	@Test
	public void LogAlreadyExists()
	{
		reset(this.logAccessLayer);
		when(this.logAccessLayer.insertUniqueMessage(any(UniqueLogEntry.class))).thenReturn(new UniqueLogEntry());
		
		when(this.groupAccessLayer.getGroupMemberNotificationSetting(anyString(), anyString())).thenReturn(this.setting);
		this.worker.run();
		
		verify(this.groupAccessLayer, times(0)).getGroupMemberNotificationSetting(anyString(), anyString());
		verify(this.notificationSvc, times(0)).sendLogAlert(eq(this.logEntry.getLogEntry()), eq(MEMBER_ID), eq(this.group), any(GroupNotificationSetting.class));
	}
	
	@Test
	public void NullEntry()
	{
		this.worker.setEntry(null);
		this.worker.run();
		
		verify(this.logAccessLayer, times(0)).insertUniqueMessage(null);
		verify(this.groupAccessLayer, times(0)).getGroupMemberNotificationSetting(anyString(), anyString());
		verify(this.notificationSvc, times(0)).sendLogAlert(eq(this.logEntry.getLogEntry()), eq(MEMBER_ID), eq(this.group), any(GroupNotificationSetting.class));
	}
}
