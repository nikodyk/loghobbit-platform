package com.loghobbit.service.workers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.loghobbit.entities.v1.AlertCriteria;
import com.loghobbit.service.dal.ILogAccessLayer;
import com.loghobbit.service.dal.entities.UniqueLogEntry;

public class ProcessNewAlertWorker extends Thread {
	private static final Logger logger = Logger.getLogger(ProcessNewAlertWorker.class);
	
	private AlertCriteria alertCriteria;
	public ProcessNewAlertWorker setAlertCriteria(AlertCriteria value)
	{
		this.alertCriteria = value;
		return this;
	}
	
	@Autowired
	private ILogAccessLayer logAccessLayer;
	public ProcessNewAlertWorker putLogAccessLayer(ILogAccessLayer value)
	{
		this.logAccessLayer = value;
		return this;
	}

	public ProcessNewAlertWorker(){
		this.setName("ProcessNewAlertWorker");
	}
	
	@Override
	public void run()
	{
		if(this.alertCriteria == null)
		{
			logger.warn("No alert criteria was supplied to be processed");
			return;
		}
		
		UniqueLogEntry entry;
		while((entry = this.logAccessLayer.getMatchingUniqueWOAlert(this.alertCriteria)) != null)
		{
			entry.getAlertCriteriaIds().add(this.alertCriteria.getId());
			this.logAccessLayer.updateUniqueLogEntry(entry);
		}
	}
}
