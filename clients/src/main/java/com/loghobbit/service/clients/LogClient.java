package com.loghobbit.service.clients;

import java.util.Date;

import org.apache.cxf.helpers.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;

import com.loghobbit.entities.v1.AlertCriteria;
import com.loghobbit.entities.v1.LogFilter;
import com.loghobbit.entities.v1.User;
import com.loghobbit.entities.v1.UserLogEntry;

public class LogClient extends ServiceClientBase {
	
	public LogClient(String endpoint, HttpClient client)
	{
		super(endpoint, client);
	}
	
	public UserLogEntry[] getLogsForGroup(String groupAccessKey, Date requestedDate, int page, User user, LogFilter filter) throws Exception
	{
		HttpPost post = new HttpPost(String.format("%s/v1/logs/group/%s/%d", this.endpoint, groupAccessKey, page));
		this.addCommonHeaders(post, user);
		post.setHeader("requestDate", requestedDate.toString());
		post.setEntity(new StringEntity(mapper.writeValueAsString(filter)));
		
		HttpResponse response = this.client.execute(post);
		this.handleErrorResponse(response);
		
		if(response.getStatusLine().getStatusCode() == 204)
		{
			return null;
		}
		
		UserLogEntry[] retval = mapper.readValue(response.getEntity().getContent(), UserLogEntry[].class);
		response.getEntity().getContent().close();
		
		return retval;
	}

	public int getNewLogCount(String groupAccessKey, Date requestedDate, User user) throws Exception {
		HttpGet get = new HttpGet(String.format("%s/v1/logs/group/%s/new/count", this.endpoint, groupAccessKey));
		get.setHeader("requestDate", requestedDate.toString());
		get.setHeader("Accept", "text/plain");
		get.setHeader("token", user.getSecurityToken());
		
		HttpResponse response = this.client.execute(get);
		this.handleErrorResponse(response);
		
		if(response.getStatusLine().getStatusCode() != 200)
		{
			System.out.println("LogHobbit Client: Count results in : " + response.getStatusLine().getStatusCode());
			return 0;
		}
		
		try
		{
			String value = IOUtils.toString(response.getEntity().getContent());
			return Integer.parseInt(value);
		}
		catch(Throwable e)
		{
			e.printStackTrace();
			return 0;
		}
	}

	public void addAlert(AlertCriteria alertCriteria, User user) throws Exception {
		HttpPut put = new HttpPut(String.format("%s/v1/logs/alert", this.endpoint));
		put.setEntity(new StringEntity(mapper.writeValueAsString(alertCriteria)));
		this.addCommonHeaders(put, user);

		HttpResponse response = this.client.execute(put);
		this.handleErrorResponse(response);
	}
}
