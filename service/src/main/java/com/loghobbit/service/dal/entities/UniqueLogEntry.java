package com.loghobbit.service.dal.entities;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;

import com.loghobbit.entities.LogEntry;

public class UniqueLogEntry {
	public static final String GROUP_ID = "groupId";
	public static final String MESSAGE = "logEntry.message";
	public static final String ALERT_IDS = "alertCriteriaIds";
	public static final String EXCEPTION = "logEntry.exception";
	public static final String ID = "messageId";
	
	@Id
	private String messageId;
	public String getMessageId(){ return this.messageId; }
	public void setMessageId(String value){ this.messageId = value; }
	
	private String groupId;
	public String getGroupId(){ return this.groupId; }
	public void setGroupId(String value){ this.groupId = value; }
	
	private LogEntry logEntry;
	public LogEntry getLogEntry(){ return this.logEntry; }
	public void setLogEntry(LogEntry value){ this.logEntry = value; }
	
	private List<String> alertCriteriaIds;
	public List<String> getAlertCriteriaIds()
	{
		if(this.alertCriteriaIds == null)
		{
			this.alertCriteriaIds = new ArrayList<String>();
		}
		return this.alertCriteriaIds; 
	}
	public void setAlertCriteria(List<String> value){ this.alertCriteriaIds = value; }
}
