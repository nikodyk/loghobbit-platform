package com.loghobbit.service.dal.entities;

import com.loghobbit.entities.v1.GroupStatistics;

public class GroupStatisticsStorage {
	private GroupStatistics value;
	public GroupStatistics getValue(){ return this.value; }
	public void setValue(GroupStatistics value){ this.value = value; }
}
