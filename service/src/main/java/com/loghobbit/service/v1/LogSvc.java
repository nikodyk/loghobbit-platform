package com.loghobbit.service.v1;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.loghobbit.entities.LogEntry;
import com.loghobbit.entities.v1.AlertCriteria;
import com.loghobbit.entities.v1.Group;
import com.loghobbit.entities.v1.LogFilter;
import com.loghobbit.entities.v1.UserLogEntry;
import com.loghobbit.service.AuthenticationFilter;
import com.loghobbit.service.LogMessagingService;
import com.loghobbit.service.dal.IGroupAccessLayer;
import com.loghobbit.service.dal.ILogAccessLayer;
import com.loghobbit.service.dal.entities.LogEntryStorage;
import com.loghobbit.service.dal.entities.UniqueLogEntry;
import com.loghobbit.service.exceptions.AlertExistsException;
import com.loghobbit.service.utils.SvcUtils;


@Path("/v1/logs")
public class LogSvc {
	
	
	@Autowired
	@Qualifier("GroupAccessLayer")
	private IGroupAccessLayer groupAccessLayer;
	public LogSvc setGroupAccessLayer(IGroupAccessLayer value)
	{
		this.groupAccessLayer = value;
		return this;
	}
	
	@Autowired
	@Qualifier("LogAccessLayer")
	private ILogAccessLayer logAccessLayer;
	public LogSvc setLal(ILogAccessLayer value)
	{
		this.logAccessLayer = value;
		return this;
	}
	
	@Autowired
	@Qualifier("LogMessagingService")
	private LogMessagingService logMessagingService;
	public LogSvc setLogMessagingService(LogMessagingService value)
	{
		this.logMessagingService = value;
		return this;
	}
	
	@PUT
	@Path("/entry")
	public Response addLogEntry(LogEntry logEntry)
	{
		if(logEntry.getAccessKey() == null || logEntry.getAccessKey().trim().length() == 0)
		{
			return Response.status(Status.BAD_REQUEST).build();
		}
		
		Group group = this.groupAccessLayer.getGroupByAccessKey(logEntry.getAccessKey());
		if(group == null)
		{
			return Response.status(Status.BAD_REQUEST).entity("The group specified is invalid").build();
		}
		
		this.logAccessLayer.addLogEntry(logEntry, group.getId());
		
		this.logMessagingService.processLogEntry(logEntry, group.getId());
		
		return Response.ok().build();
	}
	
	@POST
	@Path("/group/{groupKey}/{page}")
	public Response getLogsForGroup(LogFilter filter, @PathParam("groupKey") String groupAccessKey, @HeaderParam("requestDate") Date requestDate, @PathParam("page") int page, @HeaderParam(AuthenticationFilter.USER_DATA) String userId)
	{
		if(requestDate == null)
		{
			requestDate = new Date();
		}
		
		Group group = this.groupAccessLayer.getGroupByAccessKey(groupAccessKey);
		if(group == null)
		{
			return Response.status(Status.FORBIDDEN).entity("Could not find the specified group").build();
		}
		
		List<LogEntryStorage> items = this.logAccessLayer.getLogs(group.getId(), requestDate, page, filter);
		if(items == null)
		{
			return Response.ok(new ArrayList<UserLogEntry>()).build();
		}
		
		Map<String,UniqueLogEntry> recievedEntries = new HashMap<String,UniqueLogEntry>();
		Map<String,AlertCriteria> alerts = new HashMap<String, AlertCriteria>();
		
		List<UserLogEntry> retval = new ArrayList<UserLogEntry>(items.size());
		for(LogEntryStorage item : items)
		{
			UniqueLogEntry uniqueEntry = recievedEntries.get(item.getLogEntry().getMessage());
			
			if(uniqueEntry == null)
			{
				uniqueEntry = this.logAccessLayer.getUniqueLogEntriesForLogEntry(group.getId(), item.getLogEntry());
				
				if(uniqueEntry == null)
				{
					uniqueEntry = new UniqueLogEntry();
					uniqueEntry.setGroupId(group.getId());
					uniqueEntry.setLogEntry(item.getLogEntry());
					uniqueEntry = this.logAccessLayer.insertUniqueMessage(uniqueEntry);
				}
				
				recievedEntries.put(uniqueEntry.getLogEntry().getMessage(), uniqueEntry);
			}
			
			
			List<AlertCriteria> logAlerts = new ArrayList<AlertCriteria>(uniqueEntry.getAlertCriteriaIds().size());
			List<String> alertIds = new ArrayList<String>(uniqueEntry.getAlertCriteriaIds());
			Iterator<String> alertIdIterator = alertIds.iterator();
			while(alertIdIterator.hasNext())
			{
				String alertId = alertIdIterator.next();
				AlertCriteria alertCriteria = alerts.get(alertId);
				if(alertCriteria != null)
				{
					logAlerts.add(alertCriteria);
					alertIdIterator.remove();
				}
			}
			
			if(alertIds.size() > 0)
			{
				List<AlertCriteria> missingAlerts = this.logAccessLayer.getAlertsById(alertIds);
				for(AlertCriteria alert : missingAlerts)
				{
					alerts.put(alert.getId(), alert);
					logAlerts.add(alert);
				}
			}
			
			UserLogEntry entry = new UserLogEntry()
										.setLogEntry(item.getLogEntry())
										.setAlertCriteria(logAlerts);
			retval.add(entry);
		}
		
		return Response.ok(retval).build();
	}
	
	@GET
	@Path("/group/{groupId}/new/count")
	@Produces("text/plain")
	public Response getLogsForGroup(@PathParam("groupId") String groupId, @HeaderParam("requestDate") Date requestDate)
	{
		if(requestDate == null)
		{
			return Response.ok(0).build();
		}
		
		int count = this.logAccessLayer.getNewLogCount(groupId, requestDate);
		
		return Response.ok(count).build();
	}
	
	@PUT
	@Path("/alert")
	public Response putAlert(AlertCriteria alertCriteria, @HeaderParam(AuthenticationFilter.USER_DATA) String userId)
	{
		if(alertCriteria == null)
		{
			return SvcUtils.constructError(Status.BAD_REQUEST, "Alert criteria must be specified");
		}
		if(alertCriteria.getGroupId() == null)
		{
			return SvcUtils.constructError(Status.BAD_REQUEST, "Alerts must have a specified group id");
		}
		if(alertCriteria.getMessageRegex() == null)
		{
			return SvcUtils.constructError(Status.BAD_REQUEST, "Alerts must have a specified message regex");
		}
		
		if(alertCriteria.getId() == null || alertCriteria.getId().isEmpty())
		{
			if(!alertCriteria.getUsers().contains(userId))
			{
				alertCriteria.getUsers().add(userId);
			}
			try
			{
				this.logAccessLayer.addAlert(alertCriteria);
				this.logMessagingService.processNewAlert(alertCriteria);
				return Response.ok(alertCriteria).build();
			}
			catch(AlertExistsException exception)
			{
				AlertCriteria existing = exception.getExistingAlert();
				if(existing != null)
				{
					alertCriteria.setId(existing.getId());
				}
			}
		}
		
		if(!alertCriteria.getUsers().contains(userId))
		{
			alertCriteria.getUsers().add(userId);
		}
		
		this.logAccessLayer.updateAlert(alertCriteria);
		
		return Response.ok().build();
	}
}
