package com.loghobbit.service.utils;

import java.util.Calendar;
import java.util.Date;

public class DateUtils {
	public static Date getTodayBOD()
	{
		Calendar today = Calendar.getInstance();
		today.set(today.get(Calendar.YEAR), today.get(Calendar.MONTH), today.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
		today.set(Calendar.MILLISECOND, 0);
		
		return today.getTime();
	}
	
	public static Date getTomorrowBOD()
	{
		Calendar retval = Calendar.getInstance();
		retval.setTime(getTodayBOD());
		retval.add(Calendar.DAY_OF_MONTH, 1);
		
		return retval.getTime();
	}
}
