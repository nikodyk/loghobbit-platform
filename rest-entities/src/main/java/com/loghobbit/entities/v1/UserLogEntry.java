package com.loghobbit.entities.v1;

import java.util.ArrayList;
import java.util.List;

import com.loghobbit.entities.LogEntry;

public class UserLogEntry {
	private LogEntry logEntry;
	public LogEntry getLogEntry(){ return this.logEntry; }
	public UserLogEntry setLogEntry(LogEntry value)
	{ 
		this.logEntry = value; 
		return this;
	}
	
	private List<AlertCriteria> alertCriteria;
	public List<AlertCriteria> getAlertCriteria()
	{
		if(this.alertCriteria == null)
		{
			this.alertCriteria = new ArrayList<AlertCriteria>();
		}
		return this.alertCriteria; 
	}
	public UserLogEntry setAlertCriteria(List<AlertCriteria> value)
	{
		this.alertCriteria = value;
		return this;
	}
}
