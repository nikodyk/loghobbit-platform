package com.loghobbit.entities.v1;

public enum NotificationResponse {
	Accept,
	Decline
}
