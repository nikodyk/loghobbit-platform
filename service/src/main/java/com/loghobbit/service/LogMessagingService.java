package com.loghobbit.service;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.loghobbit.entities.Level;
import com.loghobbit.entities.LogEntry;
import com.loghobbit.entities.v1.AlertCriteria;
import com.loghobbit.service.dal.entities.UniqueLogEntry;
import com.loghobbit.service.workers.AnalyzeUniqueLogEntryWorker;
import com.loghobbit.service.workers.ProcessNewAlertWorker;

public class LogMessagingService {
	@Autowired
	private ApplicationContext applicationContext;
	private ExecutorService executor;
	
	public void initialize()
	{
		this.executor = Executors.newCachedThreadPool();
	}

	public void processLogEntry(LogEntry logEntry, String groupId)
	{
		if(logEntry.getLevel() == Level.info || logEntry.getLevel() == Level.warn)
		{
			return;
		}
		
		UniqueLogEntry unique = new UniqueLogEntry();
		unique.setGroupId(groupId);
		unique.setLogEntry(logEntry);
		
		AnalyzeUniqueLogEntryWorker worker = (AnalyzeUniqueLogEntryWorker)this.applicationContext.getBean("AnalyseUniqueLogEntryWorker");
		worker.setEntry(unique);
		
		this.executor.execute(worker);
	}
	
	public void processNewAlert(AlertCriteria alert)
	{
		ProcessNewAlertWorker worker = (ProcessNewAlertWorker)this.applicationContext.getBean("ProcessNewAlertWorker");
		worker.setAlertCriteria(alert);
		
		this.executor.execute(worker);
	}
}
