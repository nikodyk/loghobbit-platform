package com.loghobbit.service.dal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.IndexOperations;
import org.springframework.data.mongodb.core.index.Index;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.loghobbit.entities.v1.Notification;
import com.loghobbit.entities.v1.NotificationType;
import com.loghobbit.entities.v1.User;
import com.loghobbit.service.dal.entities.NotificationStorage;

public class NotificationAccessLayer extends MongoDaoBase implements INotificationAccessLayer {
	public void initialize()
	{
		IndexOperations notification = this.getOperations().indexOps(User.class);
		notification.ensureIndex(new Index().on(NotificationStorage.USER_ID, Direction.ASC)
											.on(NotificationStorage.DATE, Direction.DESC));
	}
	
	public void addUserNotification(String userId, Notification notification)
	{
		NotificationStorage storage = new NotificationStorage();
		storage.setUserId(userId);
		storage.setNotification(notification);
		storage.setDateMS(new Date().getTime());
		
		if(notification.getType() == NotificationType.JoinGroup)
		{
			NotificationStorage result = this.getOperations().findOne(new Query(
										Criteria.where(NotificationStorage.USER_ID).is(userId)
												.and(NotificationStorage.GROUP_ID).is(notification.getGroup().getId())), 
										NotificationStorage.class);
			if(result != null)
			{
				return;
			}
		}
		
		this.getOperations().insert(storage);
	}
	
	public List<Notification> getUserNotifications(String userId)
	{
		Query query = new Query(Criteria.where(NotificationStorage.USER_ID).is(userId));
		query.with(new Sort(Direction.DESC, NotificationStorage.DATE));
		
		List<NotificationStorage> results = this.getOperations().find(query, NotificationStorage.class);
		if(results == null || results.size() == 0)
		{
			return null;
		}
		
		this.getOperations().upsert(query, new Update().set(NotificationStorage.VIEWED, true), NotificationStorage.class);
		
		List<Notification> retval = new ArrayList<Notification>(results.size());
		for(NotificationStorage item : results)
		{
			if(item.getNotification() == null)
			{
				this.getOperations().remove(new Query(Criteria.where(NotificationStorage.NOTIFICATION).is(null)), NotificationStorage.class);
				continue;
			}
			retval.add(item.getNotification());
		}
		
		return retval;
	}
	
	public int getPendingUserNotificationCount(String userId)
	{
		Query query = new Query(Criteria.where(NotificationStorage.USER_ID).is(userId)
										.and(NotificationStorage.VIEWED).is(false));
		
		return (int)this.getOperations().count(query, NotificationStorage.class);
	}

	@Override
	public Notification getUserNotificationForGroup(String groupId, String userId) {
		
		NotificationStorage result =  this.getOperations().findOne(
										new Query(Criteria.where(NotificationStorage.USER_ID).is(userId)
																.and(NotificationStorage.GROUP_ID).is(new ObjectId(groupId))), 
										NotificationStorage.class);
		
		return (result == null)? null : result.getNotification();
	}

	@Override
	public void deleteUserNotification(String groupId, String userId) {
		this.getOperations().remove(new Query(Criteria
											.where(NotificationStorage.USER_ID).is(userId)
											.and(NotificationStorage.GROUP_ID).is(new ObjectId(groupId))),
									NotificationStorage.class);
	}
}
