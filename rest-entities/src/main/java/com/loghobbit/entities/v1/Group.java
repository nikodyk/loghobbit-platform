package com.loghobbit.entities.v1;

import java.util.ArrayList;
import java.util.List;

public class Group {
	public static final String ACCESS_KEY = "accessKey";
	public static final String USER_EMAIL = "teamMembers.memberId";
	public static final String USER_ACCEPTED = "teamMembers.accepted";
	public static final String DESCRIPTION = "description";
	public static final String NAME = "name";
	public static final String ID = "id";
	public static final String TEAM_MEMBERS = "teamMembers";
	public static final String LAST_ANALYZED_DATE = "analyzedDate";
	
	private String id;
	public String getId(){ return this.id; }
	public void setId(String value){ this.id = value; }
	
	private String name;
	public String getName(){ return this.name; }
	public void setName(String value){ this.name = value; }
	
	private String description;
	public String getDescription(){ return this.description; }
	public void setDescription(String value){ this.description = value; }
	
	private String accessKey;
	public String getAccessKey(){ return this.accessKey; }
	public void setAccessKey(String value){ this.accessKey = value; }
	
	private List<TeamMember> teamMembers;
	public List<TeamMember> getTeamMembers()
	{
		if(this.teamMembers == null)
		{
			this.teamMembers = new ArrayList<TeamMember>();
		}
		return this.teamMembers;
	}
	public void setTeamMembers(List<TeamMember> value){ this.teamMembers = value; }
}
