package com.loghobbit.portal.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.loghobbit.entities.v1.Group;
import com.loghobbit.entities.v1.TeamMember;
import com.loghobbit.entities.v1.TeamMemberType;
import com.loghobbit.entities.v1.User;
import com.loghobbit.portal.filters.LoggedIn;
import com.loghobbit.portal.model.UserGroup;
import com.loghobbit.service.clients.ServiceClientFactory;

@Controller
public class GroupController {
	private static Logger logger = Logger.getLogger(GroupController.class);
	
	@Autowired
	@Qualifier("ServiceClientFactory")
	public ServiceClientFactory clientFactory;
	public GroupController setClientFactory(ServiceClientFactory value)
	{
		this.clientFactory = value;
		return this;
	}
	
	@LoggedIn
	@RequestMapping(value="/groups", method= RequestMethod.GET)
	public ModelAndView getGroupsPage(HttpServletRequest request, @RequestParam(required = false) String deleteId, @RequestParam(required = false) String leaveId, @RequestParam(required = false) boolean isNew, ModelAndView model)
	{
		User user = (User) request.getSession().getAttribute(SessionItems.USER);
		model.addObject("canHaveAdmins", (user.getSystemOptions() == null)? false : user.getSystemOptions().getCanHaveAdmins());
		
		if(deleteId != null)
		{
			try {
				this.clientFactory.getGroupClient().deleteGroup(deleteId, user);
			} catch (Exception e) {
				model.addObject("SystemError", e.getMessage());
			}
		}
		if(leaveId != null)
		{
			try {
				this.clientFactory.getGroupClient().leaveGroup(leaveId, user);
			} catch(Exception e)
			{
				model.addObject("SystemError", e.getMessage());
			}
		}
		
		try {
			Group[] groupItems = this.clientFactory.getGroupClient().getUsersGroups(user.getSecurityToken());
			List<UserGroup> groups = new ArrayList<UserGroup>(); 
			for(Group item : groupItems)
			{
				TeamMemberType type = TeamMemberType.member;
				String owner = null;
				for(TeamMember member : item.getTeamMembers())
				{
					if(member.getType() == TeamMemberType.owner)
					{
						owner = member.getMemberId();
					}
					if(member.getMemberId().equals(user.getEmail()))
					{
						type = member.getType();
					}
				}
				UserGroup group = new UserGroup()
										.setGroup(item)
										.setUserMemberType(type.name())
										.setOwner(owner);
				
				groups.add(group);
			}
			request.getSession().setAttribute(SessionItems.GROUPS, groups);
			
			if(isNew)
			{
				groups = new ArrayList<UserGroup>(groups);
				
				TeamMember owner = new TeamMember();
				owner.setMemberId(user.getEmail());
				owner.setAccepted(true);
				owner.setType(TeamMemberType.owner);
				
				Group group = new Group();
				group.setName("New Group");
				group.getTeamMembers().add(owner);
				
				UserGroup userGroup = new UserGroup();
				userGroup.setGroup(group)
						.setUserMemberType(TeamMemberType.owner.name())
						.setOwner(user.getEmail());
				
				groups.add(userGroup);
			}
			
			model.addObject("groups", groups);
		} catch (Exception e) {
			model.addObject("SystemError", e.getMessage());
		}
		
		model.setViewName("user-level/groups");
		return model;
	}
	
	@LoggedIn
	@RequestMapping(value="/group/{groupId}")
	public @ResponseBody Group getGroup(@PathVariable("groupId") String groupId, HttpServletRequest request)
	{
		@SuppressWarnings("unchecked")
		List<UserGroup> groups = (List<UserGroup>)request.getSession().getAttribute(SessionItems.GROUPS);
		
		for(UserGroup group : groups)
		{
			if(group.getGroup().getId().equals(groupId))
			{
				return group.getGroup();
			}
		}
		
		return null;
	}
	
	@LoggedIn
	@RequestMapping(value="/group/{groupId}/new/count")
	public @ResponseBody int getGroupNewLogsCount(@PathVariable("groupId") String groupId, HttpServletRequest request)
	{
		Date logDate = (Date)request.getSession().getAttribute(SessionItems.LOG_REQUEST_DATE);
		User user = (User)request.getSession().getAttribute(SessionItems.USER);
		
		try
		{
			int retval = this.clientFactory.getLogClient().getNewLogCount(groupId, logDate, user);
			return retval;
		}
		catch(Throwable e)
		{
			logger.error("Could not get the count of new log messages for log " + groupId, e);
			return 0;
		}
	}
	
	@LoggedIn
	@RequestMapping(value="/group", method=RequestMethod.PUT)
	public @ResponseBody String putGroup(@RequestBody String content, HttpServletRequest request)
	{
		User user = (User) request.getSession().getAttribute(SessionItems.USER);
		try {
			String retval = this.clientFactory.getGroupClient().putGroup(content, user);
			return retval;
		} catch (Exception e) {
			logger.error("Could not create the group specified", e);
		}
		return null;
	}
}
