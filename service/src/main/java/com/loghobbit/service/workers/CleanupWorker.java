package com.loghobbit.service.workers;

import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.loghobbit.entities.v1.Group;
import com.loghobbit.entities.v1.User;
import com.loghobbit.entities.v1.requests.SubscriptionUpdateRequest;
import com.loghobbit.service.dal.IGroupAccessLayer;
import com.loghobbit.service.dal.ILogAccessLayer;
import com.loghobbit.service.dal.IUserAccessLayer;

public class CleanupWorker extends ContinualWorkerBase {
	
	@Autowired
	private IUserAccessLayer userAccessLayer;
	public CleanupWorker setUserAccessLayer(IUserAccessLayer value)
	{
		this.userAccessLayer = value;
		return this;
	}
	
	@Autowired
	@Qualifier("GroupAccessLayer")
	private IGroupAccessLayer groupAccessLayer;
	public CleanupWorker setGroupAccessLayer(IGroupAccessLayer value)
	{
		this.groupAccessLayer = value;
		return this;
	}
	
	@Autowired
	@Qualifier("LogAccessLayer")
	private ILogAccessLayer logAccessLayer;
	public CleanupWorker setLal(ILogAccessLayer value)
	{
		this.logAccessLayer = value;
		return this;
	}
	
	public CleanupWorker()
	{
		super("Cleanup Worker", "cleaning up logs");
		this.idleSleepTime = 60;
	}

	@Override
	protected boolean executeTask() {
		User user = this.userAccessLayer.getUserForCleanup();
		if(user == null)
		{
			return false;
		}
		
		if(user.getSystemOptions().getDaysBeforeLogCleanup() != SubscriptionUpdateRequest.NO_LIMIT)
		{
			Calendar cleanUpDate = Calendar.getInstance();
			cleanUpDate.add(Calendar.DAY_OF_MONTH, user.getSystemOptions().getDaysBeforeLogCleanup() * -1);
			
			List<Group> groups = this.groupAccessLayer.getGroupsOwnedByUser(user.getEmail());
			for(Group group : groups)
			{
				if(!this.isRunning)
				{
					// Exit quickly if we're shutting down
					return true;
				}
				this.logAccessLayer.cleanUpOldLogs(group.getId(), cleanUpDate.getTime().getTime());
			}
		}
		
		return true;
	}
}
