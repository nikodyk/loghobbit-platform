package com.loghobbit.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

public class VersionSvc {
	@GET
	@Path("/version")
	@Produces("text/plain")
	public String getVersion()
	{
		return "1.3.0";
	}
}
