package com.loghobbit.entities.v1;

public enum TeamMemberType {
	owner,
	admin,
	member,
	none
}
