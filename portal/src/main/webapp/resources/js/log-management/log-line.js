
function moreLess(element)
{
	var jelement = $(element);
	
	var target = jelement.attr("target");
	var logDetails = $("#" + target);
	
	if(logDetails.is(':not(:hidden)'))
	{
		var text = jelement.html();
		text = text.replace('Less', 'More');
		jelement.html(text);
		logDetails.slideUp();
	}
	else
	{
		var text = jelement.html();
		text = text.replace('More', 'Less');
		jelement.html(text);
		logDetails.slideDown();
	}
}

function checkForNewLogs()
{
	var groupId = $("#groupId").val();
	$.ajax({
		type : "get",
		url : "/group/" + groupId + "/new/count",
		accept: "application/json",
		success:function(data){
			$("#newLogEntryCount").text(data);
			
			if(data == 0)
			{
				$("#newLogButton").hide();
			}
			else
			{
				$("#newLogButton").show();
			}
			
		}
	});
}

setInterval(checkForNewLogs, 2000);