package com.loghobbit.service.v1;

import static org.mockito.Mockito.*;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;

import javax.ws.rs.core.Response;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.loghobbit.entities.v1.GroupStatistics;

public class GroupSvc_StatsTest extends GroupSvc_TestBase {
	private List<GroupStatistics> previousStats;
	private List<GroupStatistics> todayStats;
	private Date lalStartDate = null;
	private Date lalEndDate = null;
	
	@SuppressWarnings("unchecked")
	@Before
	public void statsSetup()
	{
		GroupStatistics stat = new GroupStatistics();
		stat.setDate(new Date());
		stat.setGroupId(GROUP_ID);
		stat.setLogCount(10);
		this.previousStats = Arrays.asList(stat);
		
		stat = new GroupStatistics();
		stat.setDate(new Date());
		stat.setGroupId(GROUP_ID);
		stat.setLogCount(11);
		this.todayStats = Arrays.asList(stat);
		
		when(this.groupAccessLayer.getGroupsStats(any(ArrayList.class), any(Date.class), any(Date.class))).thenReturn(this.previousStats);
		when(this.groupAccessLayer.getGroupsOwnedByUser(anyString())).thenReturn(Arrays.asList(this.group));
		when(this.logAccessLayer.getSizeOfLogsInGroup(any(ArrayList.class), any(Date.class), any(Date.class))).thenAnswer(new Answer<List<GroupStatistics>>(){

			@Override
			public List<GroupStatistics> answer(InvocationOnMock invocation) throws Throwable {
				lalStartDate = invocation.getArgumentAt(1, Date.class);
				lalEndDate = invocation.getArgumentAt(2, Date.class);
				return todayStats;
			}
		});
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getGroupStats_NoDateRange()
	{
		Response response = this.groupSvc.getGroupStats(null, null, USER_ID);
		
		Assert.assertNotNull("Response", response);
		Assert.assertEquals(200, response.getStatus());
		
		Assert.assertNotNull("Entity", response.getEntity());
		List<GroupStatistics> stats = (List<GroupStatistics>)response.getEntity();
		Assert.assertEquals("Group Statistics Size", 2, stats.size());
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getGroupStats_PreviousRange()
	{
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_MONTH, -1);
		Date endDate = calendar.getTime();
		
		calendar.add(Calendar.DAY_OF_MONTH, -1);
		Date startDate = calendar.getTime();
		
		Response response = this.groupSvc.getGroupStats(startDate, endDate, USER_ID);
		
		Assert.assertNotNull("Response", response);
		Assert.assertEquals(200, response.getStatus());
		
		Assert.assertNotNull("Entity", response.getEntity());
		List<GroupStatistics> stats = (List<GroupStatistics>)response.getEntity();
		Assert.assertEquals("Group Statistics Size", 1, stats.size());
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getGroupStats_RangeWithToday()
	{
		Calendar calendar = Calendar.getInstance();
		Date endDate = calendar.getTime();
		
		calendar.add(Calendar.DAY_OF_MONTH, -1);
		Date startDate = calendar.getTime();
		
		Response response = this.groupSvc.getGroupStats(startDate, endDate, USER_ID);
		
		Assert.assertNotNull("Response", response);
		Assert.assertEquals(200, response.getStatus());
		
		Assert.assertNotNull("Entity", response.getEntity());
		List<GroupStatistics> stats = (List<GroupStatistics>)response.getEntity();
		Assert.assertEquals("Group Statistics Size", 2, stats.size());
		
		Calendar todayCalendar = Calendar.getInstance();
		todayCalendar.set(todayCalendar.get(Calendar.YEAR), todayCalendar.get(Calendar.MONTH), todayCalendar.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
		todayCalendar.set(Calendar.MILLISECOND, 0);
		Date startRange = todayCalendar.getTime();
		todayCalendar.add(Calendar.DAY_OF_MONTH, 1);
		
		verify(this.logAccessLayer).getSizeOfLogsInGroup(any(ArrayList.class), any(Date.class), any(Date.class));
		Assert.assertEquals(startRange, lalStartDate);
		Assert.assertEquals(todayCalendar.getTime(), lalEndDate);
	}
}
