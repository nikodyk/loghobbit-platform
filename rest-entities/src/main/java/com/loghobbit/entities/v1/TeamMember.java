package com.loghobbit.entities.v1;

public class TeamMember {
	private String memberId;
	public String getMemberId(){ return this.memberId; }
	public void setMemberId(String value){ this.memberId = value; }
	
	private TeamMemberType type;
	public TeamMemberType getType(){ return this.type; }
	public void setType(TeamMemberType value){ this.type = value; }
	
	private boolean accepted;
	public boolean getAccepted(){ return this.accepted; }
	public void setAccepted(boolean value){ this.accepted = value; }
	
	public TeamMember()
	{
	}
	
	public static TeamMember createTeamMember(String memberId, TeamMemberType type, boolean accepted)
	{
		TeamMember retval = new TeamMember();
		retval.memberId = memberId;
		retval.accepted = accepted;
		retval.type = type;
		
		return retval;
	}
}
