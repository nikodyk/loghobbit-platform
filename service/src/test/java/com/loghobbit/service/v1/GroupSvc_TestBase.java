package com.loghobbit.service.v1;

import static org.mockito.Mockito.*;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.http.entity.StringEntity;
import org.junit.Assert;
import org.junit.Before;

import com.loghobbit.entities.v1.Group;
import com.loghobbit.entities.v1.TeamMember;
import com.loghobbit.entities.v1.TeamMemberType;
import com.loghobbit.entities.v1.User;
import com.loghobbit.entities.v1.UserSystemOptions;
import com.loghobbit.service.dal.IGroupAccessLayer;
import com.loghobbit.service.dal.ILogAccessLayer;
import com.loghobbit.service.dal.IUserAccessLayer;

public class GroupSvc_TestBase {
	protected static final String GROUP_ID = "test id";
	protected static final String USER_ID = "test@test.com";
	protected static final String ACCESS_KEY = "access key";
	
	protected GroupSvc groupSvc;
	protected IGroupAccessLayer groupAccessLayer;
	protected ILogAccessLayer logAccessLayer;
	protected IUserAccessLayer userAccessLayer;
	protected NotificationSvc notificationSvc;
	protected Group group = new Group();
	protected TeamMember member = new TeamMember();
	protected TeamMember anotherMember = new TeamMember();
	protected User user = new User();
	
	@Before
	public void setup()
	{
		UserSystemOptions options = new UserSystemOptions();
		options.setCanHaveAdmins(true);
		
		this.user.setEmail(USER_ID);
		this.user.setFullName("Test user");
		this.user.setIsValidated(true);
		this.user.setSystemOptions(options);
		
		this.member.setMemberId(USER_ID);
		this.member.setAccepted(true);
		this.member.setType(TeamMemberType.owner);
		
		anotherMember.setAccepted(true);
		anotherMember.setMemberId("another@member.com");
		anotherMember.setType(TeamMemberType.member);
		
		this.group = new Group();
		this.group.setAccessKey(ACCESS_KEY);
		this.group.setId(GROUP_ID);
		this.group.setName("Test group");
		this.group.getTeamMembers().add(anotherMember);
		this.group.getTeamMembers().add(this.member);
		
		this.groupAccessLayer = mock(IGroupAccessLayer.class);
		when(this.groupAccessLayer.getGroupById(GROUP_ID)).thenReturn(this.group);
		
		this.userAccessLayer = mock(IUserAccessLayer.class);
		when(this.userAccessLayer.getUserFromId(USER_ID)).thenReturn(this.user);
		
		this.notificationSvc = mock(NotificationSvc.class);
		this.logAccessLayer = mock(ILogAccessLayer.class);
		
		this.groupSvc = new GroupSvc()
							.setGroupAccessLayer(this.groupAccessLayer)
							.setUserAccessLayer(this.userAccessLayer)
							.setNotificationService(this.notificationSvc)
							.setLogAccessLayer(this.logAccessLayer);
	}
	
	protected void validateErrorResponse(Response response, int errorCode)
	{
		Assert.assertNotNull(response);
		Assert.assertEquals("Status Code", errorCode, response.getStatus());
		Assert.assertNotNull("Entity", response.getEntity());
		Assert.assertTrue("Entity Type", response.getEntity().getClass() == StringEntity.class);
		Assert.assertEquals(MediaType.TEXT_PLAIN_TYPE, response.getMediaType());
	}
}
