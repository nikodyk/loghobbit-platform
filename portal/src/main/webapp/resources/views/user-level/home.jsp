<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>LogHobbit - Home</title>
	    
	<jsp:include page="../../fragments/header.jsp"/>
	<script type="text/javascript" src="/resources/js/userVerification.js"></script>
	<script type="text/javascript" src="/resources/js/sign-up.js"></script>
	<script type="text/javascript" src="/resources/js/log-management/log-table.js"></script>
</head>
<body>
	<jsp:include page="../../fragments/navbar.jsp" />
	<c:if test="${groups != null }">
		<input id="groupId" type="hidden" value="${groups[selectedOffset].id }"/>
	</c:if>
	<div class="container">
		<div class="row page-header">
			<span class="heading-title">Groups:</span>
			<c:forEach var="item" items="${groups}" varStatus="loop">
				<span>
					<c:choose>
						<c:when test="${loop.index == selectedOffset}">
							<b>
								<a href="?groupId=${item.id}">${item.name}</a>
							</b>
						</c:when>
						<c:otherwise>
							<a href="?groupId=${item.id}">${item.name}</a>
						</c:otherwise>
					</c:choose>
				</span> |
			</c:forEach>
			
			<a href="/groups?isNew=true">Create</a>
		</div>
		<c:if test="${logs == null && sessionScope.filter == null}">
			<div class="row bs-callout bs-callout-info ">
				<b>Getting Started</b>
				<p>
					There are two ways to manage your logs.  Either you can use your personal log, named <a href="#">Your Logs</a>,
					or you can create an <a href="#">Groups</a> which allows you to share access to logs with others.
				</p>
				<p>
					<b>Access Key:</b> ${group.accessKey}
				</p>
				<p>
					For more details see the <a href="#">LogHobbit Walk Through</a>
				</p>
			</div>
		</c:if>
		<jsp:include page="log-partial.jsp" />
	</div>
	<jsp:include page="../../fragments/group-edit-form.jsp" />
</body>
</html>