package com.loghobbit.service.v1;

import static org.mockito.Mockito.*;

import javax.ws.rs.core.Response;

import org.junit.Assert;
import org.junit.Test;

import com.loghobbit.entities.v1.TeamMemberType;

public class GroupSvc_LeavGroupTest extends GroupSvc_TestBase {
	
	@Test
	public void leaveGroup_NullGroupId() throws Exception
	{
		Response response = this.groupSvc.leaveGroup(null, USER_ID);
		
		Assert.assertNotNull(response);
		Assert.assertEquals("Status Code", 400, response.getStatus());
	}
	
	@Test
	public void leaveGroup_NoGroup() throws Exception
	{
		reset(this.groupAccessLayer);
		Response response = this.groupSvc.leaveGroup(GROUP_ID, USER_ID);
		
		Assert.assertNotNull(response);
		Assert.assertEquals("Status Code", 400, response.getStatus());
	}
	
	@Test
	public void leaveGroup_MemberNotInGroup() throws Exception
	{
		Response response = this.groupSvc.leaveGroup(GROUP_ID, "Not a user");
		
		Assert.assertNotNull(response);
		Assert.assertEquals("Status Code", 400, response.getStatus());
	}
	
	@Test
	public void leaveGroup_Valid_NonAdmin() throws Exception
	{
		this.member.setType(TeamMemberType.member);
		Response response = this.groupSvc.leaveGroup(GROUP_ID, "Not a user");
		
		Assert.assertNotNull(response);
		Assert.assertEquals("Status Code", 400, response.getStatus());
	}
	
	@Test
	public void leaveGroup_Valid() throws Exception
	{
		this.member.setType(TeamMemberType.admin);
		Response response = this.groupSvc.leaveGroup(GROUP_ID, USER_ID);
		
		Assert.assertNotNull(response);
		Assert.assertEquals("Status Code", 200, response.getStatus());
	}
}
