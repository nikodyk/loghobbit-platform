
function verifyPassword(){
	var password1 = $(".password");
	var password2 = $(".password-verify");
	
	password1.parent().removeClass("has-error");
	password2.parent().removeClass("has-error");
	
	if(password1.val() != password2.val())
	{
		password1.parent().addClass("has-error");
		password2.parent().addClass("has-error");
	}
	if(password1.val().length == 0)
	{
		password1.parent().addClass("has-error");
		password2.parent().addClass("has-error");
	}
}

$(document).ready(function(){
	$(".fullName").change(function(){
		if($(this).val() == "")
		{
			$(this).addClass("has-error");
		}
		else
		{
			$(this).removeClass("has-error")
		}
	});

	$(".email").change(function(){
		var email = $(".email");
		email.parent().removeClass("has-error");
		
		if(email.val().indexOf("@") < 0){
			email.parent().addClass("has-error");
		}
		if(email.val().indexOf(".") < 0){
			email.parent().addClass("has-error");
		}
	});
	$(".password").change(function(){ verifyPassword(); });

	$(".password-verify").change(function(){ verifyPassword(); });
});
