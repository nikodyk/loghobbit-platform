package com.loghobbit.entities.v1;

public class Credentials {
	private String email;
	public String getEmail(){ return this.email; }
	public void setEmail(String value){ this.email = value; }
	
	private String password;
	public String getPassword(){ return this.password; }
	public void setPassword(String value){ this.password = value; }
	
	public Credentials()
	{
		
	}
	public Credentials(String email, String password)
	{
		this.email = email;
		this.password = password;
	}
	
}
